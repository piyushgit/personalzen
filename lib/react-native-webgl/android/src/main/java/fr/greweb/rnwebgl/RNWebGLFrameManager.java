package fr.greweb.rnwebgl;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class RNWebGLFrameManager extends ReactContextBaseJavaModule {
    @Override
    public String getName() {
        return "RNWebGLFrameManager";
    }

    public RNWebGLFrameManager(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void endFrame(final int ctxId) {
        RNWebGLView.endFrame(ctxId);
    }
}
