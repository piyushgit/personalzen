/*
 *  Copyright 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  This file is governed by the BSD-style license found in the webrtc repository.
 *  It's derived from avfoundationvideocapturer.h/mm at 0d3eef20802f in
 *  https://chromium.googlesource.com/external/webrtc.
 */

#import <AVFoundation/AVFoundation.h>
#import <WebRTC/RTCMacros.h>
#import <WebRTC/RTCAudioSource.h>

@class RTCMediaConstraints;
@class RTCPeerConnectionFactory;

/// The format that the sample buffers you provide to the video source will be in
typedef struct {
    int bitsPerSample;
    int sampleRate;
    int numberOfChannels;
} RTCSampleBufferAudioFormat;


/// Video capturer that captures individual frames manually from an external source.
RTC_EXPORT
@interface RTCSampleBufferAudioSource : RTCAudioSource
- (instancetype)initWithFactory:(RTCPeerConnectionFactory *)factory
    format:(RTCSampleBufferAudioFormat)format
    NS_DESIGNATED_INITIALIZER;

- (void)captureSampleBuffer:(CMSampleBufferRef)sampleBuffer;
@end
