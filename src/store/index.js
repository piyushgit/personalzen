import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { persistCombineReducers, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';

import * as Routes from '../routes';
import client from '../services/client';
import rootReducer from '../reducers';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['nav', 'rehydrated'],
};

const persistedReducer = persistCombineReducers(persistConfig, rootReducer);
const middleware = [thunk, Routes.middleware];

if (process.env.NODE_ENV !== 'production') {
  middleware.push(createLogger());
}

const store = createStore(persistedReducer, applyMiddleware(...middleware));
const persistor = persistStore(store);

store.subscribe(() => {
  const state = store.getState();

  if (state.auth) {
    client.setToken(state.auth.token);
  }
});

export { store, persistor };
