import React, { PureComponent } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';

import { AppNavigator } from './routes';

// create nav component
class ReduxNavigator extends PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    const { navigation, dispatch } = this.props;
    if (navigation.index === 0) {
      return false;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { navigation, dispatch } = this.props;

    return (
      <AppNavigator
        state={navigation}
        dispatch={dispatch}
      />
    );
  }
}

const mapStateToProps = state => ({
  navigation: state.nav,
});

export default connect(mapStateToProps)(ReduxNavigator);
