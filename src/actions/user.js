import moment from 'moment';

import { fetchUsers, createUser } from '../services/user';

export const FIND_OR_CREATE_USER = 'FIND_OR_CREATE_USER';
export const FETCH_USERS_COMPLETED = 'FETCH_USERS_COMPLETED';
export const FETCH_USERS_FAILURE = 'FETCH_USERS_FAILURE';

export const FETCH_USER_COMPLETED = 'FETCH_USER_COMPLETED';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const findOrCreateUser = () => (dispatch) => {
  const today = moment();

  return createUser({ downloadDate: moment(today, moment.ISO_8601).format() })
    .then((response) => {
      const user = response.data[0];

      dispatch({
        type: FETCH_USER_COMPLETED,
        user,
      });

      return user;
    })
    .catch((err) => {
      dispatch({
        type: FETCH_USER_FAILURE,
        error: err.message,
      });

      return err;
    });
};

export const getUsers = (page, search) => dispatch => fetchUsers(page, search)
  .then((users) => {
    dispatch({
      type: FETCH_USERS_COMPLETED,
      users,
    });
  })
  .catch((err) => {
    dispatch({
      type: FETCH_USERS_FAILURE,
      error: err.message,
    });
  });
