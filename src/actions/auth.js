import { authenticate } from '../services/auth';
import { getDeviceId } from '../utils/device-id';

export const AUTHENTICATE_COMPLETED = 'AUTHENTICATE_COMPLETED';
export const AUTHENTICATE_FAILURE = 'AUTHENTICATE_FAILURE';

export function login() {
  return dispatch => getDeviceId()
    .then(deviceId => authenticate({ deviceId }))
    .then((response) => {
      const payload = {
        token: response.accessToken,
        isSuper: response.isSuper,
      };

      dispatch({
        type: AUTHENTICATE_COMPLETED,
        payload,
      });

      return payload;
    })
    .catch((err) => {
      dispatch({
        type: AUTHENTICATE_FAILURE,
        error: err.message,
      });

      return err;
    });
}
