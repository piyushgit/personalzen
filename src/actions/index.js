import { createAction } from 'redux-actions';

import * as Names from './index.names';

export * from './user';
export * from './auth';

export const updatePlayedTime = createAction(Names.UPDATE_PLAYED_TIME);
export const updateSaturation = createAction(Names.UPDATE_SATURATION);
export const updateScrollPosition = createAction(Names.UPDATE_SCROLL_POSITION);
export const updateWeeklyPlayedTime = createAction(Names.UPDATE_WEEKLY_PLAYED_TIME);
export const updateStressLevel = createAction(Names.UPDATE_STRESS_LEVEL);
export const updateGoal = createAction(Names.UPDATE_GOAL);
export const resetWeeklyStart = createAction(Names.RESET_WEEKLY_START);

export const updateAchievementIsRecent = createAction(Names.UPDATE_ACHIEVEMENT_IS_RECENT);
export const updateAchievementScholar = createAction(Names.UPDATE_ACHIEVEMENT_SCHOLAR);
export const updateAchievementPlayedTime = createAction(Names.UPDATE_ACHIEVEMENT_PLAYED_TIME);
export const updateAchievementRegions = createAction(Names.UPDATE_ACHIEVEMENT_REGIONS);
export const updateAchievementStressLevel = createAction(Names.UPDATE_ACHIEVEMENT_STRESSLEVEL);
export const updateAchievementGoals = createAction(Names.UPDATE_ACHIEVEMENT_GOALS);
export const updateAchievementDays = createAction(Names.UPDATE_ACHIEVEMENT_DAYS);

export const updateLastAccess = createAction(Names.UPDATE_LAST_ACCESS);

export const playSound = createAction(Names.PLAY_SOUND);
export const stopSound = createAction(Names.STOP_SOUND);
export const resetSounds = createAction(Names.RESET_SOUNDS);

export const soundEffectsOn = createAction(Names.SOUND_EFFECTS_ON);
export const soundEffectsOff = createAction(Names.SOUND_EFFECTS_OFF);

export const gameMusicOn = createAction(Names.GAME_MUSIC_ON);
export const gameMusicOff = createAction(Names.GAME_MUSIC_OFF);
