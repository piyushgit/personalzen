import { Platform } from 'react-native';

export const REGULAR_FONT = {
  fontFamily: (Platform.OS === 'ios') ? 'Raleway' : 'Raleway-Regular',
};

export const THIN_FONT = {
  fontFamily: 'Raleway-Thin',
  fontWeight: '100',
};
