const path1img = require('../../../assets/images/goal-setting/path-1.png');
const path2img = require('../../../assets/images/goal-setting/path-2.png');
const path3img = require('../../../assets/images/goal-setting/path-3.png');
const path4img = require('../../../assets/images/goal-setting/path-4.png');

const goal1 = {
	minutes: 10,
	circle: {
		top: 1.11,
		left: 1.6,
		checked: false
	},
	text: {
		position: 'absolute',
		top: 1.09,
		left: 1.25,
		text: 'MOOD\nBOOST'
	},
	path: null
};

const goal2 = {
	minutes: 15,
	circle: {
		top: 1.37,
		left: 2.65,
		checked: false
	},
	text: {
		position: 'absolute',
		top: 1.35,
		left: 1.8,
		text: 'STRESS\nRELIEF'
	},
	path: {
		top: 1.305,
		left: 4,
		image: path1img,
		show: false
	}
};

const goal3 = {
	minutes: 20,
	circle: {
		top: 1.86,
		left: 1.55,
		checked: false
	},
	text: {
		position: 'absolute',
		top: 1.8,
		left: 2.2,
		text: 'POSITIVE\nCHANGE '
	},
	path: {
		top: 1.73,
		left: 2.15,
		image: path2img,
		show: false
	}
};

const goal4 = {
	minutes: 25,
	circle: {
		top: 3.3,
		left: 2.5,
		checked: false
	},
	text: {
		position: 'absolute',
		top: 3.05,
		left: 8.5,
		text: 'NEW OUTLOOK'
	},
	path: {
		top: 2.81,
		left: 2,
		image: path3img,
		show: false
	}
};

const goal5 = {
	minutes: 30,
	circle: {
		top: 7,
		left: 1.39,
		checked: false
	},
	text: {
		position: 'absolute',
		top: 6.2,
		left: 2,
		text: 'PEAK\nPOSITIVITY'
	},
	path: {
		top: 5,
		left: 2.1,
		image: path4img,
		show: false
	}
};

export const goals = [ goal1, goal2, goal3, goal3, goal4, goal5 ];
