
export const TEXT_COLOR = '#4f4f51';

export const BACKGROUND_COLOR_GREY_C1 = '#C1C1C1';
export const BACKGROUND_COLOR_GREY_E5 = '#E5E5E5';
export const BACKGROUND_COLOR_WHITE = '#FFF';
