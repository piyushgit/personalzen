export const REGION_02_KEY = 'region-02';

const regionImage = require('../../../assets/images/world-map/World-Map-02.png');
const regionImageGrayscale = require('../../../assets/images/world-map/World-Map-02-Grayscale.png');
const titleImage = require('../../../assets/images/world-map/title-02.png');
const titleImageUnlocked = require('../../../assets/images/world-map/title-02-unlocked.png');
const introImage = require('../../../assets/images/region-intro/tree-icon.png');

// // Background
const backgroundImage = require('../../../assets/images/regions/forest-region/background.png');
const backgroundGrayscaleImage = require('../../../assets/images/regions/forest-region/background-grayscale.png');
// // Sprites
const spritePositiveImage = require('../../../assets/images/regions/forest-region/sprite-yellow-positive.png');
const spriteNegativeImage = require('../../../assets/images/regions/forest-region/Sprite-yellow-negative.png');
// // Paths
const brownMushroomPathImage = require('../../../assets/images/regions/forest-region/brown-mushroom.png');
const clover1PathImage = require('../../../assets/images/regions/forest-region/clover1.png');
const clover2PathImage = require('../../../assets/images/regions/forest-region/clover2.png');
const mushroom1PathImage = require('../../../assets/images/regions/forest-region/mushroom1.png');
const orangeLeafPathImage = require('../../../assets/images/regions/forest-region/orange-leaf.png');
const orangePetalsPathImage = require('../../../assets/images/regions/forest-region/orange-petals.png');
const redLeafPathImage = require('../../../assets/images/regions/forest-region/red-leaf.png');
const reddishFlowerPathImage = require('../../../assets/images/regions/forest-region/reddish-flower.png');
const whiteMushroomPathImage = require('../../../assets/images/regions/forest-region/white-mushroom.png');
const yellowFlowerPathImage = require('../../../assets/images/regions/forest-region/yellow-flower.png');
const yellowRosePathImage = require('../../../assets/images/regions/forest-region/yellow-rose.png');

export const Region02 = {
  key: REGION_02_KEY,
  region: {
    img: regionImage,
    imgGrayscale: regionImageGrayscale,
    grayscaleOpacity: 0.4,
    top: 2564,
  },
  title: {
    label: 'Arboreal Point',
    img: titleImage,
    top: 2800,
    left: 34,
  },
  titleUnlocked: {
    label: 'Arboreal Point',
    img: titleImageUnlocked,
    top: 2800,
    left: 34,
  },
  intro: {
    img: introImage,
  },
  gameLevel: {
    background: {
      img: backgroundImage,
      imgGrayscale: backgroundGrayscaleImage,
    },
    sprites: {
      positive: {
        img: spritePositiveImage,
        width: 163,
        height: 173,
      },
      negative: {
        img: spriteNegativeImage,
        width: 162,
        height: 175,
      },
    },
    paths: [
      {
        img: brownMushroomPathImage,
        width: 76,
        height: 64,
        rotate: false,
      },
      {
        img: clover1PathImage,
        width: 63,
        height: 87,
        rotate: true,
      },
      {
        img: clover2PathImage,
        width: 63,
        height: 87,
        rotate: true,
      },
      {
        img: mushroom1PathImage,
        width: 55,
        height: 50,
        rotate: false,
      },
      {
        img: orangeLeafPathImage,
        width: 107,
        height: 105,
        rotate: true,
      },
      {
        img: orangePetalsPathImage,
        width: 84,
        height: 83,
        rotate: true,
      },
      {
        img: redLeafPathImage,
        width: 106,
        height: 103,
        rotate: true,
      },
      {
        img: reddishFlowerPathImage,
        width: 68,
        height: 65,
        rotate: true,
      },
      {
        img: whiteMushroomPathImage,
        width: 47,
        height: 75,
        rotate: false,
      },
      {
        img: yellowFlowerPathImage,
        width: 64,
        height: 65,
        rotate: true,
      },
      {
        img: yellowRosePathImage,
        width: 62,
        height: 68,
        rotate: true,
      },
    ],
  },
};
