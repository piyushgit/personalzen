export const REGION_05_KEY = 'region-05';

const regionImage = require('../../../assets/images/world-map/World-Map-05.png');
const regionImageGrayscale = require('../../../assets/images/world-map/World-Map-05-Grayscale.png');
const titleImage = require('../../../assets/images/world-map/title-05.png');
const titleImageUnlocked = require('../../../assets/images/world-map/title-05-unlocked.png');
const introImage = require('../../../assets/images/region-intro/ice-rock.png');

// // BackGroung
const backgroundImage = require('../../../assets/images/regions/mountain-region/background.png');
const backgroundGrayscaleImage = require('../../../assets/images/regions/mountain-region/background-grayscale.png');
// // Sprites
const spritePositiveImage = require('../../../assets/images/regions/mountain-region/sprite-gray-positive.png');
const spriteNegativeImage = require('../../../assets/images/regions/mountain-region/sprite-gray-negative.png');
// // Paths
const berriesPathImage = require('../../../assets/images/regions/mountain-region/berries.png');
const everGreen1PathImage = require('../../../assets/images/regions/mountain-region/ever-green-1.png');
const everGreen2PathImage = require('../../../assets/images/regions/mountain-region/ever-green-2.png');
const icePathImage = require('../../../assets/images/regions/mountain-region/ice.png');
const pinecone1PathImage = require('../../../assets/images/regions/mountain-region/pinecone-1.png');
const pinecone2PathImage = require('../../../assets/images/regions/mountain-region/pinecone-2.png');
const pinecone3PathImage = require('../../../assets/images/regions/mountain-region/pinecone-3.png');
const poinsettaPathImage = require('../../../assets/images/regions/mountain-region/poinsetta.png');
const sproutPathImage = require('../../../assets/images/regions/mountain-region/sprout.png');
const twigPathImage = require('../../../assets/images/regions/mountain-region/twig.png');

export const Region05 = {
  key: REGION_05_KEY,
  region: {
    img: regionImage,
    imgGrayscale: regionImageGrayscale,
    grayscaleOpacity: 1,
    top: 1,
  },
  title: {
    label: 'Zenith Heights',
    img: titleImage,
    top: 80,
    left: 448,
  },
  titleUnlocked: {
    label: 'Zenith Heights',
    img: titleImageUnlocked,
    top: 80,
    left: 448,
  },
  intro: {
    img: introImage,
  },
  gameLevel: {
    background: {
      img: backgroundImage,
      imgGrayscale: backgroundGrayscaleImage,
    },
    sprites: {
      positive: {
        img: spritePositiveImage,
        width: 209,
        height: 184,
      },
      negative: {
        img: spriteNegativeImage,
        width: 217,
        height: 191,
      },
    },
    paths: [
      {
        img: berriesPathImage,
        width: 104,
        height: 73,
        rotate: true,
      },
      {
        img: everGreen1PathImage,
        width: 70,
        height: 123,
        rotate: true,
      },
      {
        img: everGreen2PathImage,
        width: 62,
        height: 100,
        rotate: true,
      },
      {
        img: icePathImage,
        width: 80,
        height: 77,
        rotate: false,
      },
      {
        img: pinecone1PathImage,
        width: 96,
        height: 72,
        rotate: true,
      },
      {
        img: pinecone2PathImage,
        width: 72,
        height: 56,
        rotate: true,
      },
      {
        img: pinecone3PathImage,
        width: 64,
        height: 63,
        rotate: true,
      },
      {
        img: poinsettaPathImage,
        width: 83,
        height: 78,
        rotate: true,
      },
      {
        img: sproutPathImage,
        width: 84,
        height: 64,
        rotate: true,
      },
      {
        img: twigPathImage,
        width: 144,
        height: 56,
        rotate: true,
      },
    ],
  },
};
