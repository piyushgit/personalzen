export const REGION_03_KEY = 'region-03';

const regionImage = require('../../../assets/images/world-map/World-Map-03.png');
const regionImageGrayscale = require('../../../assets/images/world-map/World-Map-03-Grayscale.png');
const titleImage = require('../../../assets/images/world-map/title-03.png');
const titleImageUnlocked = require('../../../assets/images/world-map/title-03-unlocked.png');
const introImage = require('../../../assets/images/region-intro/flower.png');

// // Background
const backgroundImage = require('../../../assets/images/regions/plains-region/background.png');
const backgroundGrayscaleImage = require('../../../assets/images/regions/plains-region/background-grayscale.png');
const textBackground = require('../../../assets/images/tutorial/text-background.png');
// // Sprites
const spritePositiveImage = require('../../../assets/images/regions/plains-region/sprite-green-positive.png');
const spriteNegativeImage = require('../../../assets/images/regions/plains-region/sprite-green-negative.png');
// // Paths
const grassPathImage = require('../../../assets/images/regions/plains-region/grass.png');
const greenBushPathImage = require('../../../assets/images/regions/plains-region/green-bush.png');
const leafPathImage = require('../../../assets/images/regions/plains-region/leaf.png');
const leavesPathImage = require('../../../assets/images/regions/plains-region/leaves.png');
const mushroomPathImage = require('../../../assets/images/regions/plains-region/mushroom.png');
const orangePetalsPathImage = require('../../../assets/images/regions/plains-region/orange-petals.png');
const orangeFlowerPathImage = require('../../../assets/images/regions/plains-region/orange-flower.png');
const reddishFlowerPathImage = require('../../../assets/images/regions/plains-region/reddish-flower.png');
const yellowFlowerPathImage = require('../../../assets/images/regions/plains-region/yellow-flower.png');
const yellowRosePathImage = require('../../../assets/images/regions/plains-region/yellow-rose.png');

// Tutorial
const arrowPath = require('../../../assets/images/tutorial/arrow-path.png');

export const Region03 = {
	key: REGION_03_KEY,
	region: {
		img: regionImage,
		imgGrayscale: regionImageGrayscale,
		grayscaleOpacity: 0.5,
		top: 1945
	},
	title: {
		label: 'Panacea Plains',
		img: titleImage,
		top: 1941,
		left: 446
	},
	titleUnlocked: {
		label: 'Panacea Plains',
		img: titleImageUnlocked,
		top: 1941,
		left: 446
	},
	intro: {
		img: introImage
	},
	gameLevel: {
		background: {
			// img: backgroundImage, Previously set up
			img: backgroundGrayscaleImage,
			imgGrayscale: backgroundGrayscaleImage
		},
		sprites: {
			positive: {
				img: spritePositiveImage,
				width: 163,
				height: 173
			},
			negative: {
				img: spriteNegativeImage,
				width: 162,
				height: 175
			}
		},
		paths: [
			{
				img: grassPathImage,
				width: 48,
				height: 62,
				rotate: false
			},
			{
				img: greenBushPathImage,
				width: 98,
				height: 109,
				rotate: true
			},
			{
				img: leafPathImage,
				width: 64,
				height: 42,
				rotate: true
			},
			{
				img: leavesPathImage,
				width: 76,
				height: 57,
				rotate: true
			},
			{
				img: mushroomPathImage,
				width: 55,
				height: 50,
				rotate: false
			},
			{
				img: orangePetalsPathImage,
				width: 84,
				height: 83,
				rotate: true
			},
			{
				img: orangeFlowerPathImage,
				width: 58,
				height: 67,
				rotate: true
			},
			{
				img: reddishFlowerPathImage,
				width: 68,
				height: 65,
				rotate: true
			},
			{
				img: yellowFlowerPathImage,
				width: 64,
				height: 65,
				rotate: true
			},
			{
				img: yellowRosePathImage,
				width: 62,
				height: 68,
				rotate: true
			}
		],
		tutorial: {
			arrow: {
				img: arrowPath,
				width: 200,
				height: 200
			},
			textBackground: {
				img: textBackground,
				width: 863,
				height: 809
			}
		}
	}
};
