import { Region01, REGION_01_KEY } from './water-region';
import { Region02, REGION_02_KEY } from './forest-region';
import { Region03, REGION_03_KEY } from './plains-region';
import { Region04, REGION_04_KEY } from './desert-region';
import { Region05, REGION_05_KEY } from './mountain-region';
import {
  ACHIEVEMENT_06_KEY,
  ACHIEVEMENT_07_KEY,
  ACHIEVEMENT_08_KEY,
  ACHIEVEMENT_09_KEY,
  ACHIEVEMENT_10_KEY,
} from '../../screens/collection/config';

export {
  REGION_01_KEY,
  REGION_02_KEY,
  REGION_03_KEY,
  REGION_04_KEY,
  REGION_05_KEY,
};

export const RegionStates = {
  locked: 'locked',
  unlocked: 'unlocked',
};

export const REGION_UNLOCK_TIME = {
  [REGION_01_KEY]: 10,
  [REGION_02_KEY]: 15,
  [REGION_03_KEY]: 20,
  [REGION_04_KEY]: 25,
  [REGION_05_KEY]: 30,
};

export const REGION_LINKS = {
  [REGION_01_KEY]: REGION_02_KEY,
  [REGION_02_KEY]: REGION_03_KEY,
  [REGION_03_KEY]: REGION_04_KEY,
  [REGION_04_KEY]: REGION_05_KEY,
  [REGION_05_KEY]: null,
};

const regionsMap = [
  Region05,
  Region04,
  Region03,
  Region02,
  Region01,
];

export const REGION_ACHIEVEMENT_KEY = {
  [REGION_01_KEY]: ACHIEVEMENT_06_KEY,
  [REGION_02_KEY]: ACHIEVEMENT_07_KEY,
  [REGION_03_KEY]: ACHIEVEMENT_08_KEY,
  [REGION_04_KEY]: ACHIEVEMENT_09_KEY,
  [REGION_05_KEY]: ACHIEVEMENT_10_KEY,
};
export default regionsMap;
