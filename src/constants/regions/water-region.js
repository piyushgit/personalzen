export const REGION_01_KEY = 'region-01';

const regionImage = require('../../../assets/images/world-map/World-Map-01.png');
const regionImageGrayscale = require('../../../assets/images/world-map/World-Map-01-Grayscale.png');
const titleImage = require('../../../assets/images/world-map/title-01.png');
const titleImageUnlocked = require('../../../assets/images/world-map/title-01-unlocked.png');
const introImage = require('../../../assets/images/region-intro/lily-pad.png');

// // Background
const backgroundImage = require('../../../assets/images/regions/water-region/background.png');
const backgroundGrayscaleImage = require('../../../assets/images/regions/water-region/background-grayscale.png');
// // Sprites
const spritePositiveImage = require('../../../assets/images/regions/water-region/sprite-blue-positive.png');
const spriteNegativeImage = require('../../../assets/images/regions/water-region/Sprite-blue-negative.png');
// // Paths
const lilyPads1PathImage = require('../../../assets/images/regions/water-region/lily-pads-1.png');
const lilyPads2PathImage = require('../../../assets/images/regions/water-region/lily-pads-2.png');
const lilyPads3PathImage = require('../../../assets/images/regions/water-region/lily-pads-3.png');
const lilyPads4PathImage = require('../../../assets/images/regions/water-region/lily-pads-4.png');
const seaweedPathImage = require('../../../assets/images/regions/water-region/seaweed.png');

export const Region01 = {
  key: REGION_01_KEY,
  region: {
    img: regionImage,
    imgGrayscale: regionImageGrayscale,
    grayscaleOpacity: 0,
    top: 3183,
  },
  title: {
    label: 'Pacifica Bay',
    img: titleImage,
    top: 3990,
    left: 250,
  },
  titleUnlocked: {
    label: 'Pacifica Bay',
    img: titleImageUnlocked,
    top: 3990,
    left: 250,
  },
  intro: {
    img: introImage,
  },
  gameLevel: {
    background: {
      img: backgroundImage,
      imgGrayscale: backgroundGrayscaleImage,
    },
    sprites: {
      positive: {
        img: spritePositiveImage,
        width: 210,
        height: 160,
      },
      negative: {
        img: spriteNegativeImage,
        width: 225,
        height: 147,
      },
    },
    paths: [
      {
        img: lilyPads1PathImage,
        width: 121,
        height: 117,
        rotate: true,
      },
      {
        img: lilyPads2PathImage,
        width: 108,
        height: 72,
        rotate: false,
      },
      {
        img: lilyPads3PathImage,
        width: 144,
        height: 102,
        rotate: true,
      },
      {
        img: lilyPads4PathImage,
        width: 60,
        height: 59,
        rotate: true,
      },
      {
        img: seaweedPathImage,
        width: 51,
        height: 67,
        rotate: false,
      },
    ],
  },
};
