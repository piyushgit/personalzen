export const REGION_04_KEY = 'region-04';

const regionImage = require('../../../assets/images/world-map/World-Map-04.png');
const regionImageGrayscale = require('../../../assets/images/world-map/World-Map-04-Grayscale.png');
const titleImage = require('../../../assets/images/world-map/title-04.png');
const titleImageUnlocked = require('../../../assets/images/world-map/title-04-unlocked.png');
const introImage = require('../../../assets/images/region-intro/cactus.png');

// // Background
const backgroundImage = require('../../../assets/images/regions/desert-region/background.png');
const backgroundGrayscaleImage = require('../../../assets/images/regions/desert-region/background-grayscale.png');
// // Sprites
const spritePositiveImage = require('../../../assets/images/regions/desert-region/sprite-red-positive.png');
const spriteNegativeImage = require('../../../assets/images/regions/desert-region/sprite-red-negative.png');
// // Paths
const agavePathImage = require('../../../assets/images/regions/desert-region/agave.png');
const barrelCactusPathImage = require('../../../assets/images/regions/desert-region/barrel-cactus.png');
const crystalPathImage = require('../../../assets/images/regions/desert-region/crystal.png');
const goldPathImage = require('../../../assets/images/regions/desert-region/gold.png');
const pinkFlowerPathImage = require('../../../assets/images/regions/desert-region/pink-flower.png');
const pricklyPearPathImage = require('../../../assets/images/regions/desert-region/prickly-pear.png');
const rockPathImage = require('../../../assets/images/regions/desert-region/rock.png');
const succulentPathImage = require('../../../assets/images/regions/desert-region/succulent.png');

export const Region04 = {
  key: REGION_04_KEY,
  region: {
    img: regionImage,
    imgGrayscale: regionImageGrayscale,
    grayscaleOpacity: 0.7,
    top: 1124,
  },
  title: {
    label: 'Clarion Sands',
    img: titleImage,
    top: 1153,
    left: 34,
  },
  titleUnlocked: {
    label: 'Clarion Sands',
    img: titleImageUnlocked,
    top: 1153,
    left: 34,
  },
  intro: {
    img: introImage,
  },
  gameLevel: {
    background: {
      img: backgroundImage,
      imgGrayscale: backgroundGrayscaleImage,
    },
    sprites: {
      positive: {
        img: spritePositiveImage,
        width: 162,
        height: 174,
      },
      negative: {
        img: spriteNegativeImage,
        width: 172,
        height: 180,
      },
    },
    paths: [
      {
        img: agavePathImage,
        width: 114,
        height: 101,
        rotate: false,
      },
      {
        img: barrelCactusPathImage,
        width: 112,
        height: 94,
        rotate: false,
      },
      {
        img: crystalPathImage,
        width: 99,
        height: 95,
        rotate: false,
      },
      {
        img: goldPathImage,
        width: 106,
        height: 95,
        rotate: false,
      },
      {
        img: pinkFlowerPathImage,
        width: 44,
        height: 42,
        rotate: true,
      },
      {
        img: pricklyPearPathImage,
        width: 106,
        height: 96,
        rotate: false,
      },
      {
        img: rockPathImage,
        width: 85,
        height: 99,
        rotate: false,
      },
      {
        img: succulentPathImage,
        width: 103,
        height: 88,
        rotate: false,
      },
    ],
  },
};
