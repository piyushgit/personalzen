import React, { PureComponent } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import * as Animatable from 'react-native-animatable';

import { REGULAR_FONT } from '../../constants/styles';
import scale from '../../utils/scale';

const styles = StyleSheet.create({
  textContainer: {
    borderRadius: 11,
    marginHorizontal: scale(10),
    minHeight: 80,
    marginTop: 20,
  },
  text: {
    ...REGULAR_FONT,
    fontSize: scale(17),
    textAlign: 'center',
    color: 'white',
  },
});

export default class Message extends PureComponent {
  static propTypes = {
    text: PropTypes.array.isRequired,
  };

  render() {
    return (
      this.props.text && (
        <View style={styles.textContainer}>
          {this.props.text.map((text, index) => (
            <Animatable.Text
              style={styles.text}
              delay={index * 900}
              animation="fadeIn"
            >
              {text}
            </Animatable.Text>
          ))
          }
        </View>
      )
    );
  }
}
