import * as Animatable from 'react-native-animatable';
import React, { Component } from 'react';

import { Text, Image, StyleSheet, View, Dimensions } from 'react-native';

import { THIN_FONT } from '../../constants/styles';

const background = require('../../../assets/images/entrance/background.png');
const entranceTop = require('../../../assets/images/entrance/entrance-top.png');
const entranceBottom = require('../../../assets/images/entrance/entrance-bottom.png');
const redSpriteLogo = require('../../../assets/images/entrance/sprite-red.png');
const blueSpriteLogo = require('../../../assets/images/entrance/sprite-blue.png');

const window = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'transparent',
		justifyContent: 'space-between'
	},
	containerBackground: {
		position: 'absolute',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%'
	},
	containerCenter: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	containerLogo: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	containerText: {
		flex: 1,
		top: 50,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	title: {
		...THIN_FONT,
		textAlign: 'center'
	}
});

const spriteLeftAnim = {
	0.0: {
		opacity: 0,
		translateX: -100,
		rotate: '-20deg'
	},
	0.75: {
		opacity: 0,
		translateX: -100,
		rotate: '-20deg'
	},
	0.9: {
		rotate: '-20deg'
	},
	1.0: {
		opacity: 1,
		translateX: 0,
		rotate: '0deg'
	}
};

const spriteRightAnim = {
	0.0: {
		opacity: 0,
		translateX: 100,
		rotate: '20deg'
	},
	0.75: {
		opacity: 0,
		translateX: 100,
		rotate: '20deg'
	},
	0.9: {
		rotate: '20deg'
	},
	1.0: {
		opacity: 1,
		translateX: 0,
		rotate: '0deg'
	}
};

const textAnim = {
	0.0: {
		opacity: 0,
		translateY: 100
	},
	0.75: {
		opacity: 0,
		translateY: 100
	},
	1.0: {
		opacity: 1,
		translateY: 0
	}
};

class Splash extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View style={styles.containerBackground}>
					<Image
						style={{
							width: window.width,
							height: window.height
						}}
						source={background}
					/>
				</View>
				<View style={styles.container}>
					<Animatable.Image
						style={{ width: window.width, height: window.height / 3 }}
						animation='fadeInDown'
						source={entranceTop}
						useNativeDriver
					/>

					<View style={styles.containerCenter}>
						<View style={styles.containerLogo}>
							<View style={{ position: 'absolute', left: -15 }}>
								<Animatable.Image
									style={{ width: 150, height: 150 }}
									source={redSpriteLogo}
									animation={spriteRightAnim}
									duration={2000}
									useNativeDriver
								/>
							</View>
							<View style={{ position: 'absolute', right: -15 }}>
								<Animatable.Image
									style={{ width: 150, height: 150 }}
									source={blueSpriteLogo}
									animation={spriteLeftAnim}
									duration={2000}
									useNativeDriver
								/>
							</View>
						</View>
						<View style={{ flex: 1, top: 50 }}>
							<Animatable.View duration={2000} animation={textAnim} useNativeDriver>
								<Text style={[styles.title, { fontSize: window.width / 12 }]}>PERSONAL</Text>
							</Animatable.View>
							<Animatable.View duration={2000} animation={textAnim} useNativeDriver>
								<Text style={[styles.title, { fontSize: window.width / 12 }]}>ZEN</Text>
							</Animatable.View>
						</View>
					</View>

					<Animatable.Image
						style={{ width: window.width, height: window.height / 3 }}
						source={entranceBottom}
						animation='fadeInUp'
						useNativeDriver
					/>
				</View>
			</View>
		);
	}
}

export default Splash;
