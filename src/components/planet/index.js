import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import THREE from '../../lib/three';
import FBXLoader from '../../lib/three-fbx-loader';
import { withTHREE } from '../three';

const background = require('../../../assets/images/menu/backgroud.png');

class Planet extends Component {
  static propTypes = {
    renderer: PropTypes.object.isRequired,
    rotation: PropTypes.object.isRequired,
    position: PropTypes.object.isRequired,
    screen: PropTypes.shape({
      width: PropTypes.number,
      height: PropTypes.number,
    }).isRequired,
  };

  componentDidMount() {
    this.onInit(this.props.renderer);
  }

  onInit({ scene, loadTexture }) {
    const light = new THREE.DirectionalLight(0xffffff, 1);
    light.position.set(1, 1, 1).normalize();
    scene.add(light);

    scene.background = loadTexture(background);

    const loader = new FBXLoader();
    loader.load('low-poly-planet.fbx')
      .then((object3d) => {
        console.log('Loaded scene');
        scene.add(object3d);
        this.planet = object3d;
      })
      .catch((err) => {
        // TODO: Error handling.
        console.log('MODEL PARSE ERROR:', err);
      });
  }

  render() {
    const { rotation, position } = this.props;
    const { width, height } = this.props.screen;

    if (this.planet) {
      this.planet.rotation.copy(rotation);
      this.planet.position.copy(position);
    }

    return (<View style={{ width, height }} />);
  }
}

export default withTHREE(Planet);
