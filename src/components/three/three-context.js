import React from 'react';

export const THREEContext = React.createContext({
  renderer: null,
  scene: null,
  camera: null,
});

export default function withTHREE(Component) {
  return function THREEComponent(props) {
    return (
      <THREEContext.Consumer>
        {context => <Component {...props} renderer={context} />}
      </THREEContext.Consumer>
    );
  };
}
