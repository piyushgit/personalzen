import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';

class ProportionalImage extends Component {
  static propTypes = {
    source: Image.propTypes.source.isRequired,
    style: PropTypes.object,
    ratio: PropTypes.number,
    touchable: PropTypes.bool,
    onPress: TouchableOpacity.propTypes.onPress,
  };

  static defaultProps = {
    style: {},
    ratio: 0,
    touchable: null,
    onPress: null,
  };

  constructor(props) {
    super(props);

    const { ratio } = this.props;
    const asset = resolveAssetSource(this.props.source);
    this.state = {
      width: asset.width * ratio,
      height: asset.height * ratio,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.ratio !== this.props.ratio) {
      this.updateBounds();
    }
  }

  updateBounds() {
    const { ratio } = this.props;
    const asset = resolveAssetSource(this.props.source);
    this.setState({
      width: asset.width * ratio,
      height: asset.height * ratio,
    });
  }

  renderImage(style) {
    const { width, height } = this.state;
    return (
      <Image style={[{ width, height }, style]} source={this.props.source} />
    );
  }

  render() {
    const { width, height } = this.state;
    if (this.props.touchable) {
      return (
        <TouchableOpacity
          style={[{ width, height }, this.props.style]}
          activeOpacity={0.5}
          onPress={this.props.onPress}
        >
          {this.renderImage({ width, height })}
        </TouchableOpacity>
      );
    }
    return this.renderImage(this.props.style);
  }
}

export default ProportionalImage;
