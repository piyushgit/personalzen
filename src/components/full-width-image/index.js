import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
  },
});

class FullWidthImage extends Component {
  static propTypes = {
    source: Image.propTypes.source.isRequired,
    style: PropTypes.object,
  };

  static defaultProps = {
    style: { position: 'absolute', top: 0 },
  };

  constructor(props) {
    super(props);

    this.state = {
      width: 0,
      height: 0,
    };
  }

  onLayout = () => {
    const window = Dimensions.get('window');
    const asset = resolveAssetSource(this.props.source);
    const { width } = window;
    const height = (window.width * asset.height) / asset.width;
    this.setState({
      width,
      height,
    });
  }

  render() {
    const { width, height } = this.state;
    return (
      <View style={[styles.container, this.props.style]} onLayout={() => this.onLayout()}>
        <Image style={{ width, height }} source={this.props.source} />
      </View>
    );
  }
}

export default FullWidthImage;
