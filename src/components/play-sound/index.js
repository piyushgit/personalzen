import { Component } from 'react';
import { AppState } from 'react-native';
import Sound from 'react-native-sound';
import PropTypes from 'prop-types';

class PlaySound extends Component {
  static propTypes = {
    source: PropTypes.number.isRequired,
    running: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);
    const soundUrl = this.props.source;
    this.state = {
      soundUrl,
      appState: AppState.currentState,
    };
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    const callback = (error, sound) => {
      if (error) {
        return;
      }
      sound.setNumberOfLoops(-1);
      sound.setVolume(0);
      if (this.props.running) {
        sound.play();
        this.fadeInFadeOutVolume(sound, 'fadein');
      }
    };
    const sound = new Sound(this.state.soundUrl, error => callback(error, sound));
    this.setState({ sound }); // eslint-disable-line
  }

  componentDidUpdate(prevProps) {
    const { running } = this.props;
    const {
      sound,
      appState,
    } = this.state;
    if (appState === 'background') {
      sound.pause();
    }
    if (!running && running !== prevProps.running) {
      clearInterval(this.intervalId);
      this.fadeInFadeOutVolume(sound, 'fadeout');
    }
    if (appState === 'active' && running && prevProps.running !== running) {
      clearInterval(this.intervalId);
      this.fadeInFadeOutVolume(sound, 'fadein');
      sound.play();
      sound.setCurrentTime(0);
    }
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = (nextAppState) => {
    const { sound } = this.state;
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      sound.play();
    }
    this.setState({ appState: nextAppState });
  }

  fadeInFadeOutVolume = (sound, fade) => {
    if (this.intervalId) {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
    if (!sound) return;
    let dv = -0.1;
    let volume = 1;
    if (fade === 'fadein') {
      volume = 0;
      dv = 0.1;
    }
    const intervalId = setInterval(() => {
      if (!sound) return;
      volume = sound.getVolume() + dv;
      sound.setVolume(volume);
      if (sound.getVolume() >= 1 && fade === 'fadein') {
        clearInterval(intervalId);
        this.intervalId = null;
      }
      if (sound.getVolume() <= 0 && fade === 'fadeout') {
        clearInterval(intervalId);
        this.intervalId = null;
        sound.pause();
      }
    }, 100);
    this.intervalId = intervalId;
  }

  render() {
    return null;
  }
}

export default PlaySound;
