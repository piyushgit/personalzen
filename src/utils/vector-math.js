import THREE from '../lib/three';

const xAxis = new THREE.Vector3(1, 0, 0);
const yAxis = new THREE.Vector3(0, 1, 0);
const zAxis = new THREE.Vector3(0, 0, 1);

export const axis = {
  X: xAxis,
  Y: yAxis,
  Z: zAxis,
};

export function rotateOnAxis(object, rotationAxis, angle) {
  const q1 = new THREE.Quaternion();
  q1.setFromAxisAngle(rotationAxis, angle);
  object.quaternion.premultiply(q1);
  object.rotation.setFromQuaternion(object.quaternion);
}

