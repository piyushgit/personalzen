import { Dimensions } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

export const guidelineBaseWidth = 350;
export const guidelineBaseHeight = 680;

export default function scale(size) {
  return size * (SCREEN_WIDTH / guidelineBaseWidth);
}

export function verticalScale(size) {
  return size * (SCREEN_HEIGHT / guidelineBaseHeight);
}
