import { Platform } from 'react-native';
import * as Keychain from 'react-native-keychain';
import DeviceInfo from 'react-native-device-info';

export const getDeviceId = () => {
  let deviceId = DeviceInfo.getUniqueID();

  if (Platform.OS === 'android') {
    return new Promise(resolve => resolve(deviceId));
  }

  const password = 'personalZen';

  return Keychain.getGenericPassword()
    .then((credentials) => {
      if (credentials) {
        deviceId = credentials.username;
      } else {
        // Store the credentials
        Keychain.setGenericPassword(deviceId, password);
      }

      return deviceId;
    })
    .catch(() => {
      // Keychain couldn't be accessed, so we need to reset the deviceId
      Keychain.setGenericPassword(deviceId, password);
      return deviceId;
    });
};
