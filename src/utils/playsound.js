import Sound from 'react-native-sound';

function PlaySound(SoundUrl) {
  return new Promise((resolve, reject) => {
    const sound = new Sound(SoundUrl, (error) => {
      if (error) {
        reject(Error('erro'));
      } else {
        sound.play(() => {
          sound.release();
          resolve();
        });
      }
    });
  });
}

export default PlaySound;
