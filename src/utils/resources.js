import fs from 'react-native-fs';
import { Platform } from 'react-native';

export function loadResource(filePath, encoding) {
  if (Platform.OS === 'android') {
    return fs.readFileAssets(filePath, encoding);
  }
  return fs.readFile(`${fs.MainBundlePath}/${filePath}`, encoding);
}

