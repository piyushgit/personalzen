import getDistance from './distance';
import scale from './scale';

export default function sparklesPosition(path, region, ratio) {
  const len = getDistance(path[0], path[1]);

  const dx = (path[1].x - path[0].x) / len;
  const dy = (path[1].y - path[0].y) / len;

  const assetWidth = (region.gameLevel.paths[path[0].pathIndex].width * ratio) / 2;
  const assetHeight = (region.gameLevel.paths[path[0].pathIndex].height * ratio) / 2;

  const position = {
    x: path[0].x - ((assetWidth + scale(20)) * dx),
    y: path[0].y - ((assetHeight + scale(20)) * dy),
  };
  return position;
}
