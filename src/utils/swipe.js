import React, { Component } from 'react';
import { View, PanResponder } from 'react-native';
import PropTypes from 'prop-types';

export const swipeDirections = {
  SWIPE_UP: 'SWIPE_UP',
  SWIPE_DOWN: 'SWIPE_DOWN',
  SWIPE_LEFT: 'SWIPE_LEFT',
  SWIPE_RIGHT: 'SWIPE_RIGHT',
};

const swipeConfig = {
  velocityThreshold: 0.01,
  directionalOffsetThreshold: 20,
};

function isValidSwipe(velocity, velocityThreshold, directionalOffset, directionalOffsetThreshold) {
  return Math.abs(velocity) > velocityThreshold
    && Math.abs(directionalOffset) < directionalOffsetThreshold;
}

class GestureRecognizer extends Component {
  static propTypes = {
    onSwipe: PropTypes.func,
    onSwipeUp: PropTypes.func,
    onSwipeDown: PropTypes.func,
    onSwipeLeft: PropTypes.func,
    onSwipeRight: PropTypes.func,
    config: PropTypes.object,
  }

  static defaultProps = {
    onSwipe: null,
    onSwipeUp: null,
    onSwipeDown: null,
    onSwipeLeft: null,
    onSwipeRight: null,
    config: swipeConfig,
  }

  constructor(props, context) {
    super(props, context);
    this.swipeConfig = props.config;
  }

  componentWillMount() {
    const responderEnd = this.handlePanResponderEnd.bind(this);
    const shouldSetResponder = this.handleShouldSetPanResponder.bind(this);
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: shouldSetResponder,
      onMoveShouldSetPanResponder: shouldSetResponder,
      onPanResponderRelease: responderEnd,
      onPanResponderTerminate: responderEnd,
    });
  }

  componentWillReceiveProps(props) {
    this.swipeConfig = Object.assign(swipeConfig, props.config);
  }

  getSwipeDirection(gestureState) {
    const {
      SWIPE_LEFT,
      SWIPE_RIGHT,
      SWIPE_UP,
      SWIPE_DOWN,
    } = swipeDirections;
    const { dx, dy } = gestureState;
    if (this.isValidHorizontalSwipe(gestureState)) {
      return (dx < 0)
        ? SWIPE_LEFT
        : SWIPE_RIGHT;
    } else if (this.isValidVerticalSwipe(gestureState)) {
      return (dy > 0)
        ? SWIPE_DOWN
        : SWIPE_UP;
    }
    return null;
  }

  triggerSwipeHandlers(swipeDirection, gestureState) {
    const {
      onSwipe,
      onSwipeUp,
      onSwipeDown,
      onSwipeLeft,
      onSwipeRight,
    } = this.props;
    const {
      SWIPE_LEFT,
      SWIPE_RIGHT,
      SWIPE_UP,
      SWIPE_DOWN,
    } = swipeDirections;
    if (onSwipe) {
      onSwipe(swipeDirection, gestureState);
    }
    switch (swipeDirection) {
      case SWIPE_LEFT:
        onSwipeLeft(gestureState);
        break;
      case SWIPE_RIGHT:
        onSwipeRight(gestureState);
        break;
      case SWIPE_UP:
        onSwipeUp(gestureState);
        break;
      case SWIPE_DOWN:
        onSwipeDown(gestureState);
        break;
      default:
        break;
    }
  }

  handleShouldSetPanResponder(evt, gestureState) {
    return evt.nativeEvent.touches.length === 1 && !this.gestureIsClick(gestureState);
  }

  gestureIsClick = (gestureState) => {
    const gest = Math.abs(gestureState.dx) < 20;
    return gest;
  }

  handlePanResponderEnd(evt, gestureState) {
    const swipeDirection = this.getSwipeDirection(gestureState);
    this.triggerSwipeHandlers(swipeDirection, gestureState);
  }


  isValidHorizontalSwipe(gestureState) {
    const { vx, dy } = gestureState;
    const { velocityThreshold, directionalOffsetThreshold } = this.swipeConfig;
    return isValidSwipe(vx, velocityThreshold, dy, directionalOffsetThreshold);
  }

  isValidVerticalSwipe(gestureState) {
    const { vy, dx } = gestureState;
    const { velocityThreshold, directionalOffsetThreshold } = this.swipeConfig;
    return isValidSwipe(vy, velocityThreshold, dx, directionalOffsetThreshold);
  }

  render() {
    return (<View {...this.props} {...this.panResponder.panHandlers} />);
  }
}

export default GestureRecognizer;
