import RNAmplitude from 'react-native-amplitude-analytics';
import {
  API_KEY_DEV,
  API_KEY_PROD,
} from '../constants/amplitude';

class Analytics {
  constructor() {
    const apiKey = process.env.NODE_ENV === 'development' ? API_KEY_DEV : API_KEY_PROD;
    if (process.env.NODE_ENV !== 'test') {
      this.amplitude = new RNAmplitude(apiKey);
    }
  }
  eventLog(eventName, data) {
    if (!this.amplitude) {
      return;
    }
    this.amplitude.logEvent(eventName, data);
  }
}

export const analytics = new Analytics();
