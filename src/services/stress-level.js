import client from './client';

export const addStressLevel = stressLevel =>
  client.post('stresslevel', stressLevel)
    .then(response => response.data);
