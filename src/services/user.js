import client from './client';

export const fetchUsers = (page, search) =>
  client.get('users', {
    params: {
      page,
      search,
    },
  })
    .then(response => response.data);

export const updateUser = user =>
  client.put(`users/${user.id}`, user)
    .then(response => response.data);

export const createUser = user =>
  client.post('users', user)
    .then(response => response.data);
