import client from './client';

export const addSwipeSpeed = swipe =>
  client.post('swipes', swipe)
    .then(response => response.data);
