import axios from 'axios';

import { AUTHENTICATE_COMPLETED } from '../actions/auth';
import { getDeviceId } from '../utils/device-id';
import { store } from '../store';

class Client {
  constructor() {
    this.token = null;

    this.client = axios.create({
      baseURL: 'http://app.cloud.researchmodepersonalzen.com/',
    });

    this.client.interceptors.request.use((config) => {
      if (!this.token) {
        return config;
      }

      config.headers.Authorization = `Bearer ${this.token}`;

      return config;
    }, err => Promise.reject(err));

    this.client.interceptors.response.use(response => response, (err) => {
      const request = err.config;

      if (err.response && err.response.status === 401 && !request.retry) {
        request.retry = true;

        return getDeviceId()
          .then(deviceId => this.client.post('authentication', { deviceId }))
          .then((response) => {
            const { accessToken, isSuper } = response.data;

            const payload = {
              token: accessToken,
              isSuper,
            };

            store.dispatch({
              type: AUTHENTICATE_COMPLETED,
              payload,
            });

            request.headers.Authorization = `Bearer ${accessToken}`;

            return axios(request);
          })
          .catch(() => Promise.reject(err));
      }

      return Promise.reject(err);
    });
  }

  setToken(token) {
    this.token = token;
  }

  get(url, config) {
    return this.client.get(url, config);
  }

  post(url, data, config) {
    return this.client.post(url, data, config);
  }

  put(url, data, config) {
    return this.client.put(url, data, config);
  }

  patch(url, data, config) {
    return this.client.patch(url, data, config);
  }

  delete(url, config) {
    return this.client.delete(url, config);
  }
}

export default new Client();
