import * as Firebase from 'firebase';
var config = {
	apiKey: 'AIzaSyCFw1KmM8AWygb6CHcIVEAvLzGcBKY1rJA',
	authDomain: 'personalzen-authenticate-users.firebaseapp.com',
	databaseURL: 'https://personalzen-authenticate-users.firebaseio.com',
	projectId: 'personalzen-authenticate-users'
};
export const firebaseRef = Firebase.initializeApp(config);
