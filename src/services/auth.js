import client from './client';

export const authenticate = authentication =>
  client.post('authentication', authentication)
    .then(response => response.data);
