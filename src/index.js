import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import React from 'react';
import { StatusBar, Image } from 'react-native';
//import { Sentry } from 'react-native-sentry';

import { persistor, store } from './store';

import ReduxNavigator from './redux-navigator';
import Sounds from './sound-player';
//Sentry.config('https://99fb94d2dfb146e596e2ee0485fb17erd@sentry.io/1288130').install();

const backGround = require('../assets/images/entrance/background.png');

console.disableYellowBox = true;

const App = () => (
	<Provider style={{ position: 'absolute' }} store={store}>
		{/* FIXME: Create loading component */}
		<PersistGate loading={null} persistor={persistor}>
			<Image
				source={backGround}
				style={{
					position: 'absolute',
					height: '100%',
					width: '100%'
				}}
			/>
			<StatusBar hidden />
			<ReduxNavigator />

			<Sounds />
		</PersistGate>
	</Provider>
);
export default App;
