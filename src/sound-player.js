import React from 'react';
import { connect } from 'react-redux';
import PlaySound from './components/play-sound';
import { GAMEPLAYMUSIC, INTHENOW } from './reducers/sounds';
import { stopSound } from './actions';

const GamePlayMusic = require('../assets/audio/GameplayMusic.m4a');
const inthenow = require('../assets/audio/INTHENOW_15Sec.m4a');

const SoundPlayer = props => [
	<PlaySound source={inthenow} running={props.sounds[INTHENOW].playing} />,
	<PlaySound source={GamePlayMusic} running={props.sounds[GAMEPLAYMUSIC].playing} />
];

const mapDispatchToProps = {
	stopSound
};

function mapStateToProps(state){
	return {
		sounds: state.sounds
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(SoundPlayer);
