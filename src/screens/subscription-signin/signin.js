import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Dimensions, TextInput, KeyboardAvoidingView } from 'react-native';
import { Text, Item } from 'native-base';
import { firebaseRef } from '../../services/firebase';
import Icon from 'react-native-vector-icons/Ionicons';

const EntranceTopImage = require('../../../assets/images/entrance/entrance-top.png');
const EntranceBottomImage = require('../../../assets/images/entrance/entrance-bottom.png');
const spriteBlueImage = require('../../../assets/images/entrance/sprite-blue.png');
const spriteRedImage = require('../../../assets/images/entrance/sprite-red.png');

const window = Dimensions.get('window');

export default class Signin extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			ButtonStateHolder: true,
			status: ''
		};
		this._singin = this._singin.bind(this);
		// this._forgotPassword = this._forgotPassword.bind(this);
	}
	_singin() {
		firebaseRef
			.auth()
			.signInWithEmailAndPassword(this.state.email, this.state.password)
			.then(() => this.props.navigation.navigate('MainMenu'))
			.catch((error) => {
				alert('User Does not Exist!!');
			});
	}
	// _forgotPassword = (email) => {
	// 	firebaseRef
	// 		.auth()
	// 		.sendPasswordResetEmail(email)
	// 		.then(function(user) {
	// 			alert('Please check your email..');
	// 		})
	// 		.catch(function(error) {
	// 			console.log(error);
	// 		});
	// };
	render() {
		return (
			<View style={styles.container}>
				<Image style={styles.topImage} source={EntranceTopImage} />
				<View style={styles.viewSpriteCenter}>
					<View style={styles.viewSpriteLogo}>
						<Image
							style={{
								position: 'absolute',
								right: -15,
								height: 150,
								width: 150
							}}
							source={spriteBlueImage}
						/>
						<Image
							style={{
								position: 'absolute',
								left: -15,
								height: 150,
								width: 150
							}}
							source={spriteRedImage}
						/>
					</View>
				</View>
				<KeyboardAvoidingView behavior="padding" style={styles.viewSignIn}>
					<Item style={{ marginBottom: 10 }}>
						<Icon style={styles.iconStyle} size={25} name="ios-mail" />
						<TextInput
							style={styles.textInput}
							onChangeText={(text) => this.setState({ email: text })}
							value={this.state.email}
							placeholder="Email"
							returnKeyType="next"
							autoCapitalize="none"
							autoFocus="true"
							autoCorrect={false}
							keyboardType="email-address"
							placeholderTextColor="#82589F"
							onSubmitEditing={() => this.emailInput.focus()}
						/>
					</Item>
					<Item style={{ marginBottom: 10 }}>
						<Icon style={styles.iconStyle} size={25} name="ios-lock" />
						<TextInput
							style={styles.textInput}
							onChangeText={(text) => this.setState({ password: text, ButtonStateHolder: false })}
							value={this.state.password}
							placeholder="Password"
							returnKeyType="next"
							secureTextEntry={true}
							placeholderTextColor="#82589F"
							ref={(inputEmail) => (this.emailInput = inputEmail)}
							// onSubmitEditing={() => this.passwordInput.focus()}
						/>
					</Item>
					<View style={styles.viewButtonStyle}>
						<TouchableOpacity
							block
							style={[
								styles.buttonStyle,
								{ backgroundColor: this.state.ButtonStateHolder ? '#b2bec3' : '#82589F' }
							]}
							activeOpacity={0.5}
							disabled={this.state.ButtonStateHolder}
							onPress={this._singin}
						>
							<Text style={{ color: '#fff', textAlign: 'center' }}>Sign In</Text>
						</TouchableOpacity>
						<TouchableOpacity
							block
							style={styles.buttonStyle}
							onPress={() => this.props.navigation.navigate('Subscription')}
						>
							<Text style={{ color: '#fff', textAlign: 'center' }}>Cancel</Text>
						</TouchableOpacity>
					</View>
					{/* <View style={styles.viewForgotPassword}>
						<TouchableOpacity onPress={this._forgotPassword(this.state.email)}>
							<Text style={{ color: '#82589F' }}>Forgot Your Password?</Text>
						</TouchableOpacity>
					</View> */}
				</KeyboardAvoidingView>
				<View>
					<Image style={styles.bottomImage} source={EntranceBottomImage} />
					<Text
						style={{
							textAlign: 'center',
							paddingLeft: 8,
							paddingRight: 8,
							paddingBottom: 25,
							color: '#fff',
							fontSize: 12
						}}
					/>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		// flex: 1
		height: Dimensions.get('screen').height,
		width: Dimensions.get('screen').width
	},
	topImage: {
		height: window.height / 3,
		width: window.width
	},
	viewSpriteCenter: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		marginTop: -80
	},
	viewForgotPassword: {
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'row'
	},
	bottomImage: {
		position: 'absolute',
		height: window.height / 3,
		width: window.width,
		bottom: 0
	},
	viewSpriteLogo: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	viewSignIn: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	iconStyle: {
		color: '#82589F',
		marginLeft: 15
	},
	textInput: {
		color: '#82589F',
		borderBottomColor: '#82589F',
		padding: 7,
		height: 30,
		width: 250,
		fontSize: 15,
		opacity: 0.6,
		fontFamily: 'Arial'
	},
	buttonStyle: {
		height: 30,
		width: window.width / 3,
		backgroundColor: '#82589F',
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center'
	},
	viewButtonStyle: {
		flex: 1,
		flexDirection: 'row',
		paddingLeft: 20,
		paddingRight: 20,
		justifyContent: 'space-between'
	}
});
