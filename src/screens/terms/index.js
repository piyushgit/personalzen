import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import ProportionalImage from '../../components/proportional-image';
import { REGULAR_FONT } from '../../constants/styles';
import scale, { verticalScale } from '../../utils/scale';

const backButtonIcon = require('../../../assets/images/stress-tracking/nav-buttom-back.png');

const styles = StyleSheet.create({
	container: {
		position: 'absolute',
		width: '100%',
		height: '100%',
		backgroundColor: 'white',
		paddingHorizontal: scale(25),
		paddingTop: verticalScale(55)
	},
	title: {
		...REGULAR_FONT,
		fontSize: scale(15),
		marginTop: verticalScale(15),
		marginBottom: verticalScale(10)
	},
	termsText: {
		...REGULAR_FONT,
		fontSize: scale(13),
		marginVertical: verticalScale(10)
	},
	containerScroll: {
		height: verticalScale(540)
	},
	backButton: {
		paddingVertical: verticalScale(20)
	}
});

export default class Terms extends Component {
	static propTypes = {
		navigation: PropTypes.shape().isRequired
	};

	handleBackButton = () => {
		this.props.navigation.goBack();
	};

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.containerScroll}>
					<ScrollView>
						<Text style={styles.title}>TERMS OF USE AND SERVICE.</Text>
						<Text style={styles.termsText}>
							By using this application (“APP”), you signify your Assent and Agreement to these Terms of
							Use. If you do not agree to these Terms of Use, please discontinue use of this APP
							immediately.
						</Text>
						<Text style={styles.title}>RESTRICTIONS ON USE OF MATERIALS.</Text>
						<Text style={styles.termsText}>
							Materials in this APP are copyrighted and all rights are reserved. Text, graphics,
							databases, HTML code, and other intellectual property are protected by US and International
							Copyright Laws, and may not be copied, reprinted, published, reengineered, translated,
							hosted, or otherwise distributed by any means without explicit permission. All of the
							trademarks on this APP are trademarks of Personal Zen or of other owners used with their
							permission.
						</Text>
						<Text style={styles.title}>RESERVATION OF RIGHTS. </Text>
						<Text style={styles.termsText}>
							Personal Zen reserves any rights not expressly granted herein.
						</Text>
						<Text style={styles.title}>LICENSE AND USE. </Text>
						<Text style={styles.termsText}>
							Personal Zen grants you a non-exclusive, non-transferable license to use this APP subject to
							these Terms and Conditions. Personal Zen is the author and/or authorized sub-licensor of
							certain materials owned and created by Personal Zen or Personal Zen’s licensors. Personal
							Zen grants you a non-transferable, non-exclusive license to use the information, images and
							other content contained on this APP (the “Materials”), solely for internal use by your
							business or for your own personal use, only with one central processing unit at any one
							time. You may not copy, reverse engineer, translate, port, modify or make derivative works
							of the Materials. You may not rent, disclose, publish, sell, assign, lease, sublicense,
							market, or transfer the Materials or use them in any manner not expressly authorized by this
							Agreement. You shall not derive or attempt to derive the source code, source files or
							structure of all or any portion of the Materials by reverse engineering, disassembly,
							decompilation or any other means. You shall not use the Materials to operate a service
							bureau or for any other use involving the processing of data of other persons or entities.
							Personal Zen and/or Personal Zen’s licensors retains all ownership rights in the Materials.
							The Materials are copyrighted and may not be copied, even if modified or merged with other
							Materials. You shall not alter or remove any copyright notice or proprietary legend
							contained in or on the Materials. You agree not to remove any proprietary rights notices of
							Personal Zen and/or its licensors from the Materials.
						</Text>
						<Text style={styles.termsText}>
							You agree to use information obtained from Personal Zen’s databases only for your own
							private use or the internal purposes of your home or business, provided that it does not
							involve the selling or brokering of information, and in no event cause or permit to be
							published, printed, downloaded, transmitted, distributed, reengineered, or reproduced in any
							form or any part of the APP (whether directly or in condensed, selective or tabulated form)
							whether for resale, republishing, redistribution, viewing, or otherwise.
						</Text>
						<Text style={styles.termsText}>
							Nevertheless, you may on an occasional limited basis download or print out individual pages
							of information that have been individually selected, to meet a specific, identifiable need
							for information which is for your personal use only, or is for use in your business only
							internally, on a confidential basis. You may make such limited number of duplicates of any
							output, both in machine-readable or hard copy form, as may be reasonable for these purposes
							only. Nothing herein shall authorize you to create any database, directory or hard copy
							publication of or from the databases, whether for internal or external distribution or use.
						</Text>
						<Text style={styles.title}>OWNERSHIP.</Text>
						<Text style={styles.termsText}>
							The Materials are copyrighted and may not be copied, reproduced, modified, published,
							uploaded, posted, or distributed in any way, without Personal Zen’s prior written consent.
							Except as expressly provided herein, Personal Zen does not grant any express or implied
							right to you under any copyrights, trademarks, patents or trade secret information. Personal
							Zen warrants, and you accept, that Personal Zen is the owner of the copyright of all
							material available from time to time through Personal Zen. Personal Zen and its contributors
							reserve all rights and no intellectual property rights are conferred by this agreement.
						</Text>
						<Text style={styles.title}>RESTRICTIONS.</Text>
						<Text style={styles.termsText}>
							The Materials do not include the design or layout of the APP or the pages located therein.
							Elements of these APP and the pages therein are protected by trade dress and other laws and
							may not be copied or imitated in whole or in part. No logo, graphic, sound or image from any
							of the APPS may be copied or retransmitted unless expressly permitted by Personal Zen.
						</Text>
						<Text style={styles.title}>TRADEMARKS.</Text>
						<Text style={styles.termsText}>
							Products and services referenced herein are protected by either trademarks or registered
							trademarks of Personal Zen. Other product and company names mentioned herein (if any) may be
							protected by the trademarks of their respective owners.
						</Text>
						<Text style={styles.title}>LINKS AND MARKS.</Text>
						<Text style={styles.termsText}>
							The owner of this APP and/or in the Materials is not necessarily affiliated with other
							applications and is not responsible for their content. Any application and/or Web site that
							is linked to this APP is for your convenience only and you access them at your own risk.
							Links to other material or references to products, services or publications other than those
							of Personal Zen and its subsidiaries and affiliates of this APP and/or in the Materials, do
							not imply the endorsement or approval of such APP, products, services or publications by
							Personal Zen or its subsidiaries and affiliates.
						</Text>
						<Text style={styles.termsText}>
							Certain names, graphics, logos, icons, designs, words, titles or phrases at this APP may
							constitute trade names, trademarks or service marks of Personal Zen or of other entities.
							The display of trademarks on this APP (if any) does not imply that a license of any kind has
							been granted. Any unauthorized downloading, re-transmission, or other copying of
							modification of trademarks and/or the contents herein may be a violation of federal common
							law trademark and/or copyright laws and could subject the copier to legal action.
						</Text>
						<Text style={styles.title}>CONFIDENTIALITY OF INFORMATION. </Text>
						<Text style={styles.termsText}>
							You agree to treat as strictly private and confidential any information which you may have
							received from Personal Zen, and all information to which you have access and will not cause
							or permit any such information to be communicated, copied or otherwise divulged to any other
							person whatsoever.
						</Text>
						<Text style={styles.title}>PRODUCTS AND SERVICES. </Text>
						<Text style={styles.termsText}>
							1. NOT FOR MINORS. The products, services and information on Personal Zen are to be used
							only by persons 18 years of age and older. Although these products, services and information
							do not contain adult content as such, the product is not intended for minors nor is the
							underlying information and services featured here intended for use by minors.
						</Text>
						<Text style={styles.termsText}>
							2. NO MEDICAL ADVICE. This APP and/or Materials, are not intended to diagnose, treat, cure,
							or prevent any disease, and are no substitute for medical advice. You should consult your
							physician and follow his advice. The statements made within this APP have not been evaluated
							by any governmental agency.
						</Text>
						<Text style={styles.termsText}>
							3.ENTERTAINMENT PURPOSES ONLY. Our products are for informational and entertainment purposes
							only. They are not intended to treat or cure any physical or mental conditions you may have.
							You agree that our products, services and information are designed solely for
							self-improvement in an informational and entertaining way. They are not intended as a
							replacement for medical or psychological treatment. No medical claims are intended express
							or implied.
						</Text>
						<Text style={styles.termsText}>
							You should consult with your physician first before using our APP. If you are taking
							prescription medication, please check with your doctor before trying the APP.
						</Text>
						<Text style={styles.termsText}>
							DO NOT USE THIS APP: (I) WHILE UNDER THE INFLUENCE OF ALCOHOL OR OTHER MOOD ALTERING
							SUBSTANCES, WHETHER THEY ARE LEGAL OR ILLEGAL; (II) WHILE DRIVING OR OPERATING HEAVY
							MACHINERY.
						</Text>
						<Text style={styles.termsText}>
							You assume all risks in using our APP, waving any claims against Personal Zen and its,
							executives, employees, consultants and their affiliates for any and all mental or physical
							injuries. You also agree to assume liabilities when allowing other persons access to this
							products.
						</Text>
						<Text style={styles.termsText}>
							In no case will Personal Zen be liable for chance, accidental, special, direct or indirect
							damages resulting from use, misuse or defect of its APP and/or Materials and/or products,
							instructions or documentation.
						</Text>
						<Text style={styles.termsText}>
							These Terms of Use will apply to every access to Personal Zen. Personal Zen reserves the
							right to issue revisions to these Terms of Use by publishing a revised version of this
							document on this APP: that version will then apply to all use by you following the date of
							publication. Each access of information from Personal Zen will be a separate, discrete
							transaction based on the then prevailing terms.
						</Text>
						<Text style={styles.title}>LOCAL LAW. </Text>
						<Text style={styles.termsText}>
							This APP is operated from a location in the State of [New York], United States of America.
							Personal Zen makes no representation that the Materials are appropriate for use in other
							locations. If you access this application from other locations, you are responsible for
							compliance with local laws. Any user of this APP agrees to subject himself to the courts of
							the State of New York in the County of New York and hereby agrees to waive his rights to a
							jury trial.
						</Text>
						<Text style={styles.title}>REPRESENTATIONS AND WARRANTIES. </Text>
						<Text style={styles.termsText}>
							You represent and warrant to Personal Zen that: (a) you possess the legal right and ability
							to enter into this Agreement; (b) all information submitted by you to the APP is true and
							accurate; (c) you are at least 18 years old; and (d) you will not use the APP for any
							purpose that is unlawful or prohibited by this Agreement.
						</Text>
						<Text style={styles.title}>DISCLAIMER OF WARRANTIES. </Text>
						<Text style={styles.termsText}>
							THE MATERIALS IN THIS APP ARE PROVIDED “AS IS” AND WITHOUT WARRANTIES OF ANY KIND EITHER
							EXPRESS OR IMPLIED. PERSONAL Zen DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING,
							BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
							PURPOSE. PERSONAL ZEN DOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE MATERIALS WILL BE
							UNINTERRUPTED OR ERROR-FREE, THAT DEFECTS WILL BE CORRECTED, OR THAT THIS APP OR THE SERVER
							THAT MAKES IT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. PERSONAL ZEN DOES
							NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE
							MATERIALS IN THIS APP IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE.
							YOU (AND NOT PERSONAL ZEN) ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR OR
							CORRECTION. APPLICABLE LAW MAY NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE
							EXCLUSION MAY NOT APPLY TO YOU.
						</Text>
						<Text style={styles.termsText}>
							UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL PERSONAL ZEN OR ANY
							OF ITS AFFILIATES BE LIABLE FOR ANY SPECIAL OR CONSEQUENTIAL DAMAGES THAT RESULT FROM THE
							USE OF, OR THE INABILITY TO USE, THE MATERIALS IN THIS APP, EVEN IF PERSONAL ZEN OR A
							PERSONAL ZEN AUTHORIZED REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
							APPLICABLE LAW MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR
							CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IN NO
							EVENT SHALL PERSONAL ZEN’S TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF
							ACTION (WHETHER IN CONTRACT, TORT, INCLUDING BUT NOT LIMITED TO, NEGLIGENCE OR OTHERWISE)
							EXCEED THE AMOUNT PAID BY YOU, IF ANY, FOR ACCESSING THIS APP.
						</Text>
						<Text style={styles.title}>ERRORS AND CORRECTIONS. </Text>
						<Text style={styles.termsText}>
							THE MATERIALS PUBLISHED COULD INCLUDE TECHNICAL INACCURACIES AND/OR TYPOGRAPHICAL ERRORS.
							PERSONAL ZEN AND/OR ITS LICENSORS MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE MATERIALS AT
							ANY TIME.
						</Text>
						<Text style={styles.termsText}>
							FACTS AND INFORMATION ON THIS APP AND/OR IN THE MATERIALS ARE BELIEVED TO BE ACCURATE AT THE
							TIME THEY WERE PLACED ON THE APP AND/OR IN THE MATERIALS. CHANGES MAY BE MADE AT ANY TIME
							WITHOUT PRIOR NOTICE. ALL DATA PROVIDED ON THIS APP AND/OR IN THE MATERIALS IS TO BE USED
							FOR INFORMATION PURPOSES ONLY. THE INFORMATION CONTAINED ON THIS APP AND/OR IN THE MATERIALS
							AND PAGES WITHIN, IS NOT INTENDED TO PROVIDE SPECIFIC LEGAL, FINANCIAL OR TAX ADVICE, OR ANY
							OTHER ADVICE, WHATSOEVER, FOR ANY INDIVIDUAL OR COMPANY AND SHOULD NOT BE RELIED UPON IN
							THAT REGARD. THE SERVICES DESCRIBED ON THIS APP AND/OR IN THE MATERIALS ARE ONLY OFFERED IN
							JURISDICTIONS WHERE THEY MAY BE LEGALLY OFFERED. INFORMATION PROVIDED IN OUR APP AND/OR IN
							THE MATERIALS IS NOT ALL-INCLUSIVE, AND IS LIMITED TO INFORMATION THAT IS MADE AVAILABLE TO
							PERSONAL ZEN AND SUCH INFORMATION SHOULD NOT BE RELIED UPON AS ALL-INCLUSIVE OR ACCURATE.
						</Text>
						<Text style={styles.title}>LIMITATION OF DAMAGES. </Text>
						<Text style={styles.termsText}>
							IN NO EVENT WILL PERSONAL ZEN OR ANY OF ITS AFFILIATES BE LIABLE FOR ANY CONSEQUENTIAL,
							INDIRECT, INCIDENTAL, OR SPECIAL DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION DAMAGES
							FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, LOSS OF OR UNAUTHORIZED ACCESS TO INFORMATION,
							AND THE LIKE, EVEN IN THE EVENT OF FAULT, TORT, BREACH OF CONTRACT, OR BREACH OF WARRANTY,
							AND EVEN IF PERSONAL ZEN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
						</Text>
						<Text style={styles.title}>INDEMNITY. </Text>
						<Text style={styles.termsText}>
							You agree to indemnify and hold us harmless, and pay our attorney’s fees and costs, if we
							become liable for or incur any damages in connection with your breach of this Agreement. You
							may not settle any dispute without our prior consent, which may only be given in a
							non-electronic writing signed by an authorized representative of Personal Zen.
						</Text>
						<Text style={styles.title}>TERMINATION. </Text>
						<Text style={styles.termsText}>
							This Terms of Use agreement is effective until terminated by either party. You may terminate
							this agreement at any time by destroying all materials obtained from any and all Personal
							Zen APP(s) and all related documentation and all copies and installations thereof, whether
							made under the terms of this agreement or otherwise. This agreement will terminate
							immediately without notice at Personal Zen’s sole discretion, should you fail to comply with
							any term or provision of this agreement. Upon termination, you must destroy all materials
							obtained from this APP and any and all other Personal Zen APP(s) and all copies thereof,
							whether made under the terms of this agreement or otherwise.
						</Text>
					</ScrollView>
				</View>
				<TouchableOpacity style={styles.backButton} onPress={this.handleBackButton}>
					<ProportionalImage style={{ tintColor: 'black' }} source={backButtonIcon} ratio={0.5} />
				</TouchableOpacity>
			</View>
		);
	}
}
