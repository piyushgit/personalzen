import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import moment from 'moment';

import FullWidthImage from '../../components/full-width-image';
import ProportionalImage from '../../components/proportional-image';

import { updateStressLevel, updateAchievementStressLevel } from '../../actions';

import { REGULAR_FONT } from '../../constants/styles';
import scale from '../../utils/scale';
import { analytics } from '../../utils/analytics';
import { ACHIEVEMENT_19_KEY } from '../collection/config';

import { addStressLevel } from '../../services/stress-level';

const background = require('../../../assets/images/stress-tracking/background.png');
const button = require('../../../assets/images/stress-tracking/circle-buttom.png');
const buttonSelected = require('../../../assets/images/stress-tracking/circle-buttom-selected.png');
const leftButton = require('../../../assets/images/tutorial/nav-buttom-left.png');
const rightButton = require('../../../assets/images/tutorial/nav-buttom-right.png');

const styles = StyleSheet.create({
  fullWidthImage: {
    position: 'absolute',
  },
  containerStressButons: {
    flexDirection: 'row',
    position: 'absolute',
    left: scale(59),
    paddingRight: scale(35),
    top: scale(255),
    flex: 1,
  },
  containerStressButonsText: {
    flexDirection: 'row',
    position: 'absolute',
    top: scale(300),
    width: '76%',
    left: scale(32),
    justifyContent: 'space-between',
    // backgroundColor: 'red',
  },
  stressButton: {
    flex: 1,
  },
  stressLevelText: {
    ...REGULAR_FONT,
    transform: [{ rotate: '-51deg' }],
    color: 'white',
    fontSize: scale(12),
  },
  containerText: {
    position: 'absolute',
    top: scale(450),
    alignItems: 'center',
    width: '100%',
  },
  BottomText: {
    ...REGULAR_FONT,
    color: 'white',
    fontSize: scale(25),
  },
  backButton: {
    position: 'absolute',
    bottom: scale(10),
    left: scale(10),
  },
  containerTutorialButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: scale(75),
  },
});

class StressTracking extends Component {
  static propTypes = {
    settings: PropTypes.object.isRequired,
    achievements: PropTypes.object.isRequired,
    updateStressLevel: PropTypes.func.isRequired,
    updateAchievementStressLevel: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
    nav: PropTypes.shape().isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      stressLevel: '',
      isTutorial: false,
      show: true,
      isGoal: false,
    };
  }

  componentWillMount() {
    const { stressLevel } = this.props.settings;
    const { navigation } = this.props;
    const isTutorial = navigation.getParam('isTutorial');
    const regionKey = navigation.getParam('regionKey');
    const isGoal = navigation.getParam('isGoal');

    this.setState({
      stressLevel,
      isTutorial,
      regionKey,
      isGoal,
    });
  }

  componentDidUpdate() {
    const { index, routes } = this.props.nav;
    const { routeName } = routes[index];

    const show = routeName === 'StressTracking';

    if (show && !this.state.show) {
      this.setState({ show: true }); // eslint-disable-line
    }
  }

  onPressStressLevel(level) {
    const { isTutorial, regionKey, isGoal } = this.state;
    const previousStressLevel = this.props.settings.stressLevel;
    const { achievements } = this.props;

    const request = {
      stressLevel: level,
      date: moment().toISOString(),
    };

    addStressLevel(request).then(response => {
      console.log(response);
    });

    this.setState({
      stressLevel: level,
    });

    if (
      !achievements[ACHIEVEMENT_19_KEY].achieved &&
      level < previousStressLevel
    ) {
      this.props.updateAchievementStressLevel();
    }

    this.props.updateStressLevel({
      stressLevel: level,
    });

    if (regionKey) {
      setTimeout(() => {
        this.setState({ show: false });
      }, 2000);
      this.props.navigation.navigate('Settings', {
        regionKey: this.state.regionKey,
      });
    } else if (isTutorial) {
      this.props.navigation.navigate('Settings', {
        isTutorial: true,
      });
    } else if (isGoal) {
      this.props.navigation.navigate('Settings', {
        isGoal: true,
      });
    } else {
      setTimeout(() => {
        this.props.navigation.navigate('MainMenu');
        analytics.eventLog(`Stress Level Selection: ${level}`);
      }, 500);
    }
  }

  handleLeftButton = () => {
    this.props.navigation.goBack();
  };

  handleRightButton = () => {
    const { isTutorial, regionKey, isGoal } = this.state;
    if (regionKey) {
      setTimeout(() => {
        this.setState({ show: false });
      }, 2000);
      this.props.navigation.navigate('Settings', {
        regionKey: this.state.regionKey,
      });
    } else if (isGoal) {
      setTimeout(() => {
        this.props.navigation.navigate('Settings', {
          isGoal: true,
        });
      }, 500);
    } else if (isTutorial) {
      setTimeout(() => {
        this.props.navigation.navigate('Settings', {
          isTutorial: true,
        });
      }, 500);
    } else {
      this.props.navigation.navigate('MainMenu');
    }
  };

  render() {
    const { stressLevel, show } = this.state;
    if (!show) return null;
    return (
      <View
        style={{
          justifyContent: 'flex-end',
          height: '100%',
          backgroundColor: '#9fcac7',
        }}
      >
        <View>
          <FullWidthImage
            style={{ position: 'relative' }}
            source={background}
          />
          <View style={styles.containerStressButons}>
            <TouchableOpacity
              style={styles.stressButton}
              onPress={() => this.onPressStressLevel(1)}
            >
              <ProportionalImage
                style={{}}
                source={stressLevel === 1 ? buttonSelected : button}
                ratio={0.5}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.stressButton}
              onPress={() => this.onPressStressLevel(2)}
            >
              <ProportionalImage
                style={{}}
                source={stressLevel === 2 ? buttonSelected : button}
                ratio={0.5}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.stressButton}
              onPress={() => this.onPressStressLevel(3)}
            >
              <ProportionalImage
                style={{}}
                source={stressLevel === 3 ? buttonSelected : button}
                ratio={0.5}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.stressButton}
              onPress={() => this.onPressStressLevel(4)}
            >
              <ProportionalImage
                style={{}}
                source={stressLevel === 4 ? buttonSelected : button}
                ratio={0.5}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.containerStressButonsText}>
            <Text style={styles.stressLevelText}>LOW</Text>
            <Text style={styles.stressLevelText}>HIGH</Text>
          </View>
          <View style={styles.containerText}>
            <Text style={styles.BottomText}>SELECT YOUR</Text>
            <Text style={styles.BottomText}>CURRENT STRESS LEVEL</Text>
          </View>
          <SafeAreaView style={styles.backButton}>
            <View style={styles.containerTutorialButtons}>
              <TouchableOpacity onPress={this.handleLeftButton}>
                <ProportionalImage source={leftButton} ratio={0.4} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this.handleRightButton}>
                <ProportionalImage source={rightButton} ratio={0.4} />
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
    achievements: state.achievements,
    nav: state.nav,
  };
}

const mapDispatchToProps = {
  updateStressLevel,
  updateAchievementStressLevel,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StressTracking);
