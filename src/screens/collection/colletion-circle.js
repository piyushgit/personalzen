import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

import { REGULAR_FONT } from '../../constants/styles';
import scale, { verticalScale } from '../../utils/scale';
import ProportionalImage from '../../components/proportional-image';

const styles = StyleSheet.create({
	container: {
		position: 'absolute',
		width: '100%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	touchableBackground: {
		position: 'absolute',
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		height: '100%'
	},
	circle: {
		width: scale(300),
		height: scale(300),
		backgroundColor: '#ffffff',
		opacity: 1,
		borderRadius: scale(300),
		alignItems: 'center'
	},
	icon: {
		paddingTop: scale(15)
	},
	containerTexts: {
		width: '100%',
		padding: scale(30)
	},
	achievement: {
		...REGULAR_FONT,
		fontSize: scale(14),
		textAlign: 'center',
		marginTop: scale(10)
	},
	name: {
		...REGULAR_FONT,
		textAlign: 'center',
		fontSize: scale(25),
		letterSpacing: scale(2)
	},
	description: {
		...REGULAR_FONT,
		fontSize: scale(15),
		marginTop: scale(15),
		marginHorizontal: scale(40),
		textAlign: 'center'
	},
	buttonContainer: {
		position: 'absolute',
		bottom: verticalScale(2),
		alignItems: 'center',
		paddingTop: scale(10)
	},
	centerText: {
		position: 'absolute',
		alignItems: 'center',
		justifyContent: 'center',
		height: scale(63)
	},
	buttonText: {
		...REGULAR_FONT,
		color: 'white',
		fontSize: scale(20)
	}
});

export default class InfoCircle extends Component {
	static propTypes = {
		higherDelay: PropTypes.number,
		close: PropTypes.func.isRequired,
		source: Image.propTypes.source.isRequired,
		name: PropTypes.string.isRequired,
		text: PropTypes.string.isRequired
	};
	static defaultProps = {
		higherDelay: 0
	};

	render() {
		const { higherDelay, source, name, text, close } = this.props;
		return (
			<Animatable.View style={styles.container} animation='fadeIn' delay={higherDelay} useNativeDriver>
				<View style={styles.container}>
					<TouchableOpacity style={styles.touchableBackground} onPress={close} />
					<View style={{ justifyContent: 'center', alignItems: 'center' }}>
						<View style={styles.circle}>
							<View style={styles.icon}>
								<ProportionalImage source={source} ratio={scale(0.4)} />
							</View>
							<View styles={styles.containerTexts}>
								<Text style={styles.achievement}>ACHIEVEMENT</Text>
								<Text style={styles.name}>{name}</Text>
								<Text style={styles.description}>{text}</Text>
							</View>
						</View>
					</View>
				</View>
			</Animatable.View>
		);
	}
}
