const arborealPointImage = require('../../../assets/images/achievements-images/Arboreal-Point.png');
const arborealPointImageGray = require('../../../assets/images/achievements-images/Arboreal-Point-gray.png');
const clarionSandsImage = require('../../../assets/images/achievements-images/Clarion-Sands.png');
const clarionSandsImageGray = require('../../../assets/images/achievements-images/Clarion-Sands-gray.png');
const feelingGoodImage = require('../../../assets/images/achievements-images/Feeling-Good.png');
const feelingGoodImageGray = require('../../../assets/images/achievements-images/Feeling-Good-gray.png');
const formedAHabitImage = require('../../../assets/images/achievements-images/Formed-a-Habit.png');
const formedAHabitImageGray = require('../../../assets/images/achievements-images/Formed-a-Habit-gray.png');
const moodBoostImage = require('../../../assets/images/achievements-images/Mood-Boost.png');
const moodBoostImageGray = require('../../../assets/images/achievements-images/Mood-Boost-gray.png');
const newOutlookImage = require('../../../assets/images/achievements-images/New-Outlook.png');
const newOutlookImageGray = require('../../../assets/images/achievements-images/New-Outlook-gray.png');
const onARollImage = require('../../../assets/images/achievements-images/On-a-Roll.png');
const onARollImageGray = require('../../../assets/images/achievements-images/On-a-Roll-gray.png');
const pacificaBayImage = require('../../../assets/images/achievements-images/Pacifica-Bay.png');
const pacificaBayImageGray = require('../../../assets/images/achievements-images/Pacifica-Bay-gray.png');
const panaceaPlainsImage = require('../../../assets/images/achievements-images/Panacea-Plains.png');
const panaceaPlainsImageGray = require('../../../assets/images/achievements-images/Panacea-Plains-gray.png');
const peakProductivityImage = require('../../../assets/images/achievements-images/Peak-Productivity.png');
const peakProductivityImageGray = require('../../../assets/images/achievements-images/Peak-Productivity-gray.png');
const positiveChangeImage = require('../../../assets/images/achievements-images/Positive-Change.png');
const positiveChangeImageGray = require('../../../assets/images/achievements-images/Positive-Change-gray.png');
const rejuvenatorImage = require('../../../assets/images/achievements-images/Rejuvenator.png');
const rejuvenatorImageGray = require('../../../assets/images/achievements-images/Rejuvenator-gray.png');
const ScholarImage = require('../../../assets/images/achievements-images/Scholar.png');
const ScholarImageGray = require('../../../assets/images/achievements-images/Scholar-gray.png');
const steppingUpImage = require('../../../assets/images/achievements-images/Stepping-Up.png');
const steppingUpImageGray = require('../../../assets/images/achievements-images/Stepping-Up-gray.png');
const StressReliefImage = require('../../../assets/images/achievements-images/Stress-Relief.png');
const StressReliefImageGray = require('../../../assets/images/achievements-images/Stress-Relief-gray.png');
const zenAcolyteImage = require('../../../assets/images/achievements-images/Zen-Acolyte.png');
const zenAcolyteImageGray = require('../../../assets/images/achievements-images/Zen-Acolyte-gray.png');
const zenDiscipleImage = require('../../../assets/images/achievements-images/Zen-Disciple.png');
const zenDiscipleImageGray = require('../../../assets/images/achievements-images/Zen-Disciple-gray.png');
const zenMasterImage = require('../../../assets/images/achievements-images/Zen-Master.png');
const zenMasterImageGray = require('../../../assets/images/achievements-images/Zen-Master-gray.png');
const zenStudentImage = require('../../../assets/images/achievements-images/Zen-Student.png');
const zenStudentImageGray = require('../../../assets/images/achievements-images/Zen-Student-gray.png');
const zenithHeightsImage = require('../../../assets/images/achievements-images/Zenith-Heights.png');
const zenithHeightsImageGray = require('../../../assets/images/achievements-images/Zenith-Heights-gray.png');

export const ACHIEVEMENT_01_KEY = 'MOOD BOOST';
export const ACHIEVEMENT_02_KEY = 'STRESS RELIEF';
export const ACHIEVEMENT_03_KEY = 'POSITIVE CHANGE';
export const ACHIEVEMENT_04_KEY = 'NEW OUTLOOK';
export const ACHIEVEMENT_05_KEY = 'PEAK POSITIVITY';
export const ACHIEVEMENT_06_KEY = 'PACIFICA BAY';
export const ACHIEVEMENT_07_KEY = 'ARBOREAL POINT';
export const ACHIEVEMENT_08_KEY = 'PANACEA PLAINS';
export const ACHIEVEMENT_09_KEY = 'CLARION SANDS';
export const ACHIEVEMENT_10_KEY = 'ZENITH HEIGHTS';
export const ACHIEVEMENT_11_KEY = 'STUDENT';
export const ACHIEVEMENT_12_KEY = 'TEACHER';
export const ACHIEVEMENT_13_KEY = 'PROFESSOR';
export const ACHIEVEMENT_14_KEY = 'ON A ROLL';
export const ACHIEVEMENT_15_KEY = 'MAKING A HABIT';
export const ACHIEVEMENT_16_KEY = 'MADE A HABIT';
export const ACHIEVEMENT_17_KEY = 'CONSISTENCY';
export const ACHIEVEMENT_18_KEY = 'SCHOLAR';
export const ACHIEVEMENT_19_KEY = 'FEELING GOOD';
export const ACHIEVEMENT_20_KEY = 'REJUVENATOR';

export const badges = [
	{
		id: ACHIEVEMENT_06_KEY,
		source: pacificaBayImage,
		sourceGray: pacificaBayImageGray,
		text: 'GET THIS BADGE WHEN YOU RESTORE THE  REGION BY REVITALIZING THE SCENE 10 TIMES'
	},
	{
		id: ACHIEVEMENT_01_KEY,
		source: moodBoostImage,
		sourceGray: moodBoostImageGray,
		text: 'GET THIS BADGE WHEN YOU SET AND ACHIEVE A 10 MIN WEEKLY GOAL'
	},
	{
		id: ACHIEVEMENT_02_KEY,
		source: StressReliefImage,
		sourceGray: StressReliefImageGray,
		text: 'GET THIS BADGE WHEN YOU SET AND ACHIEVE A 15 MIN WEEKLY GOAL'
	},
	{
		id: ACHIEVEMENT_03_KEY,
		source: positiveChangeImage,
		sourceGray: positiveChangeImageGray,
		text: 'GET THIS BADGE WHEN YOU SET AND ACHIEVE A 20 MIN WEEKLY GOAL'
	},
	{
		id: ACHIEVEMENT_04_KEY,
		source: newOutlookImage,
		sourceGray: newOutlookImageGray,
		text: 'GET THIS BADGE WHEN YOU SET AND ACHIEVE A 25 MIN WEEKLY GOAL'
	},
	{
		id: ACHIEVEMENT_07_KEY,
		source: arborealPointImage,
		sourceGray: arborealPointImageGray,
		text: 'GET THIS BADGE WHEN YOU RESTORE THE ARBOREAL POINT BY REVITALIZING THE SCENE 15 TIMES'
	},
	{
		id: ACHIEVEMENT_18_KEY,
		source: ScholarImage,
		sourceGray: ScholarImageGray,
		text: 'GET THIS BADGE WHEN YOU VISIT SCHOLARS TOWER'
	},
	{
		id: ACHIEVEMENT_08_KEY,
		source: panaceaPlainsImage,
		sourceGray: panaceaPlainsImageGray,
		text: 'GET THIS BADGE WHEN YOU RESTORE THE PANACEA PLAINS REGION BY REVITALIZING THE SCENE 20 TIMES'
	},
	{
		id: ACHIEVEMENT_05_KEY,
		source: peakProductivityImage,
		sourceGray: peakProductivityImageGray,
		text: 'GET THIS BADGE WHEN YOU SET AND ACHIEVE A 30 MIN WEEKLY GOAL'
	},
	{
		id: ACHIEVEMENT_09_KEY,
		source: clarionSandsImage,
		sourceGray: clarionSandsImageGray,
		text: 'GET THIS BADGE WHEN YOU RESTORE THE CLARION SANDS REGION BY REVITALIZING THE SCENE 25 TIMES'
	},
	{
		id: ACHIEVEMENT_11_KEY,
		source: zenAcolyteImage,
		sourceGray: zenAcolyteImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN FOR 50 MINS'
	},
	{
		id: ACHIEVEMENT_12_KEY,
		source: zenStudentImage,
		sourceGray: zenStudentImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN FOR 100 MINS'
	},
	{
		id: ACHIEVEMENT_14_KEY,
		source: onARollImage,
		sourceGray: onARollImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN EVERY OTHER DAY FOR A WEEK'
	},
	{
		id: ACHIEVEMENT_15_KEY,
		source: formedAHabitImage,
		sourceGray: formedAHabitImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN EVERY OTHER DAY FOR 2 WEEKS'
	},
	{
		id: ACHIEVEMENT_16_KEY,
		source: steppingUpImage,
		sourceGray: steppingUpImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN EVERY OTHER DAY FOR A MONTH'
	},
	{
		id: ACHIEVEMENT_10_KEY,
		source: zenithHeightsImage,
		sourceGray: zenithHeightsImageGray,
		text: 'GET THIS BADGE WHEN YOU RESTORE THE ZENITH HEIGHTS REGION BY REVITALIZING THE SCENE 30 TIMES'
	},
	{
		id: ACHIEVEMENT_17_KEY,
		source: zenMasterImage,
		sourceGray: zenMasterImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN EVERY OTHER DAY FOR 2 MONTHS'
	},
	{
		id: ACHIEVEMENT_19_KEY,
		source: feelingGoodImage,
		sourceGray: feelingGoodImageGray,
		text: 'GET THIS BADGE WHEN YOU LOWER YOUR STRESS LEVEL'
	},
	{
		id: ACHIEVEMENT_13_KEY,
		source: zenDiscipleImage,
		sourceGray: zenDiscipleImageGray,
		text: 'GET THIS BADGE WHEN YOU PLAY PERSONAL ZEN FOR 200 MINS'
	},
	{
		id: ACHIEVEMENT_20_KEY,
		source: rejuvenatorImage,
		sourceGray: rejuvenatorImageGray,
		text: 'GET THIS BADGE WHEN YOU RESTORE THE ENTIRE WORLD OF PERSONAL ZEN'
	}
];
