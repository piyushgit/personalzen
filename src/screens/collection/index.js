import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  SafeAreaView,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import _ from 'lodash';
import Promise from 'bluebird';
import PropTypes from 'prop-types';

import { REGULAR_FONT } from '../../constants/styles';
import scale, { verticalScale } from '../../utils/scale';
import ProportionalImage from '../../components/proportional-image';
import Circle from './colletion-circle';
import { badges } from './config';
import RegionsLayout from '../../constants/regions';
import { updateAchievementIsRecent } from '../../actions';

const exitButtonImage = require('../../../assets/images/tutorial/close-icon.png');
const stroke = require('../../../assets/images/results/stroke.png');

const window = Dimensions.get('window');
const ORIGINAL_WIDTH = 749;
const BUTTOM_SIZE = 75;
const ratio = window.width / ORIGINAL_WIDTH;
const aspectRatio = window.height / window.width;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F6F6F6',
    width: '100%',
    height: '100%',
  },
  containerTitle: {
    alignItems: 'center',
    marginVertical: scale(40),
  },
  stroke: {
    position: 'absolute',
  },
  title: {
    ...REGULAR_FONT,
    fontSize: scale(22),
    position: 'absolute',
    letterSpacing: 5,
  },
  containerBadges: {
    flexWrap: 'wrap',
    width: '100%',
    paddingHorizontal: aspectRatio < 1.5 ? scale(47) : scale(20),
    flexDirection: 'row',
  },
  badge: {
    borderRadius: 300,
    width: 250 * (aspectRatio < 1.5 ? scale(0.17) : scale(0.229)),
    marginHorizontal: aspectRatio < 1.5 ? scale(10) : scale(10),
    marginVertical: aspectRatio < 1.5 ? scale(9) : scale(12),
  },
  footer: {
    width: '100%',
    alignItems: 'center',
    marginTop: aspectRatio < 1.5 ? scale(25) : scale(50),
  },
  textFooter: {
    ...REGULAR_FONT,
    fontSize: scale(13),
  },
  closeButton: {
    position: 'absolute',
    top: verticalScale(17),
    right: scale(17),
  },
  closeButtonImage: {
    // fontSize: scale(30)
    width: BUTTOM_SIZE * ratio,
    height: BUTTOM_SIZE * ratio,
  },
});

class Collection extends Component {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
    achievements: PropTypes.object.isRequired,
    updateAchievementIsRecent: PropTypes.func.isRequired,
  };
  constructor(props) {
    super(props);
    this.state = {
      showCircle: false,
      achievement: 0,
      regions: _.clone(RegionsLayout),
      currentRegion: {},
      delays: 0,
      show: true,
    };
  }

  componentWillMount() {
    const { navigation } = this.props;
    const { regions } = this.state;
    const backToMap = navigation.getParam('backToMap');
    const regionKey = navigation.getParam('regionKey');
    const delays = navigation.getParam('delays');
    const currentRegion = regions.find(region => region.key === regionKey);

    this.setState({
      currentRegion,
      delays,
      backToMap,
    });
  }

  componentDidMount() {
    const achievement = _.pickBy(
      this.props.achievements,
      item => item.isRecent
    );
    const timeout = ms => new Promise(res => setTimeout(res, ms));
    if (achievement) {
      Promise.each(_.keys(this.props.achievements), item => {
        if (this.props.achievements[item].isRecent) {
          return timeout(500)
            .then(() => {
              this.props.updateAchievementIsRecent({ achievementKey: item });
              this.setState({
                showCircle: true,
                achievement: item,
              });
              return timeout(4000);
            })
            .then(() => {
              this.setState({
                showCircle: false,
              });
            });
        }
        return null;
      });
    }
  }

  handleBadgePress = index => {
    this.setState({
      showCircle: true,
      achievement: index,
    });
  };

  handleCloseCircle = () => {
    this.setState({ showCircle: false });
  };

  handleCloseButton = () => {
    if (this.state.backToMap) {
      this.props.navigation.goBack();
    } else {
      setTimeout(() => {
        this.setState({ show: false });
      }, 1000);
      const { currentRegion, delays } = this.state;
      this.props.navigation.navigate('Results', {
        regionKey: currentRegion.key,
        delays,
        collection: true,
      });
    }
  };

  render() {
    const { showCircle, achievement, show } = this.state;
    if (!show) return null;
    return (
      <SafeAreaView style={styles.container}>
        <TouchableOpacity
          style={styles.closeButton}
          onPress={this.handleCloseButton}
        >
          {/* <Text style={styles.textButton}>X</Text> */}
          <Image style={styles.closeButtonImage} source={exitButtonImage} />
        </TouchableOpacity>
        <View style={styles.containerTitle}>
          <ProportionalImage
            style={styles.stroke}
            source={stroke}
            ratio={scale(0.28)}
          />

          <Text style={styles.title}>ACHIEVEMENTS</Text>
        </View>
        <View style={styles.containerBadges} onLayout={this.onLayout}>
          {badges.map(item => {
            let image = item.sourceGray;

            if (this.props.achievements[item.id].achieved) {
              image = item.source;
            }
            return (
              <TouchableOpacity
                style={styles.badge}
                onPress={() => this.handleBadgePress(item.id)}
              >
                <ProportionalImage
                  source={image}
                  ratio={aspectRatio < 1.5 ? scale(0.17) : scale(0.229)}
                />
              </TouchableOpacity>
            );
          })}
        </View>
        {showCircle && (
          <Circle
            source={
              badges[_.findIndex(badges, item => item.id === achievement)]
                .source
            }
            name={
              badges[_.findIndex(badges, item => item.id === achievement)].id
            }
            text={
              badges[_.findIndex(badges, item => item.id === achievement)].text
            }
            close={this.handleCloseCircle}
          />
        )}
        <View style={styles.footer}>
          <Text style={styles.textFooter}>YOUR AWARD APPEARS HERE</Text>
          <Text style={styles.textFooter}>TAP AN ITEM TO VIEW DETAILS</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = {
  updateAchievementIsRecent,
};

function mapStateToProps(state) {
  return {
    achievements: state.achievements,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Collection);
