import React, { Component } from 'react';
import * as RNIap from 'react-native-iap';
// import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { View, StyleSheet, Text, Image, Platform, TouchableOpacity, Dimensions, Alert, Linking } from 'react-native';
import { APPLE_STORE_SHARED_SECRET } from '../../../shared_secret';
import { THIN_FONT, REGULAR_FONT } from '../../constants/styles';

const spriteBlueImage = require('../../../assets/images/entrance/sprite-blue.png');
const spriteGreenImage = require('../../../assets/images/regions/plains-region/sprite-green-negative.png');

const AwardIcon = require('../../../assets/images/Subscription/award-icon.png');

const itemSkus = Platform.select({
	ios: ['PersonalZen', 'PersonalZen1Y'],
	android: ['PersonalZen', 'PersonalZen1Y']
});

const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: Dimensions.get('window').height,
		width: Dimensions.get('window').width,
		justifyContent: 'center',
		alignItems: 'center'
	},
	containerCenter: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		// marginTop: 20,
		marginRight: 250
	},
	containerLogo: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	textHeader: {
		fontSize: 30,
		color: '#57606f',
		...THIN_FONT
	},
	header: {
		right: 30,
		top: 80
	},
	buttonSignIn: {
		flex: 1,
		width: 260,
		padding: 10,
		margin: 7,
		alignItems: 'center',
		justifyContent: 'center'
	},
	bottomView: {
		position: 'absolute',
		bottom: 50,
		alignItems: 'center'
	},
	DescTextStyles: {
		color: '#57606f',
		fontSize: 26,
		textAlign: 'center',
		...REGULAR_FONT
	},
	AppDescriptionText: {
		color: '#57606f',
		fontSize: 18,
		textAlign: 'center',
		paddingTop: 20,
		...REGULAR_FONT
	},
	buttonText: {
		flex: 1,
		fontFamily: 'Arial',
		fontSize: 15,
		color: '#FFFFFF',
		textAlign: 'center'
	},
	buttonTextSignIn: {
		fontFamily: 'Arial',
		fontSize: 15,
		color: '#01a3a4',
		alignItems: 'center'
	},
	awardIcon: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		borderBottomColor: '#01a3a4',
		borderBottomWidth: 1,
		marginTop: 20,
		marginBottom: 20,
		paddingBottom: 30
	},
	awardIconImage: {
		width: '22%' // is 50% of container width
	},
	awardIconText: {
		width: '55%'
	},
	awardText: {
		color: '#57606f',
		fontSize: 12,
		textAlign: 'left',
		alignItems: 'baseline',
		...REGULAR_FONT,
		paddingTop: 50
	},
	btnContainer: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		marginBottom: '10%'
	},
	viewButtonTextSignIn: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'flex-start',
		marginHorizontal: 10,
		marginVertical: -6,
		flexDirection: 'row'
	},
	buttonSubscription: {
		width: 280,
		height: 40,
		// margin: 0,
		alignItems: 'center',
		backgroundColor: '#01a3a4',
		justifyContent: 'center',
		borderRadius: 50,
		marginBottom: '2%'
	},
	buttonTextMonthlyAnnual: {
		color: '#FFFFFF',
		...REGULAR_FONT,
		fontWeight: '600',
		fontSize: 15,
		textAlign: 'center',
		marginTop: -5
	}
});

export default class Subscription extends Component {
	static PropTypes = {
		navigation: PropTypes.shape({ navigate: PropTypes.func.isRequired }).isRequired
	};
	constructor(props) {
		super(props);
		this.state = {
			productList: [],
			availableItemsMessage: '',
			receipt: ''
		};
		this.goToNext = this.goToNext.bind(this);
		this.buySubscribeItem = this.buySubscribeItem.bind(this);
		this.getPurchasesItem = this.getPurchasesItem.bind(this);
	}
	async componentDidMount() {
		try {
			await RNIap.initConnection();
			this.products = await RNIap.getSubscriptions(itemSkus);
		} catch (err) {
			console.warn(err.code, err.message);
		}
	}

	goToNext = () => {
		this.props.navigation.navigate('Tutorial');
	};
	buySubscribeItem = async sku => {
		try {
			const purchase = await RNIap.buySubscription(sku);
			const receiptBody = {
				'receipt-data': purchase.transactionReceipt,
				password: APPLE_STORE_SHARED_SECRET
			};
			// console.info(purchase);
			const result = await RNIap.validateReceiptIos(receiptBody, true);
			this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
		} catch (err) {
			console.warn(err.code, err.message);
			Alert.alert(err.message);
		}
	};
	getPurchasesItem = async() => {
		try {
			const purchases = await RNIap.getAvailablePurchases();
			let restoredTitles = '';
			// let coins = CoinStore.getCount();
			let found=false;
			purchases.forEach(purchase => {
				if (purchase.productId == 'PersonalZen') {
					this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
					found=true;
				}else if (purchase.productId == 'PersonalZen1Y') {
					this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
					found=true;
				}
			})

			if(!found){
				Alert.alert("You don't have any subscriptions yet.");
			}
		} catch (err) {
			console.warn(err.code, err.message);
			Alert.alert(err.message);
		}
	};
	// buyItem = async sku => {
	// 	console.info('buyItem: ', sku);
	// 	try {
	// 		const purchase = await RNIap.buyProduct(sku);
	// 		this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
	// 		// const receipt1 = this.state.receipt.substring(0, 100);
	// 		// Alert.alert(receipt1);
	// 		// console.log('RECEIPT OF TRANSACTION', receipt1);
	// 	} catch (err) {
	// 		console.warn(err.code, err.message);
	// 		const subscription = RNIap.addAdditionalSuccessPurchaseListenerIOS(async purchase => {
	// 			this.setState({ receipt: purchase.transactionReceipt }, () => this.goToNext());
	// 			subscription.remove();
	// 		});
	// 	}
	// };
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.containerCenter}>
					<View style={styles.containerLogo}>
						<View style={{ position: 'absolute', left: -6 }}>
							<Image style={{ width: 60, height: 60 }} source={spriteGreenImage} />
						</View>
						<View style={{ position: 'absolute', right: -6 }}>
							<Image style={{ width: 60, height: 60 }} source={spriteBlueImage} />
						</View>
						<View style={{ position: 'absolute', left: 70 }}>
							<Text style={styles.textHeader}>PERSONAL ZEN</Text>
						</View>
					</View>
				</View>

				<View style={{ paddingTop: 10 }}>
					}
					<Text style={styles.DescTextStyles}>
						THE ACCLAIMED APP  FOR REDUCING  STRESS & ANXIETY
					</Text>
					<Text style={styles.AppDescriptionText}>
						Scientifically-validated and designed for relaxation 
					</Text>
				</View>

				<View style={styles.awardIcon}>
					<View style={styles.awardIconImage}>
						<Image style={{ height: 90, width: 70 }} source={AwardIcon} />
					</View>
					<View style={styles.awardIconText}>
						<Text style={styles.awardText}>Top 10 Health App by CNN Health*</Text>
					</View>
				</View>
				<View
					style={{
						borderBottomColor: '#01a3a4',
						borderBottomWidth: 0,
						// position: 'absolute',
						bottom: 30,
						width: 280
					}}
				/>

				<Text
					style={{
						color: '#01a3a4',
						fontSize: 20,
						...REGULAR_FONT,
						textAlign: 'center',
						marginBottom: '6%'
					}}
				>
					Special Introductory Pricing
				</Text>
				<Text
					style={{
						color: '#01a3a4',
						fontFamily: 'arial',
						fontSize: 15,
						textAlign: 'center',
						marginBottom: '5%'
					}}
				>
					Each Plan includes a 14-Day Free Trial
				</Text>
				<View style={styles.btnContainer}>
					<TouchableOpacity
						style={styles.buttonSubscription}
						onPress={() => this.buySubscribeItem('PersonalZen')} // 'here PersonalZen string should be exactly the same as the Reference Name in In-app Purchase tab in apple store connect account
					>
						<Text style={styles.buttonTextMonthlyAnnual}>Monthly $1.99 Subscription</Text>
					</TouchableOpacity>

					<TouchableOpacity
						style={styles.buttonSubscription}
						onPress={() => this.buySubscribeItem('PersonalZen1Y')} // 'here PersonalZen1Y string should be exactly the same as the Reference Name in In-app Purchase tab in apple store connect account
					>
						<Text style={styles.buttonTextMonthlyAnnual}>Annual $12.99 Subscription</Text>
					</TouchableOpacity>

					<TouchableOpacity
						style={styles.buttonSubscription}
						onPress={() => this.getPurchasesItem()} // 'here PersonalZen1Y string should be exactly the same as the Reference Name in In-app Purchase tab in apple store connect account
					>
						<Text style={styles.buttonTextMonthlyAnnual}>Restore Purchases</Text>
					</TouchableOpacity>

					
				</View>
				<View
					style={{
						borderBottomColor: '#01a3a4',
						borderBottomWidth: 2,
						// position: 'absolute',
						// bottom: 90,
						width: Dimensions.get('window').width
					}}
				/>
				<Text
					style={{
						fontSize: 9,
						...REGULAR_FONT,
						textAlign: 'center',
						padding: 10
					}}
				>
					Subscription Terms: After the trial, Personal Zen monthly/yearly subscription automatically renews
					unless turned off in Account settings at least 24h before current period ends.  Payment is
					charged to your iTunes Account.
				</Text>
				<Text
					style={{
						fontSize: 9,
						fontWeight: 'bold'
					}}
					onPress={() => {
						Linking.openURL('https://personalzen.com/terms/');
					}}
				>
					Terms and Privacy Policy
				</Text>
				{/* <View style={{ left: 5 }}> */}
				<Text
					style={{
						...REGULAR_FONT,
						fontSize: 10,
						marginBottom: 20,
						marginRight: 310,
						color: '#57606f'
					}}
				>
					*of 2014
				</Text>
			</View>
		);
	}
}
