import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Dimensions, TextInput, KeyboardAvoidingView } from 'react-native';
import { Text, Item } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import { firebaseRef } from '../../services/firebase';

const EntranceTopImage = require('../../../assets/images/entrance/entrance-top.png');
const EntranceBottomImage = require('../../../assets/images/entrance/entrance-bottom.png');
const spriteBlueImage = require('../../../assets/images/entrance/sprite-blue.png');
const spriteRedImage = require('../../../assets/images/entrance/sprite-red.png');

const window = Dimensions.get('window');

export default class SignUp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			confirmPassword: '',
			ButtonStateHolder: true
		};
		this._signup = this._signup.bind(this);
	}

	_signup() {
		if (this.state.password === this.state.confirmPassword) {
			firebaseRef
				.auth()
				.createUserWithEmailAndPassword(this.state.email, this.state.password)
				.catch(function(error) {
					alert(error.message);
				});
			firebaseRef.auth().onAuthStateChanged((user) => {
				this.props.navigation.navigate(user ? 'Tutorial' : 'SignUp');
			});
		} else {
			alert('Passwords did not match..');
		}
	}
	render() {
		return (
			<View style={styles.container}>
				{/* <Image style={styles.container} source={backGround} /> */}
				<Image style={styles.topImage} source={EntranceTopImage} />
				<View style={styles.viewSpriteCenter}>
					<View style={styles.viewSpriteLogo}>
						<Image
							style={{
								position: 'absolute',
								right: -15,
								height: 150,
								width: 150
							}}
							source={spriteBlueImage}
						/>
						<Image
							style={{
								position: 'absolute',
								left: -15,
								height: 150,
								width: 150
							}}
							source={spriteRedImage}
						/>
					</View>
				</View>
				<KeyboardAvoidingView behavior="padding" style={styles.viewSignUp}>
					<Item style={{ marginBottom: 10 }}>
						<Icon style={styles.iconStyle} size={25} name="ios-mail" />
						<TextInput
							style={styles.textInput}
							onChangeText={(text) => this.setState({ email: text })}
							value={this.state.email}
							placeholder="Email"
							autoFocus="true"
							returnKeyType="next"
							autoCapitalize="none"
							keyboardType="email-address"
							autoCorrect={false}
							placeholderTextColor="#82589F"
							onSubmitEditing={() => this.emailInput.focus()}
						/>
					</Item>
					<Item style={{ marginBottom: 10 }}>
						<Icon style={styles.iconStyle} size={25} name="ios-lock" />
						<TextInput
							style={styles.textInput}
							onChangeText={(text) => this.setState({ password: text })}
							value={this.state.password}
							placeholder="Password"
							returnKeyType="next"
							secureTextEntry={true}
							placeholderTextColor="#82589F"
							ref={(inputEmail) => (this.emailInput = inputEmail)}
							onSubmitEditing={() => this.passwordInput.focus()}
						/>
					</Item>
					<Item style={{ marginBottom: 10 }}>
						<Icon style={styles.iconStyle} size={25} name="ios-lock" />
						<TextInput
							style={styles.textInput}
							onChangeText={(text) => this.setState({ confirmPassword: text, ButtonStateHolder: false })}
							value={this.state.confirmPassword}
							placeholder="Confirm Password"
							returnKeyType="go"
							secureTextEntry={true}
							placeholderTextColor="#82589F"
							ref={(input) => (this.passwordInput = input)}
						/>
					</Item>
					<View style={styles.viewButtonStyle}>
						<TouchableOpacity
							block
							style={[
								styles.buttonStyle,
								{ backgroundColor: this.state.ButtonStateHolder ? '#b2bec3' : '#82589F' }
							]}
							activeOpacity={0.5}
							disabled={this.state.ButtonStateHolder}
							onPress={this._signup}
						>
							<Text style={{ color: '#fff', textAlign: 'center' }}>Get Started</Text>
						</TouchableOpacity>
						<TouchableOpacity
							block
							style={styles.buttonStyle}
							onPress={() => this.props.navigation.navigate('Subscription')}
						>
							<Text style={{ color: '#fff', textAlign: 'center' }}>Cancel</Text>
						</TouchableOpacity>
					</View>
				</KeyboardAvoidingView>
				<View>
					<Image style={styles.bottomImage} source={EntranceBottomImage} />
					<Text
						style={{
							textAlign: 'center',
							paddingLeft: 8,
							paddingRight: 8,
							paddingBottom: 25,
							color: '#fff',
							fontSize: 12
						}}
					>
						Payments will be charged to the Apple ID account at the confirmation of purchase.The
						subscription automatically renews unless it is canceled at least 24 hours before the end of the
						current period. Your account will be charged for renewal within 24 hours prior to the end of the
						current period. You can manage and cancel your subscriptions by going to your app Store account
						settings after purchase.
					</Text>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		// flex: 1,
		height: Dimensions.get('screen').height,
		width: Dimensions.get('screen').width
	},
	topImage: {
		height: window.height / 3,
		width: window.width
	},
	viewSpriteCenter: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		alignItems: 'center',
		marginTop: -80
	},
	bottomImage: {
		position: 'absolute',
		height: window.height / 3,
		width: window.width,
		bottom: 0
	},
	viewSpriteLogo: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	viewSignUp: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	iconStyle: {
		color: '#82589F',
		marginLeft: 15
	},
	textInput: {
		color: '#82589F',
		borderBottomColor: '#82589F',
		padding: 7,
		height: 30,
		width: 250,
		fontSize: 15,
		opacity: 0.6,
		fontFamily: 'Arial'
	},
	buttonStyle: {
		height: 30,
		width: window.width / 3,
		backgroundColor: '#82589F',
		borderRadius: 50,
		alignItems: 'center',
		justifyContent: 'center'
	},
	viewButtonStyle: {
		flex: 1,
		flexDirection: 'row',
		paddingLeft: 20,
		paddingRight: 20,
		justifyContent: 'space-between'
	}
});
