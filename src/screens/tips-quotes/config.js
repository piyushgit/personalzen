export const QUOTES = [
  {
    quote: 'The greatest weapon against stress is our ability to choose one thought over another.',
    author: '- William James',
  },
  {
    quote: 'Life is lived in the present.  Yesterday is gone.  Tomorrow is yet to be.  Today is the miracle.',
    author: null,
  },
  {
    quote: 'If opportunity doesn’t knock, build a door.',
    author: '- Milton Berle',
  },
  {
    quote: 'I may not have gone where I intended to go, but I think I have ended up where I needed to be.',
    author: '- Douglas Adams',
  },
  {
    quote: 'Take chances, make mistakes. That’s how you grow. Pain nourishes your courage.You have to fail in order to practice being brave.',
    author: '- Mary Tyler Moore',
  },
  {
    quote: 'Be soft. Do not let the world make you hard. Do not let pain make you hate. Do not let the bitterness steal your sweetness. Take pride that even though the rest of the world may disagree, you still believe it to be a beautiful place.',
    author: '- Kurt Vonnegut',
  },
  {
    quote: 'To experience peace does not mean that your life is always blissful. It means that you are capable of tapping into a blissful state of mind amidst the normal chaos of a hectic life.',
    author: '- Jill Bolte Taylor',
  },
  {
    quote: 'When you find yourself stressed, ask yourself one question: Will this matter in 5 years from now? If yes, then do something about the situation. If no, then let it go.',
    author: '- Catherine Pulsifer',
  },
  {
    quote: 'When we long for a life without difficulties, remind us that oaks grow strong in contrary winds and diamonds are made under pressure.',
    author: '- Peter Marshall',
  },
  {
    quote: 'Being happy doesn`t mean that everything is perfect. It means that you`ve decided to look beyond the imperfections.',
    author: null,
  },
  {
    quote: 'The time to relax is when you don`t have time for it.',
    author: '- Jim Goodwin and Sydney J. Harris',
  },
  {
    quote: 'A bird sitting on a tree is never afraid of the branch breaking, because its trust is not in the branch, but in its own wings.',
    author: null,
  },
];

export const TIPS = [
  {
    title: 'Three Good Things',
    text: 'Every night for the next week, set aside ten minutes before you go to sleep. Write down three things that went well today and why they went well. Keep a physical record of your list in a journal or computer. The three things can be simple or important. Next to each positive event, answer the question “Why did this happen?” and take a moment to elaborate on why you are thankful it happened. This exercise encourages gratitude while molding optimism about the future.',
  },
  {
    title: 'Sugar vs. Lemon',
    text: 'Before reacting to a frustrating event or person, flip a coin. Heads, it’s sugar in which you take the sweet approach and you slow down. Tails, it’s lemon and you should assume your typical behavior. Keep track of what happens. You will likely find that you got what you wanted either way, but when done with sugar, you make allies and your mood is better, and at the small cost of an extra minute or so for each encounter.',
  },
  {
    title: 'Who’s Bugging You',
    text: 'Choose a person in your life that you know well but have a grudge against. On a piece of blank paper, draw a "frown" in the center of the page and record a few words that capture the essence of your grudge. Fill the rest of the page with 15 circles. Fill each of these circles with a word or phrase that describes something about the person for which you are grateful. Now hold the page at arm’s distance and notice how the grudge gets lost in the sea of gratitude. How do your emotions and thoughts change as you focus on the person now?',
  },
  {
    title: 'Gift of Time',
    text: 'Offer the “gift” of your time to three different people this week. This might be in the form of time spent, helping someone around their house, or sharing a meal with someone who is lonely. These “gifts” should be in addition to your planned activities.',
  },
  {
    title: 'Counting Kindness',
    text: 'Keep a log of all the kind acts that you do in a particular day, jotting them down by the end of each day. Notice how these acts shape how your view yourself, and contribute to your sense of happiness, fulfillment and meaning.',
  },
  {
    title: 'Three Funny Things',
    text: 'Write down the three funniest things that you experienced or participated in each day. Also, write about why the funny thing happened (e.g., was it something you created, something you observed, something spontaneous?)',
  },
  {
    title: 'Meditation',
    text: 'Try the following mediation technique for a few minutes a day, building up to longer periods if you find it enjoyable. Find a comfortable, upright sitting pose. Allow your thoughts to come and go, acknowledging them but letting them pass naturally. Focus on your breathing. Take full but gentle breaths through your nose. This simple exercise will quickly bring you into the present and connect your mind with your body. With time your ability to meditate, and to control your focus, will improve.',
  },
  {
    title: 'Create a Vision Board',
    text: 'A vision board is a board that is filled with images and words about the future. It does not have to include long-term goals, but should reflect something that you aspire to achieve, personally or professionally. Make it as simple or elaborate as you want, it can be physical or electronic. Don’t use the board as a goal checklist, but rather as a pleasant reminder of your dreams and aspirations.',
  },
  {
    title: 'Breathe Deeply',
    text: 'Diaphragmatic breathing – slow, deep, from-the-belly breaths – directly creates a relaxed state because it triggers the parasympathetic nervous system (rest and recovery) and halts the fight-flight response. Take 1-2 minutes a day, or anytime you feel stressed, to breathe yourself back into a relaxed state.',
  },
  {
    title: 'Smile',
    text: 'Smiling and laughter actually make you feel happier. Through “facial feedback” your brain interprets your smiling face to mean that you are in a happy state. For reasons that aren’t entirely understood, your inner experience of happiness increases accordingly. So try smiling for 10 seconds, several times in a row, and feel your good mood rise.',
  },
  {
    title: 'Chocolate',
    text: 'Eat an ounce of dark chocolate (65% cocoa or more) a day to lower blood pressure and produce PEA, the neurotransmitter responsible for feelings of wellbeing, love, and alertness. Yes, chocolate fans, you read this right.',
  },
  {
    title: 'Exercise',
    text: 'Exercise is the single most effective way to promote mental and physical health. Taking a walk around the block can quickly shift you towards a more positive mood. Sprinting for 50 yards as hard as you can instantly releases endorphins, natural pain and stress fighters.',
  },
];
