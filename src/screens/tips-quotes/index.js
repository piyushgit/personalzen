import React, { Component } from 'react';
import { SafeAreaView, View, Text, TouchableOpacity, StyleSheet, Dimensions, Image } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { TIPS, QUOTES } from './config';
import scale, { verticalScale } from '../../utils/scale';
import { REGULAR_FONT } from '../../constants/styles';

const exitButtonImage = require('../../../assets/images/tutorial/close-icon.png');

const window = Dimensions.get('window');
const ORIGINAL_WIDTH = 749;
const BUTTOM_SIZE = 75;
const ratio = window.width / ORIGINAL_WIDTH;
const aspectRatio = window.height / window.width;

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
		paddingHorizontal: scale(10),
		width: '100%',
		height: '100%'
	},
	title: {
		...REGULAR_FONT,
		fontSize: scale(25),
		marginTop: scale(45),
		marginBottom: scale(20),
		width: '100%',
		textAlign: 'center'
	},
	text: {
		...REGULAR_FONT,
		fontSize: aspectRatio < 1.5 ? scale(18) : scale(21),
		textAlign: 'center',
		width: '100%',
		paddingHorizontal: 15,
		justifyContent: 'center'
	},
	author: {
		...REGULAR_FONT,
		fontSize: aspectRatio < 1.5 ? scale(16) : scale(18),
		textAlign: 'center',
		width: '100%'
	},
	quotesContainer: {
		height: '100%',
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	button: {
		position: 'absolute',
		top: verticalScale(17),
		right: scale(17),
		width: scale(40),
		height: scale(40)
	},
	CloseButtonImage: {
		// fontSize: scale(30)
		width: BUTTOM_SIZE * ratio,
		height: BUTTOM_SIZE * ratio
	},
	continueButton: {
		fontSize: 26,
		textAlign: 'center',
		letterSpacing: 6,
		marginRight: 60,
		marginLeft: 60
	}
});

class TipsAndQuotes extends Component {
	static propTypes = {
		navigation: PropTypes.shape().isRequired
	};

	handleButton = () => {
		this.props.navigation.goBack();
	};

	render() {
		const tip = _.random(0, 1);
		let arrayIndex = 0;
		if (tip === 1) {
			arrayIndex = _.random(0, TIPS.length - 1);
		} else {
			arrayIndex = _.random(0, QUOTES.length - 1);
		}
		return (
			<SafeAreaView style={styles.container}>
				{tip === 1 && (
					<View>
						<Text style={styles.title}>{TIPS[arrayIndex].title}</Text>
						<Text style={styles.text}>{TIPS[arrayIndex].text}</Text>
						<TouchableOpacity onPress={this.handleButton}>
							<Text style={styles.continueButton}>{'\n'}CONTINUE</Text>
						</TouchableOpacity>
					</View>
				)}
				{tip === 0 && (
					<View style={styles.quotesContainer}>
						<Text style={styles.text}>{QUOTES[arrayIndex].quote}</Text>
						<Text style={styles.author}>{QUOTES[arrayIndex].author}</Text>
						<TouchableOpacity onPress={this.handleButton}>
							<Text style={styles.continueButton}>{'\n'}CONTINUE</Text>
						</TouchableOpacity>
					</View>
				)}
				<TouchableOpacity style={styles.button} onPress={this.handleButton}>
					{/* <Text style={styles.textButton}>X</Text> */}
					<Image style={styles.CloseButtonImage} source={exitButtonImage} />
				</TouchableOpacity>
			</SafeAreaView>
		);
	}
}

export default TipsAndQuotes;
