import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
} from 'react-native';
import * as Animatable from 'react-native-animatable';

import FullWidthImage from '../../components/full-width-image';
import { REGULAR_FONT } from '../../constants/styles';
import flowerPostions from './flowers-backgroud';
import { verticalScale } from '../../utils/scale';
import ResultsCircle from './results-circle';
import { ROUND_PER_SCENE } from '../game-level/config';

import { addSwipeSpeed } from '../../services/swipe';

const stripe = require('../../../assets/images/results/stripe.png');

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: window.width,
    height: '100%',
    backgroundColor: 'white',
  },
  containerTextGoal: {
    paddingTop: verticalScale(40),
    paddingBottom: verticalScale(10),
  },
  yourGoal: {
    ...REGULAR_FONT,
    textAlign: 'center',
  },
  stripeStyle: {
  },
  containerTextMinutes: {
    paddingTop: verticalScale(10),
  },
});

class Results extends Component {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
    weeklyGoal: PropTypes.object.isRequired,
    goal: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    nav: PropTypes.shape().isRequired,
  }

  constructor(props) {
    super(props);

    const delays = props.navigation.getParam('delays');
    const regionKey = props.navigation.getParam('regionKey');
    const collection = props.navigation.getParam('collection');
    const averageDelay = delays / ROUND_PER_SCENE;

    const { weeklyPlayedTime, goal } = this.props.weeklyGoal;
    const progress = weeklyPlayedTime / goal;
    this.state = {
      flowers: flowerPostions(progress),
      goal,
      playedTime: weeklyPlayedTime,
      averageDelay,
      regionKey,
      collection,
      show: true,
    };
  }

  componentDidMount() {
    const { user } = this.props.user;
    const { averageDelay, regionKey } = this.state;
    const today = moment();

    const swipe = {
      placeboMode: user.placeboMode,
      swipeSpeed: averageDelay,
      date: moment(today, moment.ISO_8601).format(),
      region: regionKey,
    };

    addSwipeSpeed(swipe)
      .then((response) => {
        console.log(response);
      });
  }

  componentDidUpdate() {
    const { index, routes } = this.props.nav;
    const { routeName } = routes[index];

    const show = (routeName === 'Results');

    if (show && !this.state.show) {
      this.setState({ show: true }); // eslint-disable-line
    }
  }

  handleReturnToWorldMap = () => {
    const { playedTime, goal } = this.state;
    if (playedTime === goal) {
      setTimeout(() => {
        this.setState({ show: false });
      }, 1000);
      this.props.navigation.navigate('StressTracking', {
        regionKey: this.state.regionKey,
      });
    } else {
      this.props.navigation.navigate('WorldMap');
    }
  }

  handleRestart = () => {
    let pop = 2;
    if (this.state.collection) pop = 3;
    this.props.navigation.pop(pop);
    this.props.navigation.navigate('GameLevel', {
      regionKey: this.state.regionKey,
    });
  }

  renderFlowers = () => {
    const flowers = [];
    const positions = this.state.flowers;
    let higherDelay = 0;
    const START_DELAY = 1000;
    const DURATION = 1600;
    positions.forEach((item, index) => {
      const { position, size, source } = item;
      const delay = (((position.y - window.height) * -1) * 6) + START_DELAY;
      if (delay > higherDelay) {
        higherDelay = delay;
      }
      flowers.push(<Animatable.Image
        key={index} // eslint-disable-line
        useNativeDriver
        animation="fadeInDownBig"
        delay={delay}
        source={source}
        duration={DURATION}
        style={{
          position: 'absolute',
          width: size,
          height: size,
          top: position.y,
          left: position.x,
        }}
      />);
    });
    higherDelay += DURATION + START_DELAY;
    return ({ flowers, higherDelay });
  }

  render() {
    const day = moment(this.props.weeklyGoal.nextWeek).format('dddd');
    const { flowers, higherDelay } = this.renderFlowers();
    if (!this.state.show) return null;
    return (
      <View style={styles.container}>
        <View style={styles.containerTextGoal}>
          <Text style={styles.yourGoal}>YOUR GOAL</Text>
        </View>
        <FullWidthImage style={styles.stripeStyle} source={stripe} />
        <View style={styles.containerTextMinutes}>
          <Text style={styles.yourGoal}>{this.state.goal} MINS</Text>
        </View>
        <View style={{ position: 'absolute', top: 0, bottom: 0 }} >
          {flowers}
        </View>
        <ResultsCircle
          playedTime={this.state.playedTime}
          swipeSpeed={this.state.averageDelay}
          higherDelay={higherDelay}
          onPress={this.handleReturnToWorldMap}
          restart={this.handleRestart}
          regionKey={this.state.regionKey}
          goal={this.state.goal}
          day={day}
        />
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    weeklyGoal: state.weeklyGoal,
    user: state.user,
    nav: state.nav,
  };
}


export default connect(mapStateToProps)(Results);
