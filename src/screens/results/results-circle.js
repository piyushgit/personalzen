import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import {
  View,
  Text,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';

import Svg, { Path } from 'react-native-svg';
import { REGULAR_FONT } from '../../constants/styles';
import scale from '../../utils/scale';

const goalCompleted = require('../../../assets/images/results/goal-completed.png');

const GOAL_COMPLETED_SIZE = 670;

const window = Dimensions.get('window');

let { height } = window;
if (Platform.OS === 'android') {
  height = ExtraDimensions.get('REAL_WINDOW_HEIGHT');
}


const AVERAGE = 2.5;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height,
  },
  circle: {
    width: scale(300),
    height: scale(300),
    backgroundColor: 'white',
    opacity: 1,
    borderRadius: 200,
    alignItems: 'center',
  },
  containerTextSlow: {
    paddingTop: scale(20),
    paddingBottom: scale(20),
    alignItems: 'center',
  },
  containerTextFast: {
    paddingTop: scale(60),
    paddingBottom: scale(20),
    alignItems: 'center',
  },
  containerTextGoalCompleted: {
    paddingTop: scale(75),
    paddingBottom: scale(15),
    alignItems: 'center',
  },
  text: {
    ...REGULAR_FONT,
    fontSize: scale(20),
  },
  textGoalCompleted: {
    ...REGULAR_FONT,
    fontSize: scale(16),
  },
  containerMinutes: {
    flexDirection: 'row',
  },
  playedTime: {
    ...REGULAR_FONT,
    fontSize: scale(40),
    paddingRight: scale(15),
  },
  textMins: {
    ...REGULAR_FONT,
    fontSize: scale(20),
    paddingTop: scale(20),
  },
  underMinuteText: {
    ...REGULAR_FONT,
    fontSize: scale(14),
    paddingTop: scale(30),
  },
  buttonContainer: {
    alignItems: 'center',
    paddingTop: scale(10),
  },
  centerText: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    height: scale(63),
  },
  buttonText: {
    ...REGULAR_FONT,
    color: 'white',
    fontSize: scale(20),
  },
  backToMap: {
    position: 'absolute',
    top: (height - scale(80)),
    left: 0,
    width: '100%',
  },
  gradinetView: {
    height: scale(80),
    width: '100%',
    paddingBottom: scale(10),
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});

export default class InfoCircle extends Component {
  static propTypes = {
    swipeSpeed: PropTypes.number.isRequired,
    playedTime: PropTypes.number.isRequired,
    goal: PropTypes.number.isRequired,
    higherDelay: PropTypes.number,
    onPress: PropTypes.func.isRequired,
    restart: PropTypes.func.isRequired,
    day: PropTypes.string.isRequired,
  };
  static defaultProps = {
    higherDelay: 0,
  };

  circleText = (textGoal, textFast, textSlow, average, playedTime, goal, swipeSpeed) => {
    if (playedTime > goal) return textGoal;
    if (swipeSpeed <= average) return textFast;
    return textSlow;
  }

  render() {
    const {
      swipeSpeed,
      higherDelay,
      goal,
      playedTime,
    } = this.props;

    const textSlow = (
      <View style={styles.containerTextSlow}>
        <Text style={styles.text}>Focus on</Text>
        <Text style={styles.text}>getting to the</Text>
        <Text style={styles.text}>sparkle quickly and</Text>
        <Text style={styles.text}>follow the path</Text>
      </View>
    );
    const textFast = (
      <View style={styles.containerTextFast}>
        <Text style={styles.text}>Keep up the good</Text>
        <Text style={styles.text}>work!</Text>
      </View>
    );
    const textGoalCompleted = (
      <View style={styles.containerTextGoalCompleted}>
        <Text style={styles.textGoalCompleted}>Feel free to continue playing.</Text>
        <Text style={styles.textGoalCompleted}>A fresh week starts on {this.props.day}!</Text>
      </View>
    );


    const size = scale(223.7);
    const path = `M 0 0 L 0 0 A ${scale(115)} ${scale(115)} 0 ${scale(80)} 0 ${scale(223.7)} ${scale(-160)} L ${scale(223.7)} 0`;
    return (
      <Animatable.View style={styles.container} animation="fadeIn" delay={higherDelay} useNativeDriver>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          {playedTime === goal &&
            <Animatable.Image
              animation="fadeIn"
              duration={500}
              ratio={scale(0.47)}
              style={{
                width: GOAL_COMPLETED_SIZE * scale(0.47),
                height: GOAL_COMPLETED_SIZE * scale(0.47),
              }}
              source={goalCompleted}
            />
          }
          {(playedTime < goal || playedTime > goal) &&
            <View style={styles.circle}>
              {this.circleText(
                textGoalCompleted,
                textFast,
                textSlow,
                AVERAGE,
                playedTime,
                goal,
                swipeSpeed,
              )}
              <View style={styles.containerMinutes}>
                <Text style={styles.playedTime}>{this.props.playedTime}</Text>
                <Text style={styles.textMins}>{this.props.playedTime <= 1 ? 'MIN' : 'MINS' }</Text>
              </View>
              <Text style={styles.underMinuteText}>TOTAL TIME TOWARD GOAL</Text>
              <TouchableOpacity style={styles.buttonContainer} onPress={this.props.restart} >
                <Svg
                  height={size}
                  width={size}
                >
                  <Path
                    d={path}
                    fill="#b4b1e4"
                  />
                </Svg>
                <View style={styles.centerText}>
                  <Text style={styles.buttonText}>CONTINUE</Text>
                </View>
              </TouchableOpacity>
            </View>
          }
        </View>
        <TouchableOpacity style={styles.backToMap} onPress={this.props.onPress}>
          <LinearGradient style={styles.gradinetView} colors={['transparent', '#b4b1e4']}>
            <SafeAreaView>
              <Text style={styles.buttonText}>
                {playedTime === goal ? 'CONTINUE' : 'EXIT TO MAP'}
              </Text>
            </SafeAreaView>
          </LinearGradient>
        </TouchableOpacity>
      </Animatable.View>
    );
  }
}
