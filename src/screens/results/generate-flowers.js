const _ = require('lodash');
const fs = require('fs');

const window = {
  width: 350,
  height: 680,
};

function getDistance({ x: x1, y: y1 }, { x: x2, y: y2 }) {
  return Math.sqrt(Math.abs(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2))); // eslint-disable-line
}

function flowerPostions(progress) {
  const positions = [];
  const minDistance = 35.0;
  const minSize = 30;
  const maxSize = 60;
  const top = 60;
  const height = (window.height - top);

  if (progress <= 0) {
    return [];
  }

  const randomSize = () => _.random(minSize, maxSize);
  const randomX = () => _.random(-maxSize, window.width);
  const randomY = () => _.random(top + (height * (1 - (progress))), window.height);

  const firstPosition = {
    source: _.random(0, 14),
    size: randomSize(),
    position: {
      x: randomX(),
      y: randomY(),
    },
  };

  positions.push(firstPosition);

  const qntFlowers = 140 * (progress);

  while (positions.length <= qntFlowers) {
    let j = 0;
    const x = randomX();
    const y = randomY();

    let isDistant = true;
    for (j; j < positions.length; j += 1) {
      const distance = getDistance({ x, y }, positions[j].position);
      if (distance < minDistance) {
        isDistant = false;
      }
    }

    if (isDistant) {
      positions.push({
        source: _.random(0, 14),
        size: randomSize(),
        position: {
          x,
          y,
        },
      });
    }
  }
  return positions;
}

const data = flowerPostions(1);

fs.writeFile('flowers.json', JSON.stringify(data), (err) => {
  if (err) {
    return console.log(err);
  }
  return console.log('file saved!');
});

