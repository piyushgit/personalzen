import _ from 'lodash';

import scale, { verticalScale, guidelineBaseHeight } from '../../utils/scale';
import flowerPositions from './flowers.json';

const flower1 = require('../../../assets/images/regions/desert-region/pink-flower.png');
const flower2 = require('../../../assets/images/regions/forest-region/orange-petals.png');
const flower3 = require('../../../assets/images/regions/forest-region/reddish-flower.png');
const flower4 = require('../../../assets/images/regions/forest-region/yellow-flower.png');
const flower5 = require('../../../assets/images/regions/forest-region/yellow-rose.png');
const flower6 = require('../../../assets/images/regions/plains-region/orange-flower.png');
const flower7 = require('../../../assets/images/flowers/blue-flower.png');
const flower8 = require('../../../assets/images/flowers/green-flower.png');
const flower9 = require('../../../assets/images/flowers/lavender-flower.png');
const flower10 = require('../../../assets/images/flowers/pink-flower-2.png');
const flower11 = require('../../../assets/images/flowers/pink-flower-4.png');
const flower12 = require('../../../assets/images/flowers/pink-flower-5.png');
const flower13 = require('../../../assets/images/flowers/pink-flower.png');
const flower14 = require('../../../assets/images/flowers/punk-flower-3.png');
const flower15 = require('../../../assets/images/flowers/purple-flower.png');

const flowerAssets = [
  flower1,
  flower2,
  flower3,
  flower4,
  flower5,
  flower6,
  flower7,
  flower8,
  flower9,
  flower10,
  flower11,
  flower12,
  flower13,
  flower14,
  flower15,
];

export default function flowerPostions(progress) {
  const top = 60;
  const height = (guidelineBaseHeight - top);

  const minY = top + (height * (1 - (progress)));

  return _.cloneDeep(flowerPositions)
    .filter(item => item.position.y > minY)
    .map(item => ({
      source: flowerAssets[item.source],
      size: scale(item.size),
      position: {
        x: scale(item.position.x),
        y: verticalScale(item.position.y),
      },
    }));
}
