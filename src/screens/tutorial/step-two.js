import React, { Component } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';

import Message from '../../components/message';
import { TEXT_2 } from './config';
import { verticalScale } from '../../utils/scale';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
    marginTop: verticalScale(50),
  },
});

class StepOne extends Component {
  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
      <SafeAreaView
        style={styles.container}
      >
        <Message text={TEXT_2} />
      </SafeAreaView>
    );
  }
}

export default StepOne;
