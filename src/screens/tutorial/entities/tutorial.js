import React from 'react';

import THREE from '../../../lib/three';

import Planet from '../../../components/planet';

const PLANET_INITIAL_ROTATION = new THREE.Euler(-0.995, -0.560, -0.700);
const PLANET_INITIAL_POSITION = new THREE.Vector3(-300, 0, 0);


export default (scene, camera) => {
  camera.position.z = 700;
  camera.position.x = 0;
  camera.position.y = 0;
  camera.updateProjectionMatrix();

  // Unproject screen to world coords
  const screenPoint = new THREE.Vector3(1, -1, -1).unproject(camera);

  // Project screen point to planet plane
  const target = new THREE.Vector3();
  const plane = new THREE.Plane(new THREE.Vector3(0, 0, 1));
  const ray = new THREE.Ray(camera.position, screenPoint);
  ray.intersectPlane(plane, target);

  // Offset planet
  target.add(new THREE.Vector3(-50, 50, 0));

  return {
    scene,
    camera,
    planet: {
      time: 0,
      target,
      quaternion: new THREE.Quaternion().setFromEuler(PLANET_INITIAL_ROTATION),
      rotation: PLANET_INITIAL_ROTATION,
      position: PLANET_INITIAL_POSITION,
      renderer: <Planet />,
    },
    fps: {
      total: 0,
    },
  };
};
