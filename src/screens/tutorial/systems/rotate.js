import { axis, rotateOnAxis } from '../../../utils/vector-math';

import { ROTATION_SPEED } from '../config';

export default function rotate(entities, { time }) {
  const { planet } = entities;

  if (planet.time <= 2) {
    rotateOnAxis(planet, axis.Y, time.delta * ROTATION_SPEED);
  }

  return entities;
}
