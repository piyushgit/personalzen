import Rotate from './rotate';
import Translate from './translate';
import Fps from './fps';

export default [Rotate, Translate, Fps];
