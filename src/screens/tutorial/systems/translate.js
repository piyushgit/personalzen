import { Easing } from 'react-native';

import THREE from '../../../lib/three';

const easing = Easing.bezier(0.37, 0, 0.69, 1);

export default function rotate(entities, { time, dispatch }) {
  const { planet } = entities;
  const { target } = planet;

  if (planet.startTime) {
    planet.time = ((time.current - planet.startTime) / 6000);

    if (planet.time <= 1) {
      const t = easing(planet.time);
      planet.position = new THREE.Vector3(-400 + (t * 400), 0, 0);
    }

    if (planet.time > 1 && planet.time <= 2) {
      const t = easing(planet.time - 1);
      planet.position = new THREE.Vector3(t * target.x, t * target.y, 0);
    }
    if (planet.time >= 2) {
      dispatch({ type: 'planet-animation-end' });
    }
  }

  return entities;
}
