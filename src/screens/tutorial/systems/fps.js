export default function (entities, { dispatch, time }) {
  const { fps, planet } = entities;

  fps.total += 1;

  if (fps.total === 3) {
    dispatch({ type: 'render-started' });
    planet.startTime = time.current;
  }

  return entities;
}
