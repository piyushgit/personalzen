import React, { Component } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';

import Message from '../../components/message';
import { TEXT_3 } from './config';
import { verticalScale } from '../../utils/scale';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
    marginTop: verticalScale(40),
  },
});

class StepTwo extends Component {
  static defaultProps = {
    window: {},
  }

  render() {
    return (
      <SafeAreaView
        style={styles.container}
      >
        <Message text={TEXT_3} />
      </SafeAreaView>
    );
  }
}

export default StepTwo;
