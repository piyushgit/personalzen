import React, { Component } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';

import Message from '../../components/message';
import { TEXT_1 } from './config';
import { verticalScale } from '../../utils/scale';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
    marginTop: verticalScale(10),
  },
});

class StepOne extends Component {
  componentWillUnmount() {
    clearTimeout(this.timeoutHandle);
  }

  render() {
    return (
      <SafeAreaView
        style={styles.container}
      >
        <Message text={TEXT_1} />
      </SafeAreaView>
    );
  }
}

export default StepOne;
