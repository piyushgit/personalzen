import * as Animatable from 'react-native-animatable';
import _ from 'lodash';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';

import GestureRecognizer from '../../utils/swipe';
import SplashScreen from '../../components/splash';
import BackButton from '../../components/back-button';

import { DELAY_TIME, DELAY_SPLASH, BUTTOM_SIZE } from './config';

import StepOne from './step-one';
import StepTwo from './step-two';
import StepThree from './step-three';
import scale from '../../utils/scale';

import { playSound, stopSound } from '../../actions';

import { GAMEPLAYMUSIC } from '../../reducers/sounds';

const backgroundImage = require('../../../assets/images/menu/backgroud.png');
const globeImage = require('../../../assets/images/tutorial/globe-static.png');
const buttomRight = require('../../../assets/images/tutorial/nav-buttom-right.png');
const buttomLeft = require('../../../assets/images/tutorial/nav-buttom-left.png');
const closeIcon = require('../../../assets/images/tutorial/x-white-circle.png');
const ORIGINAL_WIDTH = 749;
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    zIndex: 200,
  },
  TextContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
  },
  background: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  globe: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  containerNavButtom: {
    position: 'absolute',
    bottom: scale(10),
    left: scale(10),
    zIndex: 201,
    flexDirection: 'row',
  },
});

class TutorialScreen extends PureComponent {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
    nav: PropTypes.shape().isRequired,
    playSound: PropTypes.func.isRequired,
    stopSound: PropTypes.func.isRequired,
  };

  state = {
    ratio: window.width / ORIGINAL_WIDTH,
    step: 'one',
    startTyping: false,
    splash: true,
  };

  componentWillMount() {
    this.props.playSound({ sound: GAMEPLAYMUSIC });
  }

  onMenuClick(entity) {
    if (entity.id === 'menu-world-map') {
      this.props.navigation.navigate('WorldMap');
    }
  }

  onStartTyping = () => {
    this.setState({
      splash: false,
      startTyping: true,
    });
  };
  onPressCancelButton = () => {
    this.props.stopSound({ sound: GAMEPLAYMUSIC });
    this.props.navigation.navigate('MainMenu');
  };
  onPressLeftButtom = () => {
    if (this.state.step === 'two') {
      this.setState({ step: 'one' });
    }
    if (this.state.step === 'three') {
      this.startTutorialGame();
    }
  };

  onPressRightButtom = () => {
    if (this.state.step === 'one') {
      this.setState({ step: 'two' });
    }
    if (this.state.step === 'two') {
      this.setState({ step: 'three' });
      this.startTutorialGame();
    }
    if (this.state.step === 'three') {
      this.props.navigation.navigate('StressTracking', {
        isTutorial: true,
      });
    }
  };

  onSwipeLeft = () => {
    this.onPressRightButtom();
  };

  onSwipeRight = () => {
    this.onPressLeftButtom();
  };

  onBackPress = () => {
    this.props.stopSound({ sound: GAMEPLAYMUSIC });
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'MainMenu' })],
    });

    this.props.navigation.dispatch(resetAction);
    return true;
  };

  startTutorialGame() {
    this.props.navigation.navigate('GameLevel', {
      regionKey: 'region-03',
      isTutorial: true,
    });
  }

  render() {
    const { step, startTyping, ratio, splash } = this.state;

    let showStepThree = false;

    if (
      _.last(this.props.nav.routes).params &&
      _.last(this.props.nav.routes).params.step
    ) {
      showStepThree = true;
    }

    return (
      <GestureRecognizer
        style={{ position: 'absolute', width: '100%', height: '100%' }}
        onSwipeLeft={state => this.onSwipeLeft(state)}
        onSwipeRight={state => this.onSwipeRight(state)}
      >
        <Image style={styles.background} source={backgroundImage} />
        <Image
          style={[styles.globe, { width: scale(200), height: scale(200) }]}
          source={globeImage}
        />

        {startTyping && (
          <React.Fragment>
            <Animatable.View
              style={styles.containerNavButtom}
              animation="fadeIn"
              useNativeDriver
            >
              {step === 'three' && showStepThree && (
                <TouchableOpacity onPress={this.onPressLeftButtom}>
                  <Image
                    source={buttomLeft}
                    style={{
                      width: BUTTOM_SIZE * ratio,
                      height: BUTTOM_SIZE * ratio,
                      margin: window.width / 20,
                    }}
                  />
                </TouchableOpacity>
              )}
              <TouchableOpacity onPress={this.onPressRightButtom}>
                <Image
                  source={buttomRight}
                  style={{
                    width: BUTTOM_SIZE * ratio,
                    height: BUTTOM_SIZE * ratio,
                    margin: window.width / 20,
                  }}
                />
              </TouchableOpacity>
            </Animatable.View>
            <BackButton onPress={this.onBackPress} />

            <View style={{ flex: 1 }}>
              <TouchableOpacity onPress={() => this.onPressCancelButton()}>
                <Image
                  source={closeIcon}
                  style={{
                    width: BUTTOM_SIZE * ratio,
                    height: BUTTOM_SIZE * ratio,
                    // position: 'absolute',
                    // top: 20,
                    // right: 10
                    margin: window.width / 40,
                    alignSelf: 'flex-end',
                  }}
                />
              </TouchableOpacity>
              <Animatable.View
                delay={DELAY_TIME}
                animation="fadeIn"
                useNativeDriver
                style={styles.TextContainer}
                onAnimationEnd={this.onStartTyping}
              >
                {step === 'one' && startTyping && <StepOne ratio={ratio} />}
                {step === 'two' && startTyping && <StepTwo ratio={ratio} />}
                {step === 'three' && startTyping && showStepThree && (
                  <StepThree window={window} />
                )}
              </Animatable.View>
            </View>
          </React.Fragment>
        )}
        {splash && (
          <Animatable.View
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
            }}
            delay={DELAY_SPLASH}
            animation="fadeOut"
            onAnimationEnd={this.onStartTyping}
          >
            <SplashScreen />
          </Animatable.View>
        )}
      </GestureRecognizer>
    );
  }
}

const mapDispatchToProps = {
  playSound,
  stopSound,
};

function mapStateToProps(state) {
  return {
    nav: state.nav,
    // sounds: state.sounds
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TutorialScreen);
