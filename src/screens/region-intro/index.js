import React, { Component } from 'react';
import PropTypes from 'prop-types';

import RegionsLayout from '../../constants/regions';

import RegionIntro from './region-intro';

export default class RegionIntroContainer extends Component {
	static propTypes = {
		navigation: PropTypes.object.isRequired
	};

	constructor(props) {
		super(props);

		const regionKey = props.navigation.getParam('regionKey');
		const region = RegionsLayout.find(x => x.key === regionKey);

		this.state = {
			opacity: 1,
			region
		};
	}

	onBeginClick = () => {
		const { region } = this.state;
		this.props.navigation.navigate('GameLevel', {
			regionKey: region.key,
			isTutorial: false
		});

		// Workaround for keeping navigation transition smooth.
		// This sets the opacity of the intro to 0 so it doesn't show
		// when game-level navigates back to world map.
		// If we replace this screen or reset the state to remove this
		// screen from the stack, the transition animation doesn't get executed.
		setTimeout(() => {
			this.setState({ opacity: 0 });
		}, 2000);
	};

	render() {
		const { region, opacity } = this.state;
		return <RegionIntro style={{ opacity }} region={region} onBegin={this.onBeginClick} />;
	}
}
