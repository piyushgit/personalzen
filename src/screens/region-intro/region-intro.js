import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { REGULAR_FONT, THIN_FONT } from '../../constants/styles';
import { TEXT_COLOR } from '../../constants/colors';

import scale from '../../utils/scale';
import ProportionalImage from '../../components/proportional-image';

const beginButtom = require('../../../assets/images/region-intro/begin-buttom.png');

const styles = StyleSheet.create({
	card: {
		...StyleSheet.absoluteFillObject,
		backgroundColor: '#fff',
		opacity: 1,
		flexDirection: 'column',
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	container: {
		alignItems: 'center'
	},
	text: {
		...REGULAR_FONT,
		color: TEXT_COLOR,
		letterSpacing: 3
	},
	textThin: {
		...THIN_FONT
	},
	titleContainer: {
		flex: 5,
		justifyContent: 'center'
	},
	textContainer: {
		flex: 1,
		alignItems: 'center'
	},
	imageContainer: {
		flex: 5,
		justifyContent: 'center',
		marginTop: 24,
		marginBottom: 24
	},
	beginContainer: {
		flex: 3,
		marginTop: 40
	},
	beginText: {
		fontSize: 26
	},
	titleText: {
		fontSize: 32,
		textAlign: 'center',
		letterSpacing: 6,
		marginRight: 60,
		marginLeft: 60
	}
});

export default class RegionIntro extends Component {
	static propTypes = {
		style: PropTypes.object,
		region: PropTypes.object.isRequired,
		onBegin: PropTypes.func.isRequired
	};

	static defaultProps = {
		style: null
	};

	render() {
		const { region, style } = this.props;
		return (
			<View style={[styles.card, style]}>
				<View style={styles.container}>
					<View style={styles.titleContainer}>
						<Text style={[styles.text, styles.titleText]}>{region.title.label.toUpperCase()}</Text>
					</View>
					<View style={styles.imageContainer}>
						<ProportionalImage source={region.intro.img} ratio={scale(0.6)} />
					</View>
					<View style={styles.textContainer}>
						<Text style={[styles.text, styles.textThin]}>FOCUS ON THE POSITIVE</Text>
						<Text style={[styles.text, styles.textThin]}>TRACE THE VIBRANT PATH</Text>
					</View>
					<View style={styles.beginContainer}>
						<TouchableOpacity onPress={this.props.onBegin}>
							<ProportionalImage style={styles.image} source={beginButtom} ratio={scale(0.5)} />
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}
