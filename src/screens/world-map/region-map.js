import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import FullWidthImage from '../../components/full-width-image';
import ProportionalImage from '../../components/proportional-image';
import { RegionStates } from '../../constants/regions';

class RegionMap extends Component {
  static propTypes = {
    layout: PropTypes.object.isRequired,
    region: PropTypes.object.isRequired,
    ratio: PropTypes.number.isRequired,
    onPress: ProportionalImage.propTypes.onPress.isRequired,
    auth: PropTypes.object.isRequired,
  }

  render() {
    const {
      layout,
      region,
      ratio,
    } = this.props;

    const {
      state,
      saturation = 9,
    } = region;

    const { isSuper } = this.props.auth;

    return [
      <FullWidthImage
        key={layout.key}
        source={layout.region.img}
        style={{ top: layout.region.top * ratio, position: 'absolute' }}
      />,
      <FullWidthImage
        key={`${layout.key}-grayscale`}
        source={layout.region.imgGrayscale}
        style={{
          top: layout.region.top * ratio,
          position: 'absolute',
          opacity: (1 - (saturation / 9)),
        }}
      />,
      <ProportionalImage
        touchable={state === RegionStates.unlocked || isSuper}
        key={`${layout.key}-title`}
        source={state === RegionStates.unlocked ? layout.titleUnlocked.img : layout.title.img}
        ratio={ratio}
        onPress={this.props.onPress}
        style={{
          top: layout.title.top * ratio,
          left: layout.title.left * ratio,
          position: 'absolute',
          zIndex: 1,
        }}
      />,
    ];
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

export default connect(mapStateToProps)(RegionMap);
