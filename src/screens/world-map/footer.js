import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

import { REGULAR_FONT, THIN_FONT } from '../../constants/styles';
import FullWidthImage from '../../components/full-width-image';
import ProportionalImage from '../../components/proportional-image';
import scale from '../../utils/scale';

const footerImage = require('../../../assets/images/world-map/World-Map-00.png');
const backButton = require('../../../assets/images/stress-tracking/nav-buttom-back.png');

const FOOTER_TOP = 4060;

const styles = StyleSheet.create({
  containerText: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  title: {
    ...REGULAR_FONT,
    color: 'white',
    textAlign: 'center',
  },
  subTitle: {
    ...THIN_FONT,
    color: 'white',
    textAlign: 'center',
  },
  backButton: {
    position: 'absolute',
    bottom: scale(10),
    left: scale(10),
  },
});

class WorldMapFooter extends Component {
  static propTypes = {
    ratio: PropTypes.number.isRequired,
    onPress: PropTypes.func.isRequired,
  }

  render() {
    const {
      ratio,
    } = this.props;

    return [
      <FullWidthImage
        key="background"
        source={footerImage}
        style={{
          top: FOOTER_TOP * ratio,
          position: 'absolute',
        }}
      />,
      <View
        key="container-text"
        style={[
          styles.containerText,
          { bottom: 75 },
        ]}
      >
        <Text
          key="subtitle"
          style={[
            styles.subTitle,
            { fontSize: 32 * ratio },
          ]}
        >
          PERSONAL ZEN
        </Text>
        <Text
          key="title"
          style={[
            styles.title,
            { fontSize: 72 * ratio },
          ]}
        >
          REGIONS
        </Text>
        <Text
          key="instructions"
          style={[
            styles.subTitle,
            { fontSize: 32 * ratio },
          ]}
        >
          SELECT AN AREA TO RESTORE
        </Text>
      </View>,
      <SafeAreaView style={styles.backButton}>
        <TouchableOpacity onPress={this.props.onPress} >
          <ProportionalImage source={backButton} ratio={0.6} />
        </TouchableOpacity >
      </SafeAreaView>,
    ];
  }
}
export default WorldMapFooter;
