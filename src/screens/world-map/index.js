import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';

import {
  findOrCreateUser,
  updateScrollPosition,
  playSound,
  stopSound,
} from '../../actions';
import RegionsLayout from '../../constants/regions';
import RegionMap from './region-map';
import Footer from './footer';
import { INTHENOW } from '../../reducers/sounds';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: 'white',
  },
});

export const ORIGINAL_WIDTH = 749;
export const ORIGINAL_HEIGHT = 4693;

class WorldMapScreen extends Component {
  static propTypes = {
    nav: PropTypes.shape().isRequired,
    regions: PropTypes.object.isRequired,
    navigation: PropTypes.object.isRequired,
    updateScrollPosition: PropTypes.func.isRequired,
    findOrCreateUser: PropTypes.func.isRequired,
    playSound: PropTypes.func.isRequired,
    stopSound: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    const window = Dimensions.get('window');
    const ratio = window.width / ORIGINAL_WIDTH;
    const worldMapSize = (ORIGINAL_HEIGHT * window.width) / ORIGINAL_WIDTH;

    this.state = {
      regions: _.clone(RegionsLayout),
      worldMapSize,
      ratio,
      running: true,
    };
  }

  componentDidMount() {
    this.props.findOrCreateUser();

    this.props.playSound({ sound: INTHENOW });

    setTimeout(() => {
      this.props.navigation.navigate('TipsAndQuotes');
    }, 200);
  }

  componentDidUpdate() {
    const { index, routes } = this.props.nav;
    const { routeName } = routes[index];

    // Pause sound if current screen is not WorldMap
    const running = (routeName === 'WorldMap' || routeName === 'TipsAndQuotes');
    if (running !== this.state.running) {
      this.setState({ running }); // eslint-disable-line
    }
    if (running) {
      this.props.playSound({ sound: INTHENOW });
    }
    if (!running) {
      this.props.stopSound({ sound: INTHENOW });
    }
    this.scrollView.scrollTo({ x: 0, y: this.props.regions.scrollPosition, animated: false });
  }

  onRegionClick(region) {
    this.props.navigation.navigate('RegionIntro', {
      regionKey: region.key,
    });
  }

  handleBackToMenu = () => {
    this.props.navigation.goBack();
  }

  handleScroll = (event) => {
    if (this.scrollTimeout) {
      clearTimeout(this.scrollTimeout);
      this.scrollTimeout = null;
    }

    const scrollPosition = event.nativeEvent.contentOffset.y;

    this.scrollTimeout = setTimeout(() => {
      this.props.updateScrollPosition({ scrollPosition });
    }, 100);
  };

  render() {
    const {
      regions,
      worldMapSize,
      ratio,
    } = this.state;

    return (
      <ScrollView
        onScroll={this.handleScroll}
        scrollEventThrottle={16}
        style={styles.container}
        ref={(ref) => { this.scrollView = ref; }}
        onContentSizeChange={() => {
          this.scrollView.scrollTo({ x: 0, y: this.props.regions.scrollPosition, animated: false });
        }}
      >
        <View style={{ height: worldMapSize }}>
          <Footer ratio={ratio} onPress={this.handleBackToMenu} />
          {regions.map(layout => (
            <RegionMap
              key={layout.key}
              layout={layout}
              region={this.props.regions[layout.key]}
              ratio={ratio}
              onPress={() => this.onRegionClick(layout)}
            />
          ))}
        </View>
      </ScrollView>
    );
  }
}

const mapDispatchToProps = {
  updateScrollPosition,
  findOrCreateUser,
  playSound,
  stopSound,
};

function mapStateToProps(state) {
  return {
    regions: state.regions,
    nav: state.nav,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorldMapScreen);
