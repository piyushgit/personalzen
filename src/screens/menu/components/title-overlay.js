import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { REGULAR_FONT, THIN_FONT } from '../../../constants/styles';
import scale from '../../../utils/scale';

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'transparent',
		zIndex: 200
	},
	containerOverlay: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-between'
	},
	containerFooterText: {
		flexDirection: 'column',
		justifyContent: 'center'
	},
	containerText: {
		flexDirection: 'column',
		justifyContent: 'center'
	},
	title: {
		...REGULAR_FONT,
		color: 'white',
		textAlign: 'center'
	},
	subTitle: {
		...REGULAR_FONT, // ...THIN_FONT was set up earlier
		color: 'white',
		textAlign: 'center'
	}
});

export default () => (
	<View pointerEvents='none' style={styles.containerOverlay}>
		<View style={[styles.containerText, { height: 140 }]}>
			<Text style={[styles.subTitle, { fontSize: scale(14) }]}>WELCOME TO</Text>
			<Text style={[styles.title, { fontSize: scale(24) }]}>PERSONAL ZEN</Text>
		</View>
		<View style={[styles.containerFooterText, { height: 140 }]}>
			<Text style={styles.subTitle}>SWIPE TO ROTATE</Text>
			<Text style={styles.subTitle}>AND SELECT AN OPTION</Text>
		</View>
	</View>
);
