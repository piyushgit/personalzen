import React, { Component } from 'react';
import { View, Image } from 'react-native';
import PropTypes from 'prop-types';

import THREE from '../../../lib/three';
import { withTHREE } from '../../../components/three';

const sizeShape = PropTypes.shape({
  width: PropTypes.number,
  height: PropTypes.number,
});

class MenuItem extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    renderer: PropTypes.object.isRequired,
    position: PropTypes.object,
    selected: PropTypes.bool,
    screen: sizeShape.isRequired,
    size: sizeShape.isRequired,
    image: Image.propTypes.source.isRequired,
  };

  static defaultProps = {
    selected: false,
    position: null,
  };

  componentDidMount() {
    this.createScene(this.props.renderer);
  }

  componentDidUpdate(prevProps) {
    if (!this.plane) return;

    if (this.props.selected && !prevProps.selected) {
      this.plane.material.color.setHex(0xff0000);
    }

    if (this.props.position) {
      this.plane.position.copy(this.props.position);
    }

    if (!this.mesh) return;

    if (this.props.selected && !prevProps.selected) {
      this.plane.material.color.setHex(0xff0000);
    }

    if (this.props.position) {
      this.mesh.position.copy(this.props.position);
    }
  }

  createScene({ scene, loadTexture }) {
    const { width, height } = this.props.size;
    const { image, id } = this.props;

    const geometry = new THREE.PlaneGeometry(width, height, 1);

    const material = new THREE.MeshBasicMaterial({
      transparent: true,
      map: loadTexture(image),
    });
    material.blending = THREE.CustomBlending;
    material.blendSrc = THREE.OneFactor;
    material.blendDst = THREE.OneMinusSrcAlphaFactor;

    this.plane = new THREE.Mesh(geometry, material);
    this.plane.userData.key = id;

    scene.add(this.plane);

    const clickAreaMaterial = new THREE.MeshBasicMaterial({
      transparent: true,
      opacity: 0,
    });

    const clickArea = new THREE.PlaneGeometry(width + 25, height + 35, 1);
    this.mesh = new THREE.Mesh(clickArea, clickAreaMaterial);
    this.mesh.userData.key = id;
    scene.add(this.mesh);
  }

  render() {
    const { width, height } = this.props.screen;
    return (<View style={{ width, height }} />);
  }
}

export default withTHREE(MenuItem);
