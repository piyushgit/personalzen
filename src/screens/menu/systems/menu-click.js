import THREE from '../../../lib/three';

const raycaster = new THREE.Raycaster();

const TOUCH_HIGHLIGHT_COLOR = 0x00ff90;
const CLEAR_COLOR = 0xffffff;

export default (entities, { touches, screen, dispatch }) => {
  const { width, height } = screen;
  const { camera, scene } = entities;

  const mousePress = touches.find(x => x.type === 'press');

  if (mousePress) {
    const mouse = new THREE.Vector2();
    mouse.x = ((mousePress.event.pageX / width) * 2) - 1;
    mouse.y = -((mousePress.event.pageY / height) * 2) + 1;

    // update the picking ray with the camera and mouse position
    raycaster.setFromCamera(mouse, camera);

    // calculate objects intersecting the picking ray
    const intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0) {
      let multipleTouches = false;
      intersects
        .map(x => entities[x.object.userData.key])
        .filter(x => x)
        .forEach((entity) => {
          if (!multipleTouches) {
            dispatch({ type: 'menu-click', entity });
            multipleTouches = true;
          }
        });


      // Touch highlight
      intersects.map(x => x.object.userData.key)
        .filter(key => entities[key])
        .forEach((key) => {
          const objects = scene.children
            .filter(item => item.userData.key === key);

          // Set selection color
          objects.forEach(item => item.material.color.setHex(TOUCH_HIGHLIGHT_COLOR));

          // Clear touch highlight
          setTimeout(() => {
            objects.forEach(item => item.material.color.setHex(CLEAR_COLOR));
          }, 2000);
        });
    }
  }

  return entities;
};
