import Rotate from './rotate';
import MenuClick from './menu-click';
import Fps from './fps';

export default [Rotate, MenuClick, Fps];
