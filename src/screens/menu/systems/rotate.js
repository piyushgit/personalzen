import THREE from '../../../lib/three';

const xAxis = new THREE.Vector3(1, 0, 0);
const yAxis = new THREE.Vector3(0, 1, 0);

function rotateOnAxis(object, axis, angle) {
  const q1 = new THREE.Quaternion();
  q1.setFromAxisAngle(axis, angle);
  object.quaternion.premultiply(q1);
  object.rotation.setFromQuaternion(object.quaternion);
}

export default function rotate(entities, { touches }) {
  const { planet } = entities;

  touches.filter(t => t.type === 'move').forEach((t) => {
    rotateOnAxis(planet, xAxis, t.delta.pageY * 0.005);
    rotateOnAxis(planet, yAxis, t.delta.pageX * 0.005);
  });

  Object.keys(entities)
    .filter(x => x.startsWith('menu-'))
    .map(x => entities[x])
    .forEach((menu) => {
      menu.position = menu.anchor
        .clone().applyEuler(planet.rotation);
    });

  return entities;
}
