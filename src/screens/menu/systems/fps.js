export default function (entities, { dispatch }) {
  const { fps } = entities;

  fps.total += 1;

  if (fps.total === 3) {
    dispatch({ type: 'render-started' });
  }

  return entities;
}
