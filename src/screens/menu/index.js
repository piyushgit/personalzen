import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { StyleSheet, View, Animated, Linking } from 'react-native';
import { GameEngine, DefaultTimer } from 'react-native-game-engine';
import { StackActions, NavigationActions } from 'react-navigation';

import { THREEView, THREEContext, THREERenderer } from '../../components/three';

import SplashScreen from '../../components/splash';
import TitleOverlay from './components/title-overlay';
import Menu from './entities/menu';
import Systems from './systems';

import { updateLastAccess, updateAchievementScholar } from '../../actions';
import { ACHIEVEMENT_18_KEY } from '../collection/config';
import { analytics } from '../../utils/analytics';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    zIndex: 200,
  },
});

class MainMenu extends PureComponent {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
    nav: PropTypes.shape().isRequired,
    updateLastAccess: PropTypes.func.isRequired,
    updateAchievementScholar: PropTypes.func.isRequired,
    achievements: PropTypes.object.isRequired,
  };

  state = {
    timer: new DefaultTimer(),
    running: true,
    splash: true,
    splashFadeOut: new Animated.Value(1),
  };

  componentDidMount() {
    this.props.updateLastAccess();
  }

  componentDidUpdate() {
    const { index, routes } = this.props.nav;
    const { routeName } = routes[index];

    // Pause game loop if current screen is not Main Menu
    const running = routeName === 'MainMenu';
    if (running !== this.state.running) {
      this.setState({ running }); // eslint-disable-line
    }
  }

  onMenuClick(entity) {
    if (entity.id === 'menu-play') {
      this.props.navigation.navigate('WorldMap');
      analytics.eventLog('Homepage: Play');
    } else if (entity.id === 'menu-tutorial') {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Tutorial' })],
      });

      this.props.navigation.dispatch(resetAction);
    } else if (entity.id === 'menu-weekly-goals') {
      analytics.eventLog('Homepage: Goals and Tracking');
      // this.props.navigation.navigate('Settings');
      this.props.navigation.navigate('StressTracking', { isGoal: true });
    } else if (entity.id === 'menu-schollar-tower') {
      analytics.eventLog('Homepage: Scholar’s Tower');
      if (!this.props.achievements[ACHIEVEMENT_18_KEY].achieved) {
        this.props.updateAchievementScholar();
      }
      const url = 'http://www.personalzen.com/';
      Linking.openURL(url);
    }
    // if (entity.id === 'menu-weekly-goals') {
    //   this.props.navigation.navigate('Settings');
    // }                  Megha did Comment it out
    if (entity.id === 'menu-collection') {
      this.props.navigation.navigate('Collection', { backToMap: 'true' });
    }
  }

  handleEvent = ev => {
    switch (ev.type) {
      case 'menu-click':
        this.onMenuClick(ev.entity);
        break;
      case 'render-started':
        Animated.timing(this.state.splashFadeOut, {
          toValue: 0,
          duration: 500,
          useNativeDriver: true, // <-- Add this
        }).start(() => {
          this.setState({ splash: false });
        });
        break;
      default:
        break;
    }
  };

  render() {
    const { timer, splash, splashFadeOut, running } = this.state;

    return (
      <View style={{ position: 'absolute', width: '100%', height: '100%' }}>
        <THREEView>
          <THREEContext.Consumer>
            {({ camera, scene }) => (
              <GameEngine
                style={styles.container}
                timer={timer}
                running={running}
                onEvent={this.handleEvent}
                renderer={THREERenderer}
                systems={Systems}
                entities={Menu(scene, camera)}
              >
                <TitleOverlay />
              </GameEngine>
            )}
          </THREEContext.Consumer>
        </THREEView>
        {splash && (
          <Animated.View
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              opacity: splashFadeOut,
            }}
          >
            <SplashScreen />
          </Animated.View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
  achievements: state.achievements,
});

const mapDispatchToProps = {
  updateLastAccess,
  updateAchievementScholar,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainMenu);
