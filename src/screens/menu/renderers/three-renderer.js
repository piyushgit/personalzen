import React from 'react';

import { THREEContext } from './three-context';

function renderChildren(state, screen) {
  return Object.keys(state)
    .filter(key => state[key].renderer)
    .map((key) => {
      const entity = state[key];
      if (typeof entity.renderer === 'object') {
        return <entity.renderer.type key={key} {...entity} screen={screen} />;
      } else if (typeof entity.renderer === 'function') {
        return <entity.renderer key={key} {...entity} screen={screen} />;
      }
      return null;
    });
}

export default (state, screen) => (
  <THREEContext.Consumer>
    {(context) => {
      context.renderScene();
      return renderChildren(state, screen);
    }}
  </THREEContext.Consumer>
);
