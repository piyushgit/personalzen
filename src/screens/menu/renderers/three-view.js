import React from 'react';
import PropTypes from 'prop-types';
import { Dimensions, StyleSheet, View } from 'react-native';
import { WebGLView } from 'react-native-webgl';

import THREE from '../../../lib/three';
import { THREEContext } from './three-context';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  webglView: {
    position: 'absolute',
    backgroundColor: 'transparent',
  },
});

class THREEView extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      initialized: false,
      width: 0,
      height: 0,
    };
  }

  onLayout() {
    const { width, height } = Dimensions.get('window');
    this.setState({ width, height });
  }

  onContextCreate = (gl) => {
    this.gl = gl;

    const rngl = gl.getExtension('RN');
    this.rngl = rngl;

    const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;
    const renderer = new THREE.WebGLRenderer({
      canvas: {
        width,
        height,
        style: {},
        addEventListener: () => { },
        removeEventListener: () => { },
        clientHeight: height,
      },
      context: gl,
    });
    renderer.setSize(width, height);
    renderer.setClearColor(0xFFFFFF, 1);
    this.renderer = renderer;

    const camera = new THREE.PerspectiveCamera(45, width / height, 0.2, 2000);
    this.camera = camera;

    const scene = new THREE.Scene();
    this.scene = scene;

    this.setState({ initialized: true });
  }

  loadTexture = (image) => {
    const tx = new THREE.Texture();
    const properties = this.renderer.properties.get(tx);
    const options = { image, yflip: true };
    this.rngl
      .loadTexture(options)
      .then(({ texture }) => {
        /* eslint-disable */
        properties.__webglTexture = texture;
        properties.__webglInit = true;
        /* eslint-enable */
        texture.needsUpdate = true;
      });

    return tx;
  }

  renderScene() {
    const { initialized } = this.state;
    if (!initialized) return;

    this.renderer.render(this.scene, this.camera);

    this.gl.flush();
    this.rngl.endFrame();
  }

  render() {
    const { width, height, initialized } = this.state;

    return (
      <View
        style={styles.container}
        onLayout={evt => this.onLayout(evt)}
      >
        {(width !== 0 && height !== 0) &&
          <WebGLView
            style={[styles.webglView, { width, height }]}
            onContextCreate={gl => this.onContextCreate(gl)}
          />
        }
        {initialized &&
          <THREEContext.Provider value={this}>
            <View
              style={[styles.webglView, { width, height }]}
            >
              {this.props.children}
            </View>
          </THREEContext.Provider>
        }
      </View>
    );
  }
}

export default THREEView;
