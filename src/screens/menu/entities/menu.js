import React from 'react';
import { Dimensions } from 'react-native';

import THREE from '../../../lib/three';

import Planet from '../../../components/planet';
import MenuItem from '../components/menu-item';

const menuScholarTower = require('../../../../assets/images/menu/menu-scholar-tower.png');
const menuTutorial = require('../../../../assets/images/menu/menu-tutorial.png');
const menuPlay = require('../../../../assets/images/menu/menu-play.png');
const menuGoals = require('../../../../assets/images/menu/menu-goals-tracking.png');
const menuCollection = require('../../../../assets/images/menu/menu-collection.png');

const MENU_OFFSET = 180;
const PLANET_INITIAL_ROTATION = new THREE.Euler(-0.995, -0.560, -0.700);
const PLANET_INITIAL_POSITION = new THREE.Vector3(0, 0, 0);

function cameraDistance() {
  const { height, width } = Dimensions.get('window');
  const targetBaseLineWidth = 200;
  const targetWidth = (targetBaseLineWidth * height) / width;
  const distance = targetWidth / (Math.tan(22.5 * (Math.PI / 180)));
  return distance;
}

export default (scene, camera) => {
  camera.position.z = cameraDistance();
  camera.position.x = 0;
  camera.position.y = 0;
  camera.updateProjectionMatrix();

  return {
    scene,
    camera,
    fps: { total: 0 },
    planet: {
      quaternion: new THREE.Quaternion().setFromEuler(PLANET_INITIAL_ROTATION),
      rotation: PLANET_INITIAL_ROTATION,
      position: PLANET_INITIAL_POSITION,
      renderer: <Planet />,
    },
    'menu-tutorial': {
      id: 'menu-tutorial',
      anchor: new THREE.Vector3(0, -1, 0).normalize().multiplyScalar(MENU_OFFSET),
      image: menuTutorial,
      size: { width: 75, height: 10 },
      renderer: <MenuItem />,
    },
    'menu-schollar-tower': {
      id: 'menu-schollar-tower',
      anchor: new THREE.Vector3(0, 0, 1).normalize().multiplyScalar(MENU_OFFSET),
      image: menuScholarTower,
      size: { width: 85, height: 28 },
      renderer: <MenuItem />,
    },
    'menu-play': {
      id: 'menu-play',
      anchor: new THREE.Vector3(1, 1, 0.7).normalize().multiplyScalar(MENU_OFFSET),
      image: menuPlay,
      size: { width: 75, height: 9 },
      renderer: <MenuItem />,
    },
    'menu-weekly-goals': {
      id: 'menu-weekly-goals',
      anchor: new THREE.Vector3(1, -0.7, -0.7).normalize().multiplyScalar(MENU_OFFSET),
      image: menuGoals,
      size: { width: 91, height: 27 },
      renderer: <MenuItem />,
    },
    'menu-collection': {
      id: 'menu-collection',
      anchor: new THREE.Vector3(1, -0.9, 0.8).normalize().multiplyScalar(MENU_OFFSET),
      image: menuCollection,
      size: { width: 119.4, height: 9 },
      renderer: <MenuItem />,
    },
  };
};
