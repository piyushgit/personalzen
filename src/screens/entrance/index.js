import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';

import { View, Image } from 'react-native';

import { login, resetSounds, updateSaturation } from '../../actions';
import { analytics } from '../../utils/analytics';

const background = require('../../../assets/images/entrance/background.png');

class EntranceScreen extends Component {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
    settings: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
    resetSounds: PropTypes.func.isRequired,
    updateSaturation: PropTypes.func.isRequired,
  };

  componentDidMount() {
    console.log('In entrance/index.js, at the start of componentDidMount.');
    analytics.eventLog('App: App launched');
    SplashScreen.hide();
    const { lastAccess } = this.props.settings;

    this.props.login();

    this.props.resetSounds();

    this.props.updateSaturation();

    // this.props.navigation.navigate(lastAccess ? 'MainMenu' : 'Subscription'); // For First time it will display Subscription screen and then it will display MainMenu every time..

    this.props.navigation.navigate(lastAccess ? 'MainMenu' : 'Tutorial'); // Originally it was set up like this
  }

  render() {
    return (
      <View style={{ position: 'absolute', width: '100%', height: '100%' }}>
        <Image
          style={{
            position: 'absolute',
            width: '100%',
            height: '100%',
          }}
          source={background}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    settings: state.settings,
  };
}

const mapDispatchToProps = {
  login,
  resetSounds,
  updateSaturation,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EntranceScreen);
