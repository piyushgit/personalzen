import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import { View, Text, StyleSheet, Switch, TouchableOpacity, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

import { THIN_FONT } from '../../constants/styles';
import {
  BACKGROUND_COLOR_GREY_E5,
  BACKGROUND_COLOR_WHITE,
} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    ...THIN_FONT,
    flex: 4,
    textAlign: 'left',
    fontSize: 16,
  },
});

export default class Row extends Component {
  static propTypes = {
    data: PropTypes.object,
    setModalVisible: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired,
  }

  static defaultProps = {
    data: {},
  }

  constructor(props) {
    super(props);

    this.state = {
      toggleOverlay: false,
      window: Dimensions.get('window'),
    };
  }

  onPressEdit = () => {
    const { item } = this.props.data;
    this.props.setModalVisible(item);
  }

  onValueChange = (placeboMode) => {
    const { item } = this.props.data;

    item.placeboMode = placeboMode;

    this.props.updateUser(item);
  }

  selectUser = () => {
    const { toggleOverlay } = this.state;
    const { item } = this.props.data;
    if (_.isEmpty(item.subjectId)) {
      this.props.setModalVisible(item);
    } else {
      this.setState({
        toggleOverlay: !toggleOverlay,
      });
    }
  }

  render() {
    const { data } = this.props;
    const { toggleOverlay, window } = this.state;
    const grey = BACKGROUND_COLOR_GREY_E5;
    const white = BACKGROUND_COLOR_WHITE;

    return (
      <TouchableOpacity
        onPress={this.selectUser}
      >
        <View
          style={[
            styles.container,
            { backgroundColor: data.index % 2 ? grey : white },
          ]}
        >
          <Text
            style={[styles.text, { fontSize: window.width / 20 }]}
            onPress={this.onPressEdit}
          >
            {data.item.username}
          </Text>
          <Text
            style={[styles.text, { fontSize: window.width / 20 }]}
          >
            {moment(data.item.downloadDate).format('MMMM Do YYYY, h:mm:ss a')}
          </Text>
          <Switch
            style={{ flex: 2 }}
            value={data.item.placeboMode}
            onValueChange={this.onValueChange}
          />
        </View>
        {toggleOverlay &&
          <View style={{ backgroundColor: data.index % 2 ? grey : white }}>
            <View style={styles.container}>
              <Text
                style={[styles.text, { fontSize: window.width / 20 }]}
              >
                Subject ID: {data.item.subjectId}
              </Text>
            </View>
            <View style={styles.container}>
              <Text
                style={[styles.text, { fontSize: window.width / 20 }]}
              >
                Comments: {data.item.comments}
              </Text>
            </View>
          </View>
        }
      </TouchableOpacity>
    );
  }
}

