import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import { REGULAR_FONT } from '../../constants/styles';

const styles = StyleSheet.create({
  button: {
    padding: 10,
  },
  container: {
    padding: 10,
  },
  loadingContainer: {
    padding: 20,
  },
  title: {
    ...REGULAR_FONT,
    textAlign: 'center',
  },
});

export default class Footer extends Component {
  static propTypes = {
    getUsers: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    fetchingStatus: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      window: Dimensions.get('window'),
    };
  }
  render() {
    const { window } = this.state;
    const {
      page,
      getUsers,
      fetchingStatus,
      error,
    } = this.props;

    if (fetchingStatus) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.button}
          onPress={() => getUsers(page)}
          disabled={!_.isEmpty(error)}
        >
          <Text
            style={[
              styles.title,
              { fontSize: window.width / 24 },
            ]}
          >
            {_.isEmpty(error) ? 'Load More' : error}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
