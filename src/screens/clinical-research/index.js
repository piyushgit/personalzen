import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  FlatList,
  View,
  SafeAreaView,
  RefreshControl,
  TextInput,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import ProportionalImage from '../../components/proportional-image';
import BackButton from '../../components/back-button';

import Row from './row';
import Header from './header';
import Footer from './footer';
import Modal from './modal';

import { REGULAR_FONT } from '../../constants/styles';
import {
  BACKGROUND_COLOR_GREY_C1,
  BACKGROUND_COLOR_WHITE,
} from '../../constants/colors';

import { updateUser, fetchUsers } from '../../services/user';

const backButton = require('../../../assets/images/stress-tracking/nav-buttom-back.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR_WHITE,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  safeArea: {
    backgroundColor: BACKGROUND_COLOR_GREY_C1,
  },
  searchContainer: {
    padding: 8,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: BACKGROUND_COLOR_GREY_C1,
  },
  input: {
    ...REGULAR_FONT,
    flex: 1,
    paddingHorizontal: 8,
    backgroundColor: BACKGROUND_COLOR_WHITE,
    borderRadius: 2,
  },
  backButton: {
    padding: 4,
  },
  loading: {
    backgroundColor: BACKGROUND_COLOR_WHITE,
    flex: 1,
    padding: 20,
  },
});

class ClinicalResearchScreen extends Component {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      window: Dimensions.get('window'),
      dataSource: [],
      modalVisible: false,
      isLoading: true,
      selectedUser: {},
      fetchingStatus: false,
      page: 0,
      search: '',
      error: '',
    };
  }

  componentWillMount() {
    const { page } = this.state;

    this.getUsers(page);
  }

  onBackPress = () => {
    this.props.navigation.pop(1);
    return true;
  }

  onFetchingStatus() {
    const { fetchingStatus } = this.state;
    this.setState({
      fetchingStatus: !fetchingStatus,
    });
  }

  onRefresh = () => {
    this.setState({
      dataSource: [],
      page: 0,
      error: '',
    });
    this.getUsers(0);
  }

  onCommentsChange = (text) => {
    const { selectedUser } = this.state;
    selectedUser.comments = text;
    this.setState({
      selectedUser,
    });
  }

  onChangeText = (text) => {
    this.setState({
      search: text,
      dataSource: [],
      error: '',
      fetchingStatus: true,
    });

    fetchUsers(1, text)
      .then((response) => {
        this.setState({
          isLoading: false,
          fetchingStatus: false,
          dataSource: response.data,
          page: 1,
        });
      })
      .catch(() => {
        this.setState({
          isLoading: false,
          fetchingStatus: false,
          dataSource: [],
          page: 0,
          error: 'Error while fetching user list',
        });
      });
  }

  onSubjectIdChange = (text) => {
    const { selectedUser } = this.state;
    selectedUser.subjectId = text;
    this.setState({
      selectedUser,
    });
  }

  getUsers = (page) => {
    const { search } = this.state;
    const nextPage = page + 1;
    this.setState({
      fetchingStatus: true,
    });

    return fetchUsers(nextPage, search)
      .then((response) => {
        this.setState({
          isLoading: false,
          fetchingStatus: false,
          dataSource: [...this.state.dataSource, ...response.data],
          page: nextPage,
        });
      })
      .catch(() => {
        this.setState({
          isLoading: false,
          fetchingStatus: false,
          dataSource: [],
          page: 0,
          error: 'Error while fetching user list',
        });
      });
  }

  setModalVisible = (user) => {
    const { modalVisible } = this.state;

    this.setState({
      modalVisible: !modalVisible,
      selectedUser: user,
    });

    this.onRefresh();
  }

  updateUser = (user) => {
    this.setState({
      isLoading: true,
    });

    updateUser(user)
      .then(() => {
        this.setState({
          isLoading: false,
          selectedUser: user,
        });
      });
  }

  render() {
    const {
      dataSource,
      modalVisible,
      selectedUser,
      isLoading,
      page,
      fetchingStatus,
      search,
      error,
      window,
    } = this.state;

    return (
      <View style={styles.container}>
        <BackButton onPress={this.onBackPress} />
        <SafeAreaView style={styles.safeArea}>
          <View style={styles.searchContainer}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
              style={styles.backButton}
            >
              <ProportionalImage style={{}} source={backButton} ratio={0.6} />
            </TouchableOpacity>
            <TextInput
              style={[styles.input,
              {
                fontSize: window.width / 24,
                height: window.height / 16,
              }]}
              placeholder="Search..."
              onChangeText={text => this.onChangeText(text)}
              value={search}
            />
          </View>
        </SafeAreaView>
        <FlatList
          style={{ flex: 1 }}
          data={dataSource}
          renderItem={
            data =>
              (<Row
                data={data}
                setModalVisible={this.setModalVisible}
                updateUser={this.updateUser}
              />)
          }
          keyExtractor={user => user.id}
          refreshControl={
            <RefreshControl
              refreshing={isLoading}
              onRefresh={this.onRefresh}
            />
          }
          stickyHeaderIndices={[0]}
          ListHeaderComponent={() => (<Header
            onSearch={this.onSearch}
            search={search}
          />)}
          ListFooterComponent={() => (<Footer
            getUsers={this.getUsers}
            page={page}
            fetchingStatus={fetchingStatus}
            error={error}
          />)}
        />
        <Modal
          modalVisible={modalVisible}
          setModalVisible={this.setModalVisible}
          selectedUser={selectedUser}
          updateUser={this.updateUser}
          onCommentsChange={this.onCommentsChange}
          onSubjectIdChange={this.onSubjectIdChange}
        />
      </View>
    );
  }
}

export default ClinicalResearchScreen;
