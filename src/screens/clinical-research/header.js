import React, { Component } from 'react';
import { View, StyleSheet, Text, Dimensions } from 'react-native';

import { REGULAR_FONT } from '../../constants/styles';
import { BACKGROUND_COLOR_GREY_C1, BACKGROUND_COLOR_WHITE } from '../../constants/colors';

const styles = StyleSheet.create({
	headerContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-start',
		backgroundColor: BACKGROUND_COLOR_WHITE
	},
	columnContainer: {
		flex: 1,
		padding: 8,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: BACKGROUND_COLOR_GREY_C1,
		justifyContent: 'space-between'
	},
	title: {
		...REGULAR_FONT,
		textAlign: 'center'
	}
});

export default class Header extends Component {
	constructor(props) {
		super(props);

		this.state = {
			window: Dimensions.get('window')
		};
	}

	render() {
		const { window } = this.state;
		return (
			<View style={styles.headerContainer}>
				<Text
					style={[
						styles.title,
						{
							fontSize: window.width / 18,
							padding: window.height / 64
						}
					]}
				>
					Personal Zen Clinical Research
				</Text>
				<View style={styles.columnContainer}>
					<Text style={[styles.title, { fontSize: window.width / 24 }]}>Username</Text>
					<Text style={[styles.title, { fontSize: window.width / 24 }]}>Download Date</Text>
					<Text style={[styles.title, { fontSize: window.width / 24 }]}>Placebo</Text>
				</View>
			</View>
		);
	}
}
