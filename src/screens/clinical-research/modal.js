import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  Text,
  View,
  TextInput,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import { REGULAR_FONT } from '../../constants/styles';

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    borderRadius: 5,
    marginTop: 10,
  },
  container: {
    padding: 10,
  },
  title: {
    ...REGULAR_FONT,
    textAlign: 'center',
  },
  text: {
    marginTop: 10,
  },
  textInput: {
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
  },
});

export default class PlaceboModeModal extends Component {
  static propTypes = {
    modalVisible: PropTypes.bool.isRequired,
    setModalVisible: PropTypes.func.isRequired,
    selectedUser: PropTypes.object.isRequired,
    updateUser: PropTypes.func.isRequired,
    onCommentsChange: PropTypes.func.isRequired,
    onSubjectIdChange: PropTypes.func.isRequired,
  }

  addInformation = () => {
    const { selectedUser } = this.props;
    this.props.updateUser(selectedUser);
    this.props.setModalVisible(selectedUser);
  }

  render() {
    const window = Dimensions.get('window');
    const {
      modalVisible,
      selectedUser,
      onCommentsChange,
      onSubjectIdChange,
    } = this.props;
    return (
      <Modal
        animationType="fade"
        transparent={false}
        visible={modalVisible}
      >
        <View style={{ marginTop: 22 }}>
          <View>
            <Text
              style={[
                styles.title,
                {
                  fontSize: window.width / 18,
                  padding: window.height / 64,
                },
              ]}
            >
              {selectedUser.username}
            </Text>
            <View style={styles.container}>
              <Text style={styles.text}>Subject ID</Text>
              <TextInput
                style={[
                  styles.textInput,
                  {
                    height: window.height / 16,
                  },
                ]}
                underlineColorAndroid="transparent"
                onChangeText={text => onSubjectIdChange(text)}
                value={selectedUser.subjectId}
              />
              <Text style={styles.text}>Comments</Text>
              <TextInput
                style={[
                  styles.textInput,
                  {
                    height: window.height / 4,
                  },
                ]}
                multiline
                underlineColorAndroid="transparent"
                onChangeText={text => onCommentsChange(text)}
                value={selectedUser.comments}
              />
              <TouchableOpacity
                style={[
                  styles.button,
                  {
                    backgroundColor: '#007AFF',
                  },
                ]}
                onPress={() => this.addInformation()}
              >
                <Text style={{ color: '#FFF' }}>Add</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this.props.setModalVisible(selectedUser)}
              >
                <Text>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
