import _ from 'lodash';
import regression from 'regression';

import getDistance from '../../../utils/distance';
import { K_MINIMUM_ACCURACY } from '../config';

import sparklesPosition from '../../../utils/sparkle-position';

function featuresOfPath(path) {
  // Calculate basic features
  let xMagnitude = 0;
  let yMagnitude = 0;
  let smallestX = Number.MAX_VALUE;
  let biggestX = -Number.MAX_VALUE;
  let smallestY = Number.MAX_VALUE;
  let biggestY = -Number.MAX_VALUE;

  path.forEach((cur) => {
    smallestX = Math.min(smallestX, cur[0]);
    biggestX = Math.max(biggestX, cur[0]);
    smallestY = Math.min(smallestY, cur[1]);
    biggestY = Math.max(biggestY, cur[1]);
  });

  for (let i = 1; i < path.length; i += 1) {
    const prev = path[i - 1];
    const cur = path[i];
    xMagnitude += (cur[0] - prev[0]);
    yMagnitude += (cur[1] - prev[1]);
  }

  // Calculate composite features
  const shapeWidth = biggestX - smallestY;
  const shapeHeight = biggestY - smallestY;
  const center = {
    x: smallestX + (shapeWidth / 2),
    y: smallestY - (shapeHeight / 2),
  };

  return {
    center,
    magnitude: {
      x: xMagnitude,
      y: yMagnitude,
    },
  };
}

function scoreAccuracyOfPath(path, targetPath) {
  // Linear regression Path
  const arrayPath = [];

  path.forEach((element) => {
    const point = [
      element.x,
      element.y,
    ];
    arrayPath.push(point);
  });

  const resultPath = regression.linear(arrayPath);

  // Linear regression TargetPath
  const arrayTargetPath = [];

  targetPath.forEach((element) => {
    const point = [
      element.x,
      element.y,
    ];
    arrayTargetPath.push(point);
  });

  const resultTargetPath = regression.linear(arrayTargetPath);
  // Accuracy score
  const targetFeatures = featuresOfPath(resultTargetPath.points);
  const pathFeatures = featuresOfPath(resultPath.points);

  const dx = targetFeatures.center.x - pathFeatures.center.x;
  const dy = targetFeatures.center.y - pathFeatures.center.y;

  const centersDiff = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)) / 480.0; // eslint-disable-line
  const xMagDiff = Math.abs(targetFeatures.magnitude.x - pathFeatures.magnitude.x) / 480.0;
  const yMagDiff = Math.abs(targetFeatures.magnitude.y - pathFeatures.magnitude.y) / 480.0;

  // If signs differ, reject
  if ((targetFeatures.magnitude.x * pathFeatures.magnitude.x) < 0) {
    return -1;
  }
  if ((targetFeatures.magnitude.y * targetFeatures.magnitude.y) < 0) {
    return -1;
  }

  // If covered under 10% of the size of the path, reject. (Handles taps.)
  if (((targetFeatures.magnitude.x / pathFeatures.magnitude.x) < 0.1) &&
    ((targetFeatures.magnitude.y / pathFeatures.magnitude.y) < 0.1)) {
    return -1;
  }

  // No rejections, return score
  const x = 1.0 - ((xMagDiff + yMagDiff + centersDiff) / 3.0);
  return 1.6459 * (1.0 / 1000.0) * (1.0 / 2.0) * (1300000.0 / 40.0) * x;
}

function scoreTouchDelay(targetPath, delay, point) {
  const d1 = getDistance(point, targetPath[0]);
  const d2 = getDistance(point, targetPath[1]);
  if (d1 > K_MINIMUM_ACCURACY && d2 > K_MINIMUM_ACCURACY) {
    return -1;
  }
  return (1.6459 * (1 / 1000.0) * (1500000.0 / 40.0) *
    Math.max(0.01, Math.min(1.0, 1.0 - (delay / 3.0)))) / 2.0;
}

function scoreDrawnPath(path, targetPath, delay, region, ratio) {
  const touchPosition = sparklesPosition(path, region, ratio);
  const pathSparkles = _.concat(touchPosition, path);

  // Accuracy
  const accuracyScore = scoreAccuracyOfPath(pathSparkles, targetPath);
  if (accuracyScore === -1) {
    return -1;
  }

  // Speed score
  const speedScore = scoreTouchDelay(pathSparkles, delay, targetPath[0]);
  if (speedScore === -1) {
    return -1;
  }

  // Total
  return speedScore + accuracyScore;
}

const PathTrace = (entities, { time, touches, dispatch }) => {
  if (!entities.round) return entities;

  const entityArray = entities.path.data;
  const { region, ratio } = entities.scene;
  const {
    state,
    traceStartTime = null,
  } = entities.round;

  // Record traced path
  if (state === 'trace') {
    let { traced = [] } = entities.path;

    touches.forEach((evt) => {
      switch (evt.type) {
        case 'start':
          traced = [evt.event];
          break;
        case 'move':
          traced.push(evt.event);
          break;
        case 'end': {
          traced.push(evt.event);
          const points = traced.map(e => ({ x: e.locationX, y: e.locationY }));
          const path = _.filter(points, item => item.y > K_MINIMUM_ACCURACY);
          const delay = (time.current - traceStartTime) / 1000;
          const score = scoreDrawnPath(entityArray, path, delay, region, ratio);

          if (score !== -1) {
            entities.path.score = score;
            entities.path.scoreTime = time.current;
            dispatch({
              type: 'path-traced',
              score,
              delay,
            });
          } else {
            dispatch({ type: 'wrong-path-trace' });
          }

          console.log('score:', score, 'delay:', delay);
          break;
        }
        default:
          break;
      }
    });

    entities.path.traced = traced;
  }

  return entities;
};

export default PathTrace;
