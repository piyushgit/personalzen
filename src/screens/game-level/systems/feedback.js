import { Dimensions } from 'react-native';

import {
  SWIPE_SPEED1_THRESHOLD,
  SWIPE_SPEED1_IMAGE,
  SWIPE_SPEED2_IMAGE,
} from '../config';
import { analytics } from '../../../utils/analytics';

const swipe1 = require('../../../../assets/audio/Swipe1.m4a');
const swipe3 = require('../../../../assets/audio/Swipe3.m4a');
const threeSwipe = require('../../../../assets/audio/ThreeSwipe.m4a');
const error = require('../../../../assets/audio/Error3.m4a');

let subsequentYellowStarbursts = 0;
const Feedback = (entities, { events, dispatch }) => {
  const pathTraced = events.find(e => e.type === 'path-traced');
  const wrongPathTrace = events.find(e => e.type === 'wrong-path-trace');

  if (pathTraced) {
    const { delay } = pathTraced;

    const { currentRoundIndex } = entities.scene;

    const position = {};

    const positivePivot = {};
    const negativePivot = {};


    if (entities.scene.rounds[currentRoundIndex + 1]) {
      const { negative, positive } = entities.scene.rounds[currentRoundIndex + 1].sprites;
      positivePivot.x = positive.position.x + (positive.size.width / 2);
      negativePivot.x = negative.position.x + (negative.size.width / 2);
      positivePivot.y = positive.position.y + (positive.size.height / 2);
      negativePivot.y = negative.position.y + (negative.size.height / 2);
      position.x = ((positivePivot.x + negativePivot.x) / 2);
      position.y = ((positivePivot.y + negativePivot.y) / 2);
    } else {
      const window = Dimensions.get('window');
      position.x = window.width / 2;
      position.y = window.height / 2;
    }

    let source;
    let showSparkles = false;
    if (delay < SWIPE_SPEED1_THRESHOLD) {
      source = SWIPE_SPEED2_IMAGE;
      if (subsequentYellowStarbursts < 3) {
        dispatch({
          type: 'play-sound',
          sound: swipe1,
        });
        subsequentYellowStarbursts += 1;
      } else {
        dispatch({
          type: 'play-sound',
          sound: swipe1,
        });
        subsequentYellowStarbursts = 1;
      }
      if (subsequentYellowStarbursts === 3) {
        analytics.eventLog('App: Three gold in a row to get starburst');
        dispatch({
          type: 'play-sound',
          sound: threeSwipe,
        });
        showSparkles = true;
      }
    } else {
      dispatch({
        type: 'play-sound',
        sound: swipe3,
      });
      source = SWIPE_SPEED1_IMAGE;
      subsequentYellowStarbursts = 0;
    }

    entities.traceFeedback = {
      ...entities.traceFeedback,
      position,
      show: true,
      showSparkles,
      source,
    };

    entities.sparklesPositive = {
      ...entities.sparklesPositive,
      show: false,
    };
  }

  if (wrongPathTrace) {
    dispatch({
      type: 'play-sound',
      sound: error,
    });
  }

  return entities;
};

export default Feedback;
