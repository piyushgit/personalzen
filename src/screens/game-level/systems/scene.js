import { Dimensions } from 'react-native';

import Level, { changeRound } from '../entities/level';

const Scene = (entities, { events, dispatch, time }) => {
  const {
    currentRoundIndex,
    rounds,
    startTime,
    ratio,
    user,
    region,
  } = entities.scene;

  const window = Dimensions.get('window');

  const roundCompleted = events.find(e => e.type === 'round-completed');
  const resetScene = events.find(e => e.type === 'reset-scene');

  if (resetScene) {
    entities = Level(region, window, ratio, rounds[0].isTutorial, user);
  }

  if (roundCompleted) {
    const nextRound = currentRoundIndex + 1;
    let delays = 0;

    if (nextRound >= rounds.length) {
      rounds.forEach((round) => {
        delays += round.delay;
      });
      dispatch({ type: 'scene-revitalized', delays });
    } else {
      entities = changeRound(entities, nextRound);
    }
  }

  if (!startTime) {
    entities.scene.startTime = time.current;
  }

  if (time.current - startTime > 1000 && !entities.round) {
    entities = changeRound(entities, 0);
  }

  return entities;
};

export default Scene;
