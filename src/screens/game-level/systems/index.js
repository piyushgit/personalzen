import PathTrace from './path-trace';
import Wind from './wind';
import Round from './round';
import Scene from './scene';
import Feedback from './feedback';

export {
  PathTrace,
  Wind,
  Round,
  Scene,
  Feedback,
};

export default [Round, Scene, PathTrace, Feedback, Wind];
