const Wind = (entities, { events }) => {
  if (!entities.path) return entities;

  const { data } = entities.path;
  const pathTraced = events.find(e => e.type === 'path-traced');

  if (pathTraced) {
    entities.path.data = data.map((item) => {
      item.traced = true;
      return item;
    });
  }

  return entities;
};

export default Wind;
