import { ROUND_START_DELAY, INTER_TRIAL_INTERVAL } from '../config';

const Round = (entities, { time, events, dispatch }) => {
  if (!entities.round) return entities;

  let {
    startTime = null,
    state = 'start',
  } = entities.round;

  if (!startTime) {
    startTime = time.current;
    entities.round.traceStartTime = time.current + ROUND_START_DELAY;
    entities.round.startTime = startTime;
  }

  switch (state) {
    case 'start': {
      if (time.current - startTime > ROUND_START_DELAY) {
        state = 'trace';
      }
      break;
    }
    case 'trace': {
      const pathTraced = events.find(x => x.type === 'path-traced');
      if (pathTraced) {
        const { delay, score } = pathTraced;
        entities.round.delay = delay;
        entities.round.score = score;
        state = 'traced';
      }
      break;
    }
    case 'traced': {
      if (time.current - entities.path.scoreTime > INTER_TRIAL_INTERVAL) {
        state = 'completed';
        dispatch({ type: 'round-completed' });
      }
      break;
    }
    default: break;
  }

  entities.round.state = state;

  return entities;
};

export default Round;
