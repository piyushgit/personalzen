import { GameEngine } from 'react-native-game-engine';

export default class engine extends GameEngine {
  constructor(props) {
    super(props);

    this.superDispatch = this.dispatch;

    this.onTouchEndHandler = (e) => {
      this.touchProcessor.process('end', e.nativeEvent);
      requestAnimationFrame((time) => {
        let dispatchedEvents = [];
        this.dispatch = (evt) => {
          dispatchedEvents.push(evt);
          if (this.props.onEvent) this.props.onEvent(evt);
        };

        do {
          this.updateHandler(time);
          this.events = dispatchedEvents;
          dispatchedEvents = [];
        } while (this.events.length > 0);

        this.dispatch = this.superDispatch;
      });
    };
  }
}
