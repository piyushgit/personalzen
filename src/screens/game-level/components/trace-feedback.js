import * as Animatable from 'react-native-animatable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Image, View } from 'react-native';

import {
  TRACE_FEEDBACK_IMAGE_DURATION,
  FEEDBACK_IMAGE_SIZE,
} from '../config';

const sourceDefault = require('../../../../assets/images/regions/plains-region/yellow-rose.png');
const sparkle = require('../../../../assets/images/mandalas/mini-sparkle.png');

export class Renderer extends PureComponent {
  static propTypes = {
    position: PropTypes.object,
    show: PropTypes.bool,
    index: PropTypes.number,
    source: Image.propTypes.source,
    showSparkles: PropTypes.bool,
  };

  static defaultProps = {
    position: {},
    show: false,
    index: 0,
    showSparkles: false,
    source: sourceDefault,
  };

  render() {
    const {
      position,
      show,
      source,
      showSparkles,
    } = this.props;

    if (!show) return null;

    const zoomInFadeOut = {
      0: {
        opacity: 0,
        scale: 2,
        rotate: '0deg',
      },
      0.1: {
        opacity: 1,
      },
      0.2: {
        opacity: 1,
        scale: 1,
      },
      0.7: {
        opacity: 1,
        scale: 1,
      },
      0.9: {
        opacity: 0,
        scale: 0,
      },
      1: {
        opacity: 0,
        scale: 0,
        rotate: '45deg',
      },
    };
    const zoomInFadeOutSparke = {
      0: {
        opacity: 0,
        scale: 2,
        rotate: '0deg',
      },
      0.28: {
        scale: 2,
      },
      0.31: {
        opacity: 1,
        scale: 1.8,
      },
      0.7: {
        opacity: 0,
        scale: 0,
      },
      0.8: {
        opacity: 0,
        scale: 0,
      },
      1: {
        opacity: 0,
        scale: 0,
        rotate: '-45deg',
      },
    };
    const mandalasRight = {
      0: {
        translateX: 0,
        scale: 1,
      },
      0.5: {
        translateX: 0,
        scale: 1,
        rotate: '0deg',
      },
      0.6: {
        translateX: 0,
      },
      1: {
        translateX: 300,
        scale: 0.2,
        rotate: '180deg',
      },
    };
    const mandalasLeft = {
      0: {
        translateX: 0,
        opacity: 0,
        scale: 0,
      },
      0.5: {
        translateX: 0,
        opacity: 0,
        scale: 1,
        rotate: '0deg',
      },
      0.6: {
        translateX: 0,
        opacity: 1,
      },
      1: {
        translateX: -300,
        opacity: 1,
        scale: 0.2,
        rotate: '180deg',
      },
    };

    return [

      <View>
        {showSparkles &&
          <View>
            <Animatable.View
              style={{
                position: 'absolute',
                left: position.x - (FEEDBACK_IMAGE_SIZE / 2),
                top: position.y - (FEEDBACK_IMAGE_SIZE / 2),
              }}
              animation={mandalasRight}
              duration={TRACE_FEEDBACK_IMAGE_DURATION}
              key={this.props.index}
              useNativeDriver
            >
              <Image
                source={source}
                style={{
                  width: FEEDBACK_IMAGE_SIZE,
                  height: FEEDBACK_IMAGE_SIZE,
                }}
              />
            </Animatable.View>
            <Animatable.View
              style={{
                position: 'absolute',
                left: position.x - (FEEDBACK_IMAGE_SIZE / 2),
                top: position.y - (FEEDBACK_IMAGE_SIZE / 2),
              }}
              animation={mandalasLeft}
              duration={TRACE_FEEDBACK_IMAGE_DURATION}
              key={this.props.index}
              useNativeDriver
            >
              <Image
                source={source}
                style={{
                  width: FEEDBACK_IMAGE_SIZE,
                  height: FEEDBACK_IMAGE_SIZE,
                }}
              />
            </Animatable.View>
            <Animatable.View
              style={{
                position: 'absolute',
                left: position.x - (FEEDBACK_IMAGE_SIZE * 2),
                top: position.y - (FEEDBACK_IMAGE_SIZE * 2),
              }}
              animation={zoomInFadeOutSparke}
              duration={TRACE_FEEDBACK_IMAGE_DURATION}
            >
              <Image
                source={sparkle}
                style={{
                  width: FEEDBACK_IMAGE_SIZE * 4,
                  height: FEEDBACK_IMAGE_SIZE * 4,
                }}
              />
            </Animatable.View>
          </View>
        }
      </View >,
      <Animatable.View
        style={{
          position: 'absolute',
          left: position.x - (FEEDBACK_IMAGE_SIZE / 2),
          top: position.y - (FEEDBACK_IMAGE_SIZE / 2),
        }}
        animation={zoomInFadeOut}
        duration={TRACE_FEEDBACK_IMAGE_DURATION}
        key={this.props.index}
        useNativeDriver
      >
        <Image
          source={source}
          style={{
            width: FEEDBACK_IMAGE_SIZE,
            height: FEEDBACK_IMAGE_SIZE,
          }}
        />
      </Animatable.View>,
    ];
  }
}

export default (position, ratio, index) => (
  {
    position,
    ratio,
    index,
    renderer: <Renderer />,
  }
);
