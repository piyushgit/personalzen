import * as Animatable from 'react-native-animatable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export class Renderer extends PureComponent {
  static propTypes = {
    position: PropTypes.object,
    index: PropTypes.string,
    rotation: PropTypes.string,
    delay: PropTypes.number,
    size: PropTypes.number,
    source: PropTypes.number.isRequired,
  };

  static defaultProps = {
    position: {},
    index: '0',
    delay: 0,
    rotation: '0deg',
    size: 20,
  };

  render() {
    const {
      position,
      rotation,
      delay,
      source,
      size,
    } = this.props;

    const zoomInFadeOut = {
      0: {
        opacity: 0.3,
        scale: 0.01,
        rotate: rotation,
      },
      0.33: {
        opacity: 1,
        scale: 1,
        rotate: rotation,
      },
      0.66: {
        opacity: 1,
        scale: 1,
        rotate: rotation,
      },
      1: {
        opacity: 0.3,
        scale: 0.01,
        rotate: rotation,
      },
    };

    return (
      <Animatable.Image
        source={source}
        iterationCount="infinite"
        style={{
          position: 'absolute',
          left: position.x,
          top: position.y,
          width: size,
          height: size,
        }}
        delay={delay}
        animation={zoomInFadeOut}
        duration={2000}
        key={this.props.index}
        useNativeDriver
      />
    );
  }
}

export default (position, index) => (
  {
    position,
    index,
    renderer: <Renderer />,
  }
);
