import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, Image, Platform } from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';

const imageDefault = require('../../../../assets/images/regions/water-region/background.png');
const imageGrayscaleDefault = require('../../../../assets/images/regions/water-region/background-grayscale.png');

export class Renderer extends PureComponent {
	static propTypes = {
		saturation: PropTypes.number,
		image: Image.propTypes.source,
		imageGrayscale: Image.propTypes.source
	};

	static defaultProps = {
		saturation: 0,
		image: imageDefault,
		imageGrayscale: imageGrayscaleDefault
	};

	render() {
		const { image, imageGrayscale, saturation } = this.props;
		const window = Dimensions.get('window');
		let { height } = window;
		if (Platform.OS === 'android') {
			height = ExtraDimensions.get('REAL_WINDOW_HEIGHT');
		}

		return [
			<Image
				key='image'
				style={{
					position: 'absolute',
					width: window.width,
					height
				}}
				source={image}
				resizeMode='stretch'
			/>,
			<Image
				key='imageGrayscale'
				style={{
					position: 'absolute',
					width: window.width,
					height,
					opacity: 1 - saturation
				}}
				source={imageGrayscale}
				resizeMode='stretch'
			/>
		];
	}
}

export default (image, imageGrayscale) => ({
	image,
	imageGrayscale,
	renderer: <Renderer />
});
