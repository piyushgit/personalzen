
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { View } from 'react-native';


import scale, { verticalScale } from '../../../utils/scale';
import { Renderer as Sparkle } from './sparkle';

const sparkle1 = require('../../../../assets/images/gameplay/sparkle1.png');
const sparkle2 = require('../../../../assets/images/gameplay/sparkle2.png');

const sparkles = [
  {
    index: 'sparkle-0',
    source: sparkle1,
    position: { x: 4, y: 0 },
    delay: 0,
    rotation: '5deg',
    size: scale(14),
  },
  {
    index: 'sparkle-1',
    source: sparkle2,
    position: { x: 21, y: 9 },
    delay: 100,
    rotation: '15deg',
    size: scale(15),
  },
  {
    index: 'sparkle-2',
    source: sparkle1,
    position: { x: 12, y: 14 },
    delay: 300,
    rotation: '45deg',
    size: scale(9),
  },
  {
    index: 'sparkle-3',
    source: sparkle1,
    position: { x: 0, y: 15 },
    delay: 800,
    rotation: '5deg',
    size: scale(14),
  },
  {
    index: 'sparkle-4',
    source: sparkle2,
    position: { x: 1, y: 25 },
    delay: 100,
    rotation: '15deg',
    size: scale(15),
  },
  {
    index: 'sparkle-5',
    source: sparkle1,
    position: { x: 17, y: 20 },
    delay: 300,
    rotation: '0deg',
    size: scale(17),
  },
  {
    index: 'sparkle-6',
    source: sparkle1,
    position: { x: 26, y: 17 },
    delay: 600,
    rotation: '30deg',
    size: scale(22),
  },
  {
    index: 'sparkle-7',
    source: sparkle2,
    position: { x: 13, y: 30 },
    delay: 100,
    rotation: '30deg',
    size: scale(25),
  },
  {
    index: 'sparkle-8',
    source: sparkle2,
    position: { x: 30, y: 30 },
    delay: 25,
    rotation: '30deg',
    size: scale(23),
  },
  {
    index: 'sparkle-9',
    source: sparkle2,
    position: { x: 2, y: 37 },
    delay: 350,
    rotation: '30deg',
    size: scale(19),
  },
  {
    index: 'sparkle-10',
    source: sparkle1,
    position: { x: 15, y: 40 },
    delay: 350,
    rotation: '60deg',
    size: scale(27),
  },
  {
    index: 'sparkle-11',
    source: sparkle1,
    position: { x: 32, y: 5 },
    delay: 500,
    rotation: '10deg',
    size: scale(20),
  },
  {
    index: 'sparkle-12',
    source: sparkle2,
    position: { x: 36, y: 19 },
    delay: 80,
    rotation: '30deg',
    size: scale(20),
  },
  {
    index: 'sparkle-13',
    source: sparkle2,
    position: { x: 34, y: 38 },
    delay: 120,
    rotation: '30deg',
    size: scale(27),
  },
];

export class Renderer extends PureComponent {
  static propTypes = {
    position: PropTypes.object,
    index: PropTypes.number,
    show: PropTypes.bool,
  };

  static defaultProps = {
    position: {},
    index: 0,
    show: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      sparkles: _.cloneDeep(sparkles),
    };
  }

  render() {
    const {
      position,
      show,
    } = this.props;
    if (!show) return null;

    return (
      <View
        style={{
          position: 'absolute',
          left: position.x,
          top: position.y,
        }}
        key={this.props.index}
      >
        {this.state.sparkles.map((sparkle, index) => (
          <Sparkle
            key={`${index}sparkle`} // eslint-disable-line
            source={sparkle.source}
            size={sparkle.size / 2}
            position={{
              x: scale(sparkle.position.x) - scale(28),
              y: verticalScale(sparkle.position.y) - verticalScale(30),
            }}
            index={sparkle.index}
            rotation={sparkle.rotation}
            delay={sparkle.delay * 2}
          />
        ))}
      </View>
    );
  }
}

export default (position, index, show, delay) => (
  {
    position,
    index,
    show,
    delay,
    renderer: <Renderer />,
  }
);
