import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import { REGULAR_FONT } from '../../../constants/styles';
import ProportionalImage from '../../../components/proportional-image';
import scale from '../../../utils/scale';

const stroke = require('../../../../assets/images/results/stroke.png');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerContent: {
    flex: 0,
    width: '100%',
    height: 80,
    alignItems: 'center',
  },
  containerText: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -15,
  },
  textSceneRevitalized: {
    ...REGULAR_FONT,
    fontSize: scale(30),
    letterSpacing: scale(7),
    color: 'white',
    textShadowColor: 'black',
    textShadowOffset: { width: 2, height: 2 },
    textShadowRadius: 10,
  },
  strokeImage: {
    opacity: 0.5,
    position: 'absolute',
  },
  pathsContainer: {
    height: 150,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  containerButtons: {
    alignItems: 'flex-start',
    padding: scale(10),
    flexDirection: 'row',
  },
  button: {
    margin: 5,
    width: scale(150),
    backgroundColor: '#38942c',
    borderRadius: scale(5),
    padding: scale(7),
    alignItems: 'center',
  },
  textButton: {
    fontSize: scale(16),
    color: 'white',
  },
  path: {},
});

export default class SceneRevitalized extends Component {
  static propTypes = {
    region: PropTypes.objectOf(PropTypes.object).isRequired,
    showButtons: PropTypes.bool,
    onPressContinue: PropTypes.func.isRequired,
    onPressSeeMyProgress: PropTypes.func.isRequired,
  };

  static defaultProps = {
    showButtons: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      topPaths: this.props.region.gameLevel.paths.slice(0, 3),
      bottomPaths: this.props.region.gameLevel.paths.slice(3, 6),
    };
  }

  render() {
    return (
      <Animatable.View style={styles.container} animation="fadeIn" useNativeDriver>
        <View style={[styles.pathsContainer, { alignItems: 'flex-end', paddingBottom: 30 }]}>
          {this.state.topPaths.map(path => (
            <ProportionalImage style={styles.path} source={path.img} ratio={scale(0.8)} />
          ))}
        </View>
        <View style={styles.centerContent}>
          <ProportionalImage
            style={styles.strokeImage}
            source={stroke}
            ratio={scale(0.47)}
          />
          <View style={styles.containerText}>
            <Text style={styles.textSceneRevitalized}>SCENE</Text>
            <Text style={styles.textSceneRevitalized}>REVITALIZED</Text>
          </View>
        </View>
        {this.props.showButtons &&
          <View style={styles.containerButtons}>
            <TouchableOpacity
              onPress={this.props.onPressSeeMyProgress}
            >
              <View style={styles.button}>
                <Text style={styles.textButton}>VIEW PROGRESS</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.props.onPressContinue}
            >
              <View style={styles.button}>
                <Text style={styles.textButton}>CONTINUE</Text>
              </View>
            </TouchableOpacity>
          </View>
        }
        <View style={[styles.pathsContainer, { alignItems: 'flex-start', paddingTop: 30 }]}>
          {this.state.bottomPaths.map((path, index) => (
            <ProportionalImage key={index} style={styles.path} source={path.img} ratio={scale(0.8)} /> // eslint-disable-line
          ))}
        </View>
      </Animatable.View>
    );
  }
}
