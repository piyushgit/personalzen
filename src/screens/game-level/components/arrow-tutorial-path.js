import * as Animatable from 'react-native-animatable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, Dimensions } from 'react-native';

import ProportionalImage from '../../../components/proportional-image';

import {
	TUTORIAL_ARROW_DISTANCE_X,
	TUTORIAL_ARROW_DISTANCE_Y,
	TUTORIAL_TEXT_DISTANCE_X,
	TUTORIAL_TEXT_DISTANCE_Y,
	PATH_FADEIN_TIME
} from '../config';

import { REGULAR_FONT } from '../../../constants/styles';

const text = 'TRACE THE\n' + 'VIBRANT PATH\n' + 'STARTING WITH\n' + 'SPARKLES';

const styles = StyleSheet.create({
	container: {
		position: 'absolute',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%'
	},
	text: {
		...REGULAR_FONT,
		textAlign: 'center',
		color: 'white'
	}
});

export class Renderer extends PureComponent {
	static propTypes = {
		arrow: PropTypes.object,
		position: PropTypes.object,
		ratio: PropTypes.number,
		index: PropTypes.number
	};

	static defaultProps = {
		arrow: {},
		position: {},
		ratio: 0,
		index: 0
	};

	constructor(props) {
		super(props);

		this.state = {
			window: Dimensions.get('window')
		};
	}

	render() {
		const { window } = this.state;
		const { arrow, position, ratio } = this.props;

		return (
			<Animatable.View
				style={styles.container}
				delay={PATH_FADEIN_TIME + 200}
				animation='fadeIn'
				key={this.props.index}
				useNativeDriver
			>
				<View>
					<Text
						style={[
							{
								fontSize: window.width / 18,
								fontWeight: 'bold',
								left: position.x + TUTORIAL_TEXT_DISTANCE_X * ratio,
								top: position.y + TUTORIAL_TEXT_DISTANCE_Y * ratio
							},
							styles.text
						]}
					>
						{text}
					</Text>
					<ProportionalImage
						ratio={ratio}
						style={{
							width: arrow.width * ratio * 0.7,
							height: arrow.height * ratio * 0.7,
							left: position.x + TUTORIAL_ARROW_DISTANCE_X * ratio,
							top: position.y + TUTORIAL_ARROW_DISTANCE_Y * ratio
						}}
						source={arrow.img}
					/>
				</View>
			</Animatable.View>
		);
	}
}

export default (arrow, position, ratio, index) => ({
	ratio,
	position,
	arrow,
	index,
	renderer: <Renderer />
});
