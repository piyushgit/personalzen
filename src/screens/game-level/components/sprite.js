import * as Animatable from 'react-native-animatable';
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';

import { SPRITE_FADEIN_TIME } from '../config';
import ProportionalImage from '../../../components/proportional-image';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
});

export class Renderer extends PureComponent {
  static propTypes = {
    position: PropTypes.object,
    sprite: PropTypes.object,
    ratio: PropTypes.number,
    index: PropTypes.number,
  };

  static defaultProps = {
    position: {},
    sprite: {},
    ratio: 0,
    index: 0,
  };

  render() {
    const { position, sprite, ratio } = this.props;
    const fadeIn = {
      0: {
        opacity: 0,
      },
      0.1: {
        opacity: 1,
      },
      0.9: {
        opacity: 1,
      },
      1: {
        opacity: 0,
      },
    };
    return (
      <Animatable.View
        style={styles.container}
        animation={fadeIn}
        duration={SPRITE_FADEIN_TIME}
        key={this.props.index}
        useNativeDriver
      >
        <ProportionalImage
          ratio={ratio}
          style={{
            width: sprite.width * ratio,
            height: sprite.height * ratio,
            left: position.x,
            top: position.y,
          }}
          source={sprite.img}
        />
      </Animatable.View>
    );
  }
}

export default (sprite, position, ratio, index) => (
  {
    ratio,
    position,
    sprite,
    index,
    renderer: <Renderer key={index} />,
  }
);
