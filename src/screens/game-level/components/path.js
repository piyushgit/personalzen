import * as Animatable from 'react-native-animatable';
import React, { PureComponent } from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import {
  PATH_FADEIN_TIME,
  PATH_FADEIN_FADEOUT_TIME,
  PATH_FADEOUT_TIME,
} from '../config';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
});

export class Renderer extends PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    paths: PropTypes.arrayOf(PropTypes.object),
    ratio: PropTypes.number,
    index: PropTypes.number,
  };

  static defaultProps = {
    data: [],
    paths: [],
    ratio: 0,
    index: 0,
  };

  render() {
    const fadeInfadeOut = {
      0: {
        opacity: 1,
      },
      0.5: {
        opacity: 0.3,
      },
      1: {
        opacity: 1,
      },
    };
    const { data, paths, ratio } = this.props;
    return (
      data
        .filter(item => !item.traced)
        .map((item, index) => (
          <Animatable.View
            style={styles.container}
            delay={PATH_FADEIN_TIME + item.delay}
            animation={item.traced ? 'fadeOut' : 'fadeIn'}
            ref={this.handleViewRef}
            key={`${this.props.index}path${index}`} // eslint-disable-line
            useNativeDriver
          >
            <Animatable.Image
              animation={item.traced ? 'fadeOut' : fadeInfadeOut}
              iterationCount={item.traced ? 1 : 'infinite'}
              duration={item.traced ? PATH_FADEOUT_TIME : PATH_FADEIN_FADEOUT_TIME}
              delay={PATH_FADEIN_TIME + (item.delay * 4)}
              ratio={ratio}
              useNativeDriver
              style={{
                width: paths[item.pathIndex].width * ratio,
                height: paths[item.pathIndex].height * ratio,
                left: item.x - ((paths[item.pathIndex].width * ratio) / 2),
                top: item.y - ((paths[item.pathIndex].height * ratio) / 2),
              }}
              source={paths[item.pathIndex].img}
            />
          </Animatable.View>
        ))
    );
  }
}

export default (data, paths, ratio, index) => (
  {
    data,
    ratio,
    paths,
    index,
    renderer: <Renderer />,
  }
);
