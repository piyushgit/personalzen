import React, { PureComponent } from 'react';
import {
  StyleSheet,
  Dimensions,
  Image,
  Text,
} from 'react-native';

import { REGULAR_FONT } from '../../../constants/styles';

const text = 'TRAVEL THROUGH EACH LAND\n' +
              'TO RESTORE ITS ORIGINAL\n' +
              'BEAUTY AND CALM.\n';

const styles = StyleSheet.create({
  text: {
    ...REGULAR_FONT,
    textAlign: 'center',
    color: 'white',
  },
});

export class Renderer extends PureComponent {
  static propTypes = {
    image: Image.propTypes.source.isRequired,
  };

  render() {
    const {
      image,
    } = this.props;
    const window = Dimensions.get('window');

    return [
      <Image
        style={{
            position: 'absolute',
            width: window.width,
            height: window.height / 2.5,
          }}
        source={image}
        resizeMode="stretch"
      />,
      <Text
        style={[
          {
          position: 'absolute',
          width: window.width,
          height: window.height / 2.5,
          fontSize: window.width / 18,
          marginTop: window.height / 20,
          },
          styles.text,
        ]}
      >
        {text}
      </Text>,
    ];
  }
}

export default image => (
  {
    image,
    renderer: <Renderer />,
  }
);
