import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import RNShakeEvent from 'react-native-shake-event';
import ExtraDimensions from 'react-native-extra-dimensions-android';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, Linking, Platform, Image } from 'react-native';
import PropTypes from 'prop-types';

const window = Dimensions.get('window');
const ORIGINAL_WIDTH = 749;
const BUTTOM_SIZE = 75;
const ratio = window.width / ORIGINAL_WIDTH;
import { gameMusicOn, gameMusicOff, soundEffectsOn, soundEffectsOff } from '../../../actions';

import { REGULAR_FONT } from '../../../constants/styles';
import ProportionalImage from '../../../components/proportional-image';
import { analytics } from '../../../utils/analytics';

const speakerIconOn = require('../../../../assets/images/options/sound-effects-on-icon.png');
const speakerIconOff = require('../../../../assets/images/options/sound-effects-off-icon.png');
const exitButtonImage = require('../../../../assets/images/tutorial/close-icon.png');
const musicNoteIconOn = require('../../../../assets/images/options/music-on-icon.png');
const musicNoteIconOff = require('../../../../assets/images/options/music-off-icon.png');

let { height } = window;
if (Platform.OS === 'android') {
	height = ExtraDimensions.get('REAL_WINDOW_HEIGHT');
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: 'white',
		opacity: 0.9,
		width: '100%',
		height,
		justifyContent: 'center',
		alignItems: 'center'
	},
	exitButton: {
		position: 'absolute',
		left: '85%',
		top: 15
	},
	text: {
		...REGULAR_FONT,
		fontSize: window.width / 17,
		letterSpacing: 3
	},
	textId: {
		...REGULAR_FONT,
		fontSize: window.width / 30,
		letterSpacing: 3
	},
	button: {
		padding: window.height / 25
	},
	containerIcons: {
		flexDirection: 'row',
		paddingVertical: window.height / 21,
		paddingHorizontal: window.width / 12
	},
	iconButton: {
		flex: 1,
		alignItems: 'center'
	},
	exit: {
		// fontSize: window.width / 12
		width: BUTTOM_SIZE * ratio,
		height: BUTTOM_SIZE * ratio
	}
});

class Options extends Component {
	static propTypes = {
		onClose: PropTypes.func,
		onWorldMapPress: PropTypes.func,
		onGlobePress: PropTypes.func,
		animation: PropTypes.string,
		openTerms: PropTypes.func.isRequired,
		openClinicalResearch: PropTypes.func.isRequired,
		user: PropTypes.object.isRequired,
		auth: PropTypes.object.isRequired,
		sounds: PropTypes.object.isRequired,
		gameMusicOn: PropTypes.object.isRequired,
		gameMusicOff: PropTypes.object.isRequired,
		soundEffectsOn: PropTypes.object.isRequired,
		soundEffectsOff: PropTypes.object.isRequired
	};

	static defaultProps = {
		onClose: null,
		onWorldMapPress: null,
		onGlobePress: null,
		animation: undefined
	};

	constructor(props) {
		super(props);

		this.state = {
			showUsername: false
		};
	}

	componentWillMount() {
		RNShakeEvent.addEventListener('shake', () => {
			this.showUsername();
		});
	}

	componentWillUnmount() {
		RNShakeEvent.removeEventListener('shake');
	}

	showUsername = () => {
		this.setState({
			showUsername: true
		});
	};

	handleOnPressAbout = () => {
		analytics.eventLog('Play Settings: About');
		const url = 'http://www.personalzen.com/';
		Linking.openURL(url);
	};

	handleOnPressFeedback = () => {
		analytics.eventLog('Play Settings: Feedback');
		const url = 'mailto:feedback@personalzen.com';
		Linking.openURL(url);
	};

	handleMusic = () => {
		const { playing } = this.props.sounds.GAMEPLAYMUSIC;
		if (playing) {
			this.props.gameMusicOff();
		} else {
			this.props.gameMusicOn();
		}
	};

	handleSoundEffects = () => {
		const { soundEffects } = this.props.sounds;

		if (!soundEffects) {
			this.props.soundEffectsOn();
		} else {
			this.props.soundEffectsOff();
		}
	};

	render() {
		const { onClose, onWorldMapPress, onGlobePress, animation, user, sounds } = this.props;
		const { isSuper } = this.props.auth;
		const { showUsername } = this.state;

		return (
			<Animatable.View animation={animation} style={styles.container} useNativeDriver>
				<View style={styles.container}>
					<TouchableOpacity style={styles.exitButton} onPress={onClose}>
						{/* <Text style={styles.exit} >X</Text> */}
						<Image style={styles.exit} source={exitButtonImage} />
					</TouchableOpacity>
					<TouchableOpacity style={styles.button} onPress={onGlobePress}>
						<Text style={styles.text}>GLOBE</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.button} onPress={onWorldMapPress}>
						<Text style={styles.text}>REGIONS</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={this.handleOnPressAbout} style={styles.button}>
						<Text style={styles.text}>ABOUT</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={this.handleOnPressFeedback} style={styles.button}>
						<Text style={styles.text}>FEEDBACK</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={this.props.openTerms} style={styles.button}>
						<Text style={styles.text}>TERMS</Text>
					</TouchableOpacity>
					{isSuper && (
						<TouchableOpacity onPress={this.props.openClinicalResearch} style={styles.button}>
							<Text style={styles.text}>CLINICAL RESEARCH</Text>
						</TouchableOpacity>
					)}
					{showUsername && (
						<View>
							<Text style={styles.textId}>{user.user.username}</Text>
						</View>
					)}
					<View style={styles.containerIcons}>
						<TouchableOpacity style={styles.iconButton} onPress={this.handleSoundEffects}>
							<ProportionalImage
								source={sounds.soundEffects ? speakerIconOn : speakerIconOff}
								ratio={0.5}
							/>
						</TouchableOpacity>
						<TouchableOpacity style={styles.iconButton} onPress={this.handleMusic}>
							<ProportionalImage
								source={sounds.gameMusic ? musicNoteIconOn : musicNoteIconOff}
								ratio={0.5}
							/>
						</TouchableOpacity>
					</View>
				</View>
			</Animatable.View>
		);
	}
}

const mapDispatchToProps = {
	gameMusicOn,
	gameMusicOff,
	soundEffectsOn,
	soundEffectsOff
};

function mapStateToProps(state){
	return {
		user: state.user,
		auth: state.auth,
		sounds: state.sounds
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Options);
