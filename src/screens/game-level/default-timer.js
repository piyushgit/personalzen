export default class DefaultTimer {
  constructor(interFrameInterval) {
    this.subscribers = [];
    this.loopId = null;
    this.interFrameInterval = interFrameInterval;
  }

  loop = (time) => {
    if (this.loopId) {
      this.subscribers.forEach((callback) => {
        callback(time);
      });
    }
    setTimeout(() => {
      this.loopId = requestAnimationFrame(this.loop);
    }, this.interFrameInterval);
  };

  start() {
    if (!this.loopId) {
      this.loop();
    }
  }

  stop() {
    if (this.loopId) {
      cancelAnimationFrame(this.loopId);
      this.loopId = null;
    }
  }

  subscribe(callback) {
    if (this.subscribers.indexOf(callback) === -1) {
      this.subscribers.push(callback);
    }
  }

  unsubscribe(callback) {
    this.subscribers = this.subscribers.filter(s => s !== callback);
  }
}
