import _ from 'lodash';

import Sprite from '../components/sprite';
import Path from '../components/path';
import Background from '../components/background';
import Sparkles from '../components/sparkles';
import TraceFeedback from '../components/trace-feedback';
import ArrowTutorialPath from '../components/arrow-tutorial-path';

import sparklesPosition from '../../../utils/sparkle-position';

import generateRounds from './round';

/**
 * calculates sparkles position from first two paths position
 *
 * @param {*} path
 * @param {*} region
 * @param {*} ratio
 * @returns
 */

export function changeRound(entities, index) {
  const {
    region,
    rounds,
    ratio,
    user,
  } = entities.scene;
  const { positive, negative } = region.gameLevel.sprites;
  const round = rounds[index];
  const { path, sprites } = round;

  // placebo Mode
  const invertedSprite = user.user.placeboMode ? _.random(0, 1) : 0;

  let saturation = 1;

  if (round.isTutorial) {
    return {
      ...entities,
      round,
      background: {
        ...entities.background,
        saturation,
      },
      scene: {
        ...entities.scene,
        currentRoundIndex: index,
      },
      path: Path(
        path,
        region.gameLevel.paths,
        ratio,
        index,
      ),
      positiveSprite: Sprite(
        positive,
        sprites.positive.position,
        ratio,
        index,
      ),
      negativeSprite: Sprite(
        negative,
        sprites.negative.position,
        ratio,
        index,
      ),
      traceFeedback: TraceFeedback(
        _.last(path),
        ratio,
        index,
      ),
      sparklesPositive: Sparkles(
        sparklesPosition(path, region, ratio),
        index,
        true,
      ),
      arrowTutorialPath: ArrowTutorialPath(
        region.gameLevel.tutorial.arrow,
        sprites.positive.position,
        ratio,
        index,
      ),
    };
  }

  saturation = index / (rounds.length - 1);

  return {
    ...entities,
    round,
    background: {
      ...entities.background,
      saturation,
    },
    scene: {
      ...entities.scene,
      currentRoundIndex: index,
    },
    path: Path(
      path,
      region.gameLevel.paths,
      ratio,
      index,
    ),
    positiveSprite: Sprite(
      invertedSprite ? negative : positive,
      sprites.positive.position,
      ratio,
      index,
    ),
    negativeSprite: Sprite(
      invertedSprite ? positive : negative,
      sprites.negative.position,
      ratio,
      index,
    ),
    traceFeedback: TraceFeedback(
      ratio,
      index,
    ),
    sparklesPositive: Sparkles(
      sparklesPosition(path, region, ratio),
      index,
      true,
    ),
  };
}

export default (region, window, ratio, isTutorial, user) => {
  const rounds = generateRounds(region, window, ratio, isTutorial, user);
  const { img, imgGrayscale } = region.gameLevel.background;
  return {
    background: Background(img, imgGrayscale),
    scene: {
      ratio,
      region,
      rounds,
      currentRoundIndex: 0,
      user,
    },
  };
};
