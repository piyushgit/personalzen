import _ from 'lodash';
import erfc from 'compute-erfc';

import getDistance from '../../../utils/distance';

import {
  CHAR_1_TOP,
  CHAR_1_BOTTOM,
  CHAR_2_TOP,
  CHAR_2_BOTTOM,
  VARIATION,
  STEP_VARIATION,
  MIN_X,
  MINIMUM_PATH_LENGTH,
  PATH_SIZE,
  PATH_ANGLE_ROTATION,
  DELAY_BETWEEN_PATHS,
  ROUND_PER_SCENE,
  TUTORIAL_ROUND_PER_SCENE,
} from '../config';

const generateInitialPosition = (region, window, ratio, isTutorial) => {
  const widthPositiveSprite = region.gameLevel.sprites.positive.width * ratio;
  const heightPositiveSprite = region.gameLevel.sprites.positive.height * ratio;

  const widthNegativeSprite = region.gameLevel.sprites.negative.width * ratio;
  const heightNegativeSprite = region.gameLevel.sprites.negative.height * ratio;

  const char1Top = CHAR_1_TOP * ratio;
  const char1Bottom = CHAR_1_BOTTOM * ratio;
  const char2Top = CHAR_2_TOP * ratio;
  const char2Bottom = CHAR_2_BOTTOM * ratio;

  const variation = _.floor(VARIATION * ratio);

  // Generate two beginning points
  const x1 = isTutorial ? 0 :
    _.random(0, window.width - widthPositiveSprite);

  const x2 = isTutorial ? window.width - widthPositiveSprite :
    _.random(0, window.width - widthPositiveSprite);

  const y1 = isTutorial ? char2Bottom :
    (((char1Top - char1Bottom) / 2) + (_.random(0, variation) - (variation / 2))) + char1Bottom;

  const y2 = isTutorial ? char1Bottom + heightNegativeSprite :
    (((char2Top - char2Bottom) / 2) + (_.random(0, variation) - (variation / 2))) + char2Bottom;

  let char1Pos = { x: x1, y: y1 };
  let char2Pos = { x: x2, y: y2 };

  const zoneSwap = isTutorial ? 1 : Math.round(_.random(0, 1, true));
  if (!zoneSwap) {
    const swp = char1Pos;
    char1Pos = char2Pos;
    char2Pos = swp;
  }

  return {
    positive: {
      position: char1Pos,
      size: {
        width: widthPositiveSprite,
        height: heightPositiveSprite,
      },
    },
    negative: {
      position: char2Pos,
      size: {
        width: widthNegativeSprite,
        height: heightNegativeSprite,
      },
    },
    zoneSwap,
  };
};

const generatePathPoints = (initialPosition, limits, window, ratio, isTutorial) => {
  const minX = MIN_X * ratio;
  const maxX = window.width - minX;
  const stepVariation = STEP_VARIATION * ratio;
  const minPathLength = MINIMUM_PATH_LENGTH * ratio;
  const dir = initialPosition.x < window.width / 2 ? 1 : -1;
  const path = {
    points: [],
    length: 0,
  };
  path.points.push(initialPosition);
  let lastPoint = initialPosition;

  while (true) {
    const i = _.size(path.points);
    let cdfAtX = 0.0;
    if (path.length > minPathLength) {
      cdfAtX = Math.round(0.5 * erfc(((4.0 - i) / Math.sqrt(5.0)) * Math.sqrt(0.5)));
    }
    // floorf(((double)arc4random() / 0x100000000))
    if (_.random(0, 1, true) < cdfAtX) {
      break;
    }

    // first, horizontal step
    const step = dir * (_.random(0, stepVariation) + stepVariation);
    let xTemp = lastPoint.x;
    let yTemp = isTutorial ? lastPoint.y :
      (Math.min(limits.y1, limits.y2) + _.random(0, Math.abs(limits.y1 - limits.y2)));
    xTemp += step;
    if (xTemp < minX) {
      xTemp = minX;
    } else if (xTemp > maxX) {
      xTemp = maxX;
    }

    // next, vertical step
    if (isTutorial) {
      yTemp += step;
    }

    path.points.push({
      x: xTemp,
      y: yTemp,
    });

    const nextPoint = {
      x: xTemp,
      y: yTemp,
    };

    const distance = getDistance(lastPoint, nextPoint);
    path.length += distance;

    // set state for next iteration
    lastPoint = nextPoint;
  }
  return path;
};

const generatePath = (sprites, window, ratio, isTutorial, region) => {
  const pathSize = isTutorial ? PATH_SIZE * 2 : PATH_SIZE;
  const initialPosition = {
    x: sprites.positive.position.x + (sprites.positive.size.width / 2),
    y: sprites.positive.position.y + (sprites.positive.size.height / 2),
  };

  const limits = {
    y1: 0,
    y2: 0,
  };

  if (sprites.zoneSwap) {
    limits.y1 = (CHAR_1_TOP * ratio);
    limits.y2 = (CHAR_1_BOTTOM * ratio) + sprites.positive.size.width;
  } else {
    limits.y1 = (CHAR_2_TOP * ratio);
    limits.y2 = (CHAR_2_BOTTOM * ratio) + sprites.positive.size.width;
  }

  const pathPoints = generatePathPoints(initialPosition, limits, window, ratio, isTutorial);
  const path = [];

  // pointAfter
  let i = 1;
  let delay = 0;
  while (i < _.size(pathPoints.points) && _.size(path) < pathSize) {
    const lastPoint = pathPoints.points[i - 1];
    const point = pathPoints.points[i];
    const length = getDistance(lastPoint, point);
    let pathIndex = _.random(0, _.size(region.gameLevel.paths) - 1);
    let minSpace = (region.gameLevel.paths[pathIndex].width * ratio) / 2;
    const rotate = _.random(0, 7) * PATH_ANGLE_ROTATION;

    let newPos = minSpace;
    while (newPos < length && (length - newPos) > (minSpace / 2)) {
      const xDist = (point.x - lastPoint.x);
      const yDist = (point.y - lastPoint.y);
      const pointAfter = {
        x: lastPoint.x + ((xDist * newPos) / length),
        y: lastPoint.y + ((yDist * newPos) / length),
      };
      delay += DELAY_BETWEEN_PATHS;
      path.push({
        x: pointAfter.x,
        y: pointAfter.y,
        pathIndex,
        rotate,
        delay,
        traced: false,
      });
      minSpace = (region.gameLevel.paths[pathIndex].width * ratio) / 2;
      newPos += minSpace;
      pathIndex = _.random(0, _.size(region.gameLevel.paths) - 1);
    }
    delay += DELAY_BETWEEN_PATHS;
    path.push({
      x: point.x,
      y: point.y,
      pathIndex,
      rotate,
      delay,
      traced: false,
    });
    i += 1;
  }

  return path;
};

export function generateRound(region, window, ratio, isTutorial) {
  const sprites = generateInitialPosition(region, window, ratio, isTutorial);
  const path = generatePath(sprites, window, ratio, isTutorial, region);

  return {
    sprites,
    path,
    isTutorial,
  };
}

export default (region, window, ratio, isTutorial) =>
  _.times(
    (isTutorial ? TUTORIAL_ROUND_PER_SCENE : ROUND_PER_SCENE),
    () => generateRound(region, window, ratio, isTutorial),
  );
