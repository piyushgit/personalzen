import _ from 'lodash';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  View,
  SafeAreaView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

import ProportionalImage from '../../components/proportional-image';

import { analytics } from '../../utils/analytics';
import BackButton from '../../components/back-button';
import RegionsLayout, {
  REGION_UNLOCK_TIME,
  REGION_01_KEY,
  REGION_02_KEY,
  REGION_03_KEY,
  REGION_04_KEY,
  REGION_05_KEY,
  REGION_LINKS,
} from '../../constants/regions';
import {
  updatePlayedTime,
  updateWeeklyPlayedTime,
  updateAchievementPlayedTime,
  updateAchievementRegions,
  updateAchievementRestored,
  updateAchievementGoals,
  updateAchievementDays,
  playSound,
  stopSound,
  updateScrollPosition,
} from '../../actions';

import PlaySound from '../../utils/playsound';

import Level from './entities/level';
import Systems from './systems';

import SceneRevitalized from './components/scene-revitalized';
import OptionsScreen from './components/options';
import scale from '../../utils/scale';

import GameEngine from './engine';
import DefaultTimer from './default-timer';
import { GAMEPLAYMUSIC } from '../../reducers/sounds';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  safeView: {
    position: 'absolute',
    width: '100%',
    alignItems: 'flex-end',
  },
  openOptions: {
    marginTop: scale(10),
    marginRight: scale(10),
  },
});

const ORIGINAL_WIDTH = 749;
const gear = require('../../../assets/images/options/gear.png');

class GameLevelScreen extends PureComponent {
  static propTypes = {
    navigation: PropTypes.shape().isRequired,
    updatePlayedTime: PropTypes.func.isRequired,
    updateWeeklyPlayedTime: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    regions: PropTypes.object.isRequired,
    updateAchievementPlayedTime: PropTypes.func.isRequired,
    updateAchievementRegions: PropTypes.func.isRequired,
    updateAchievementGoals: PropTypes.func.isRequired,
    weeklyGoal: PropTypes.object.isRequired,
    updateAchievementDays: PropTypes.func.isRequired,
    achievements: PropTypes.object.isRequired,
    playSound: PropTypes.object.isRequired,
    stopSound: PropTypes.object.isRequired,
    sounds: PropTypes.object.isRequired,
    updateScrollPosition: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      timer: new DefaultTimer(66),
      regions: _.clone(RegionsLayout),
      currentRegion: {},
      ratio: 0,
      window: {},
      sceneRevitalized: false,
      isTutorial: false,
      animation: 'fadeIn',
      delays: 0,
      showButtons: false,
    };
  }

  componentWillMount() {
    const { navigation, sounds } = this.props;
    const { regions } = this.state;
    const regionKey = navigation.getParam('regionKey');
    const isTutorial = navigation.getParam('isTutorial');
    const currentRegion = regions.find(region => region.key === regionKey);

    const window = Dimensions.get('window');
    const ratio = window.width / ORIGINAL_WIDTH;

    if (sounds.gameMusic) {
      this.props.playSound({ sound: GAMEPLAYMUSIC });
    }

    this.setState({
      currentRegion,
      isTutorial,
      ratio,
      window,
    });
  }

  componentDidMount() {
    // achievement play at least every other day
    const areaName = this.state.currentRegion.title.label;
    let areaNum = 'Area';
    if (areaName === 'Pacifica Bay') {
      areaNum = 'Area 1';
    } else if (areaName === 'Arboreal Point') {
      areaNum = 'Area 2';
    } else if (areaName === 'Panacea Plains') {
      areaNum = 'Area 3';
    } else if (areaName === 'Clarion Sands') {
      areaNum = 'Area 4';
    } else if (areaName === 'Zenith Heights') {
      areaNum = 'Area 5';
    }
    if (!this.state.isTutorial) {
      analytics.eventLog('App: Begin Play');
      analytics.eventLog(`Play Area: ${areaNum} ${areaName}`);
    }
    const today = moment().format();
    this.props.updateAchievementDays({ today });
  }

  onBackPress = () => {
    if (this.state.showOptions) {
      this.setState({ showOptions: false });
    } else {
      this.handleStopSound();
      this.props.navigation.navigate('WorldMap');
    }
    return true;
  }

  handleSceneRevitalized = (delays) => {
    const { currentRegion, window } = this.state;
    const { regions } = this.props;
    this.setState({ sceneRevitalized: true });
    const regionKey = currentRegion.key;
    this.props.updatePlayedTime({
      regionKey,
      playedTime: 1,
    });

    // achievement complete regions
    this.props.updateAchievementRegions({
      regionKey,
      unlockTime: REGION_UNLOCK_TIME[regionKey],
      totalTime: this.props.regions[regionKey].totalTime,
    });
    // achievement goal completed
    const { goal, weeklyPlayedTime } = this.props.weeklyGoal;
    this.props.updateAchievementGoals({ goal, weeklyPlayedTime });

    this.props.updateWeeklyPlayedTime();

    // achievement total time played
    this.props.updateAchievementPlayedTime({ regions: this.props.regions });

    // Navigate back to world map after scene revitalized
    const achievement = _.pickBy(this.props.achievements, item => item.isRecent);

    if (regions[regionKey].playedTime === REGION_UNLOCK_TIME[regionKey]) {
      switch (regionKey) {
        case REGION_01_KEY: {
          this.props.updateScrollPosition({
            scrollPosition: (window.width * 2307.33) / ORIGINAL_WIDTH,
          });
          break;
        }
        case REGION_02_KEY: {
          this.props.updateScrollPosition({
            scrollPosition: ((window.width * 1381.48) / ORIGINAL_WIDTH),
          });
          break;
        }
        case REGION_03_KEY: {
          this.props.updateScrollPosition({
            scrollPosition: ((window.width * 760) / ORIGINAL_WIDTH),
          });
          break;
        }
        case REGION_04_KEY: {
          this.props.updateScrollPosition({ scrollPosition: 0 });
          break;
        }
        default:
          break;
      }
    }
    if (regions[regionKey].playedTime === REGION_UNLOCK_TIME[regionKey] &&
      regionKey !== REGION_05_KEY) {
      setTimeout(() => {
        this.setState({ sceneRevitalized: false });
        this.handleStopSound();
        this.props.navigation.navigate('Collection', {
          regionKey: REGION_LINKS[currentRegion.key],
          delays,
        });
      }, 3000);
    } else if (!_.isEmpty(achievement)) {
      setTimeout(() => {
        this.setState({ sceneRevitalized: false });
        this.handleStopSound();
        this.props.navigation.navigate('Collection', {
          regionKey: currentRegion.key,
          delays,
        });
      }, 3000);
    } else if (this.props.weeklyGoal.weeklyPlayedTime === this.props.weeklyGoal.goal
    || regions[REGION_01_KEY].totalTime === 3) {
      setTimeout(() => {
        this.setState({ sceneRevitalized: false });
        this.handleStopSound();
        this.props.navigation.navigate('Results', {
          regionKey: currentRegion.key,
          delays,
        });
      }, 3000);
    } else if (regions[REGION_01_KEY].totalTime <= 2) {
      setTimeout(() => {
        this.setState({ sceneRevitalized: false });
        this.gameEngine.dispatch({ type: 'reset-scene' });
        this.setState({ sceneRevitalized: false });
      }, 3000);
    } else {
      this.setState({ showButtons: true });
    }
  }

  handleContinue = () => {
    this.gameEngine.dispatch({ type: 'reset-scene' });
    this.setState({ showButtons: false, sceneRevitalized: false });
  }
  handleSeeMyProgress = () => {
    const { currentRegion, delays } = this.state;
    this.setState({ sceneRevitalized: false });
    this.handleStopSound();
    this.props.navigation.navigate('Results', {
      regionKey: currentRegion.key,
      delays,
    });
  }

  handleEvent = (ev) => {
    const { isTutorial } = this.state;
    const { soundEffects } = this.props.sounds;
    switch (ev.type) {
      case 'scene-revitalized':
        if (isTutorial) {
          this.props.navigation.navigate('Tutorial', { step: 'three' });
        } else {
          this.setState({ delays: ev.delays });
          this.handleSceneRevitalized(ev.delays);
        }
        break;
      case 'play-sound':
        if (soundEffects) { PlaySound(ev.sound); }
        break;
      default:
        break;
    }
  };

  handleReturnToWorldMap = () => {
    analytics.eventLog('Play Settings: World Map');
    this.handleStopSound();
    this.props.navigation.navigate('WorldMap');
  }

  handleReturnToGlobe = () => {
    this.handleStopSound();
    this.props.navigation.navigate('MainMenu');
  }

  handleStopSound = () => {
    this.props.stopSound({ sound: GAMEPLAYMUSIC });
  }

  handleOpenOptions = () => {
    this.setState({ showOptions: true, animation: 'fadeIn' });
  }

  handleOptionsClose = () => {
    this.setState({ animation: 'fadeOut' });
    setTimeout(() => {
      this.setState({ showOptions: false });
    }, 1500);
  }

  handleOpenTerms = () => {
    analytics.eventLog('Play Settings: Terms');
    this.props.navigation.navigate('Terms');
  };

  openClinicalResearch = () => {
    this.props.navigation.navigate('ClinicalResearch');
  };

  render() {
    const { user } = this.props;
    const {
      timer,
      currentRegion,
      ratio,
      window,
      sceneRevitalized,
      isTutorial,
      showButtons,
    } = this.state;

    return (
      <View style={styles.container}>
        <BackButton onPress={this.onBackPress} />
        <GameEngine
          timer={timer}
          ref={(ref) => { this.gameEngine = ref; }}
          systems={Systems}
          entities={Level(currentRegion, window, ratio, isTutorial, user)}
          onEvent={this.handleEvent}
        >
          {sceneRevitalized && (
            <SceneRevitalized
              region={currentRegion}
              showButtons={showButtons}
              onPressContinue={this.handleContinue}
              onPressSeeMyProgress={this.handleSeeMyProgress}
            />
          )}
          {!isTutorial &&
            <SafeAreaView style={styles.safeView}>
              <TouchableOpacity style={styles.openOptions} onPress={this.handleOpenOptions}>
                <ProportionalImage source={gear} ratio={0.5} />
              </TouchableOpacity>
            </SafeAreaView>
          }
          {this.state.showOptions &&
            <OptionsScreen
              onClose={this.handleOptionsClose}
              onWorldMapPress={this.handleReturnToWorldMap}
              onGlobePress={this.handleReturnToGlobe}
              animation={this.state.animation}
              openTerms={this.handleOpenTerms}
              openClinicalResearch={this.openClinicalResearch}
            />
          }
        </GameEngine>
      </View>
    );
  }
}

const mapDispatchToProps = {
  updatePlayedTime,
  updateWeeklyPlayedTime,
  updateAchievementPlayedTime,
  updateAchievementRegions,
  updateAchievementRestored,
  updateAchievementGoals,
  updateAchievementDays,
  playSound,
  stopSound,
  updateScrollPosition,
};

function mapStateToProps(state) {
  return {
    nav: state.nav,
    user: state.user,
    regions: state.regions,
    weeklyGoal: state.weeklyGoal,
    achievements: state.achievements,
    sounds: state.sounds,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GameLevelScreen);
