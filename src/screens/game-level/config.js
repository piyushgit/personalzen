const imageSpeed1 = require('../../../assets/images/mandalas/mandala1.png');
const imageSpeed2 = require('../../../assets/images/mandalas/mandala2.png');

export const ROUND_PER_SCENE = 12;
export const TUTORIAL_ROUND_PER_SCENE = 3;

export const PATH_SIZE = 5;

export const PATH_ANGLE_ROTATION = 45;

// times
export const SPRITE_FADEIN_TIME = 500;
export const DELAY_BETWEEN_PATHS = 70;
export const PATH_FADEIN_TIME = 250;
export const ROUND_START_DELAY = SPRITE_FADEIN_TIME;
export const FEEDBACK_IMAGE_DURATION = 300;
export const INTER_TRIAL_INTERVAL = 1000;
export const PATH_FADEIN_FADEOUT_TIME = 3000;
export const PATH_FADEOUT_TIME = 200; // TODO: remove FADEOUT time
export const SCENE_DELAY = 1000;
export const TRACE_FEEDBACK_IMAGE_DURATION = 1500;

export const FEEDBACK_IMAGE_SIZE = 50;

export const SWIPE_SPEED1_THRESHOLD = 1;

export const SWIPE_SPEED1_IMAGE = imageSpeed1;
export const SWIPE_SPEED2_IMAGE = imageSpeed2;
export const CHAR_1_BOTTOM = 200;
export const CHAR_1_TOP = 650;
export const CHAR_2_BOTTOM = CHAR_1_TOP;
export const CHAR_2_TOP = CHAR_2_BOTTOM + 450;
export const VARIATION = 140;
export const STEP_VARIATION = 90;
export const MIN_X = 117;
export const MIN_SPACE = 80;
export const MINIMUM_PATH_LENGTH = 421;

export const TUTORIAL_ARROW_DISTANCE_X = 160;
export const TUTORIAL_ARROW_DISTANCE_Y = -210;

export const TUTORIAL_TEXT_DISTANCE_X = 130;
export const TUTORIAL_TEXT_DISTANCE_Y = 0;

export const K_MINIMUM_ACCURACY = 60;
