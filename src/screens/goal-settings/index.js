import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import ExtraDimensions from 'react-native-extra-dimensions-android';

import {
  View,
  Text,
  Image,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';

import scale from '../../utils/scale';
import { analytics } from '../../utils/analytics';
import { updateGoal, stopSound, resetWeeklyStart } from '../../actions';
import Path from './components/path';
import Circle from './components/circle';
import { goals } from '../../constants/goal-setting';
import { REGULAR_FONT } from '../../constants/styles';
import ProportionalImage from '../../components/proportional-image';
import { GAMEPLAYMUSIC } from '../../reducers/sounds';

const background = require('../../../assets/images/goal-setting/background.png');
const leftButton = require('../../../assets/images/tutorial/nav-buttom-left.png');
const rightButton = require('../../../assets/images/tutorial/nav-buttom-right.png');

const BACKGROUND_WIDTH = 750;
const window = Dimensions.get('window');
const ratio = window.width / BACKGROUND_WIDTH;

let { height } = window;
if (Platform.OS === 'android') {
  height = ExtraDimensions.get('REAL_WINDOW_HEIGHT');
}

const styles = StyleSheet.create({
  container: {
    height,
  },
  background: {
    position: 'absolute',
    height,
    width: window.width,
  },
  header: {
    position: 'absolute',
    alignItems: 'center',
    width: '100%',
    top: scale(10),
  },
  headerText: {
    ...REGULAR_FONT,
    color: 'white',
    fontSize: scale(21),
  },
  containerRecommendation: {
    alignItems: 'center',
    marginTop: scale(5),
  },
  recommendations: {
    ...REGULAR_FONT,
    color: 'white',
    fontSize: scale(11),
  },
  circleText: {
    ...REGULAR_FONT,
    color: 'white',
    fontSize: scale(12.5),
  },
  backButton: {
    position: 'absolute',
    bottom: scale(10),
    left: scale(10),
  },
  containerTutorialButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: scale(75),
  },
});

/**
 * Goal Setting Screen, on this screen the user selects a number of minutes he wants to play,
 * once user selects a number of minutes, that circle is highlighted in yellow
 * and highlight a path to the selected circle.
 *
 * @class SettingsScreen
 * @extends {Component}
 */
class SettingsScreen extends Component {
  static propTypes = {
    nav: PropTypes.shape().isRequired,
    updateGoal: PropTypes.func.isRequired,
    resetWeeklyStart: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
    goal: PropTypes.object,
    stopSound: PropTypes.func.isRequired,
  };

  static defaultProps = {
    goal: {},
  };

  constructor(props) {
    super(props);

    this.state = {
      isTutorial: this.props.navigation.getParam('isTutorial'),
      regionKey: this.props.navigation.getParam('regionKey'),
      isGoal: this.props.navigation.getParam('isGoal'),
      show: true,
    };
  }

  componentDidMount() {
    this.props.resetWeeklyStart();
  }

  componentDidUpdate() {
    const { index, routes } = this.props.nav;
    const { routeName } = routes[index];

    const show = routeName === 'Settings';

    if (show && !this.state.show) {
      this.setState({ show: true }); // eslint-disable-line
    }
  }

  handleCircleChecked = (index, array, minutes) => {
    const { isTutorial, regionKey, isGoal } = this.state;
    if (minutes === 10) {
      analytics.eventLog('Weekly Goal Selection: Mood Boost (10 min)');
    }
    if (minutes === 15) {
      analytics.eventLog('Weekly Goal Selection: Stress Relief (15 min)');
    }
    if (minutes === 20) {
      analytics.eventLog('Weekly Goal Selection: Positive Change (20 min)');
    }
    if (minutes === 25) {
      analytics.eventLog('Weekly Goal Selection: New Outlook (25 min)');
    }
    if (minutes === 30) {
      analytics.eventLog('Weekly Goal Selection: Peak Positivity (30 min)');
    }

    this.props.updateGoal({ minutes });

    this.props.stopSound({ sound: GAMEPLAYMUSIC });

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'MainMenu' })],
    });

    if (regionKey) {
      this.props.navigation.navigate('RegionIntro');
      this.props.navigation.navigate('GameLevel', {
        regionKey,
        isTutorial: false,
        isGoal: false,
      });
    } else {
      setTimeout(() => {
        if (isTutorial) {
          analytics.eventLog('Homepage: Complete Tutorial');
          this.props.navigation.dispatch(resetAction);
        } else if (isGoal) {
          // this.props.navigation.navigate('MainMenu');
          this.props.navigation.dispatch(resetAction);
          setTimeout(() => {
            this.setState({ show: false });
          }, 500);
        } else {
          this.props.navigation.navigate('MainMenu');
          setTimeout(() => {
            this.setState({ show: false });
          }, 1000);
        }
      }, 500);
    }
  };

  handleBackButton = () => {
    this.props.navigation.goBack();
  };

  handleRightButton = () => {
    const { isTutorial, regionKey, isGoal } = this.state;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'MainMenu' })],
    });
    if (regionKey) {
      this.props.navigation.navigate('RegionIntro');
      this.props.navigation.navigate('GameLevel', {
        regionKey,
        isTutorial: false,
        isGoal: false,
      });
    } else if (isTutorial) {
      analytics.eventLog('Homepage: Complete Tutorial');
      this.props.stopSound({ sound: GAMEPLAYMUSIC });
      this.props.navigation.dispatch(resetAction);
    } else if (isGoal) {
      // this.props.navigation.navigate('MainMenu');
      this.props.stopSound({ sound: GAMEPLAYMUSIC });
      this.props.navigation.dispatch(resetAction);
      setTimeout(() => {
        this.setState({ show: false });
      }, 500);
    } else {
      this.props.navigation.navigate('MainMenu');
      setTimeout(() => {
        this.setState({ show: false });
      }, 1500);
    }
    // else {
    //   this.props.navigation.navigate('StressTracking');
    //   setTimeout(() => {
    //     this.setState({ show: false });
    //   }, 1500);
    // }
  };

  render() {
    const { goal } = this.props.goal;
    if (!this.state.show) return null;
    return (
      <View style={styles.container}>
        <Image
          resizeMode="stretch"
          source={background}
          style={styles.background}
        />
        <SafeAreaView style={styles.header}>
          <Text style={styles.headerText}>SET YOUR WEEKLY GOAL</Text>
          <View style={styles.containerRecommendation}>
            <Text style={styles.recommendations}>
              Choose a weekly goal for optimal Zen
            </Text>
            <Text style={styles.recommendations}>
              We recommend 20 mins a week if you are just getting started
            </Text>
          </View>
        </SafeAreaView>
        {goals.map((item, index, array) => {
          const { circle, path, text, minutes } = item;
          const checked = minutes <= goal;
          return [
            <View style={{ position: 'absolute' }}>
              {path ? (
                <Path
                  source={path.image}
                  ratio={ratio}
                  style={{
                    top: window.height / path.top,
                    left: window.width / path.left,
                    zIndex: 1,
                  }}
                  show={checked}
                />
              ) : null}
            </View>,
            <Circle
              ratio={ratio}
              style={{
                top: window.height / circle.top,
                left: window.width / circle.left,
                zIndex: 2,
              }}
              number={`${minutes} min`}
              checked={checked}
              onPress={() => this.handleCircleChecked(index, array, minutes)}
            />,
            <View
              style={{
                position: text.position,
                top: window.height / text.top,
                left: window.width / text.left,
              }}
            >
              <Text style={styles.circleText}>{text.text}</Text>
            </View>,
          ];
        })}
        <View style={styles.backButton}>
          <SafeAreaView style={styles.containerTutorialButtons}>
            <TouchableOpacity onPress={this.handleBackButton}>
              <ProportionalImage source={leftButton} ratio={0.4} />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleRightButton}>
              <ProportionalImage source={rightButton} ratio={0.4} />
            </TouchableOpacity>
          </SafeAreaView>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = {
  updateGoal,
  stopSound,
  resetWeeklyStart,
};

function mapStateToProps(state) {
  return {
    goal: state.weeklyGoal,
    nav: state.nav,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsScreen);
