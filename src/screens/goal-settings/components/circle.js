import React, { Component } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import { REGULAR_FONT } from '../../../constants/styles';
import scale from '../../../utils/scale';

const styles = StyleSheet.create({
  view: {
    position: 'absolute',
  },
  circle: {
    width: scale(52),
    height: scale(52),
    backgroundColor: 'white',
    borderRadius: scale(26),
    borderWidth: scale(4),
  },
  number: {
    ...REGULAR_FONT,
    position: 'absolute',
    top: scale(18),
    left: scale(9),
    fontSize: scale(11),
  },
});
/**
 * Circle Component, renders a circle on the Goal Setting Screen and highligth
 * if the circle is selected
 *
 * @export
 * @class Path
 * @extends {Component}
 */

export default class Path extends Component {
  static propTypes = {
    style: PropTypes.object,
    number: PropTypes.number,
    onPress: PropTypes.func,
    checked: PropTypes.bool,
  };
  static defaultProps = {
    style: {},
    number: 10,
    onPress: null,
    checked: false,
  };

  render() {
    const {
      style,
      number,
      onPress,
      checked,
    } = this.props;

    let color = 'grey';

    if (checked) {
      color = '#ffd83c';
    }

    return (
      <TouchableWithoutFeedback style={[styles.view, style]} onPress={onPress}>
        <View style={[styles.view, style]}>
          <View style={[styles.circle, { borderColor: color }]} />
          <Text style={styles.number}>{number}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

