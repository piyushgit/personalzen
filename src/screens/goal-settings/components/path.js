import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';

const window = Dimensions.get('window');

const styles = StyleSheet.create({
  view: {
    position: 'absolute',
  },
});

/**
 * Path Component, if the circle selected is after the path will render the path.
 *
 * @export
 * @class Path
 * @extends {Component}
 */

export default class Path extends Component {
  static propTypes = {
    ratio: PropTypes.number,
    style: PropTypes.object,
    source: Image.propTypes.source.isRequired,
    show: PropTypes.bool,
  };
  static defaultProps = {
    ratio: 1,
    style: {},
    show: false,
  };
  render() {
    const {
      source,
      ratio,
      style,
      show,
    } = this.props;

    if (!show) return null;

    const asset = resolveAssetSource(source);
    const aspectRatio = (window.height / window.width) / (16 / 9);
    const height = (asset.height * ratio) * aspectRatio;
    const width = asset.width * ratio;

    return (
      <View style={[styles.view, style]}>
        <Image resizeMode="stretch" source={source} style={{ width, height }} />
      </View>
    );
  }
}
