import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import {
	reduxifyNavigator,
	createReactNavigationReduxMiddleware,
	createNavigationReducer
} from 'react-navigation-redux-helpers';

import { Animated, Easing } from 'react-native';

import EntranceScreen from './screens/entrance';
import MainMenuScreen from './screens/menu';
import TutorialScreen from './screens/tutorial';
import WorldMapScreen from './screens/world-map';
import RegionIntroScreen from './screens/region-intro';
import GameLevelScreen from './screens/game-level';
import ClinicalResearchScreen from './screens/clinical-research';
import StressTrackingScreen from './screens/stress-tracking';
import SettingsScreen from './screens/goal-settings';
import GameResultScreen from './screens/results/result-screen';
import TermsScreen from './screens/terms';
import TipsAndQuotesScreen from './screens/tips-quotes';
import CollectionScreen from './screens/collection';
import Subscription from './screens/subscription/subscription';
import SignUp from './screens/subscription-signup/signup';
import Signin from './screens/subscription-signin/signin';

const transitionConfig = () => ({
	transitionSpec: {
		duration: 750,
		easing: Easing.in(Easing.poly(4)),
		timing: Animated.timing,
		useNativeDriver: true
	},
	screenInterpolator: (sceneProps) => {
		const { position, scene } = sceneProps;

		const thisSceneIndex = scene.index;

		const opacity = position.interpolate({
			inputRange: [ thisSceneIndex - 1, thisSceneIndex ],
			outputRange: [ 0, 1 ]
		});

		return { opacity };
	}
});

const RootNavigator = createStackNavigator(
	{
		Entrance: EntranceScreen,
		Subscription: Subscription,
		SignUp: SignUp,
		Signin: Signin,
		MainMenu: MainMenuScreen,
		Tutorial: TutorialScreen,
		WorldMap: WorldMapScreen,
		RegionIntro: RegionIntroScreen,
		GameLevel: GameLevelScreen,
		ClinicalResearch: ClinicalResearchScreen,
		Settings: SettingsScreen,
		StressTracking: StressTrackingScreen,
		Results: GameResultScreen,
		Terms: TermsScreen,
		TipsAndQuotes: TipsAndQuotesScreen,
		Collection: CollectionScreen
	},
	{
		initialRouteName: 'Entrance',
		headerMode: 'none',
		mode: 'card',
		// cardStyle: {
		// 	backgroundColor: 'transparent'
		// },
		navigationOptions: {
			gesturesEnabled: false
		},
		transitionConfig
	}
);

const reducer = createNavigationReducer(RootNavigator);

const middleware = createReactNavigationReduxMiddleware('root', (state) => state.nav);

const AppWithNavigationState = reduxifyNavigator(RootNavigator, 'root');

const mapStateToProps = (state) => ({
	state: state.nav
});

const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

export { RootNavigator, AppNavigator, middleware, reducer };
