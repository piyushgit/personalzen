import { FETCH_USER_COMPLETED } from '../actions/index';

export const initialState = {
  user: {
    placeboMode: false,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_COMPLETED: {
      return {
        ...state,
        user: action.user,
      };
    }
    default:
      return state;
  }
};
