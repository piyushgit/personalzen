import _ from 'lodash';
import moment from 'moment';
import { analytics } from '../utils/analytics';
import {
  ACHIEVEMENT_01_KEY,
  ACHIEVEMENT_02_KEY,
  ACHIEVEMENT_03_KEY,
  ACHIEVEMENT_04_KEY,
  ACHIEVEMENT_05_KEY,
  ACHIEVEMENT_06_KEY,
  ACHIEVEMENT_07_KEY,
  ACHIEVEMENT_08_KEY,
  ACHIEVEMENT_09_KEY,
  ACHIEVEMENT_10_KEY,
  ACHIEVEMENT_11_KEY,
  ACHIEVEMENT_12_KEY,
  ACHIEVEMENT_13_KEY,
  ACHIEVEMENT_14_KEY,
  ACHIEVEMENT_15_KEY,
  ACHIEVEMENT_16_KEY,
  ACHIEVEMENT_17_KEY,
  ACHIEVEMENT_18_KEY,
  ACHIEVEMENT_19_KEY,
  ACHIEVEMENT_20_KEY,
} from '../screens/collection/config';
import {
  UPDATE_ACHIEVEMENT_IS_RECENT,
  UPDATE_ACHIEVEMENT_SCHOLAR,
  UPDATE_ACHIEVEMENT_PLAYED_TIME,
  UPDATE_ACHIEVEMENT_REGIONS,
  UPDATE_ACHIEVEMENT_STRESSLEVEL,
  UPDATE_ACHIEVEMENT_DAYS,
  UPDATE_ACHIEVEMENT_GOALS,
} from '../actions/index.names';
import { REGION_ACHIEVEMENT_KEY, REGION_05_KEY } from '../constants/regions';

export const initialState = {
  [ACHIEVEMENT_01_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_02_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_03_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_04_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_05_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_06_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_07_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_08_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_09_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_10_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_11_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_12_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_13_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_14_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_15_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_16_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_17_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_18_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_19_KEY]: {
    achieved: false,
    isRecent: false,
  },
  [ACHIEVEMENT_20_KEY]: {
    achieved: false,
    isRecent: false,
  },
  days: {
    initialDate: null,
    lastDate: null,
  },
};

export default (state = initialState, action) => {
  const achievementRecent = {
    achieved: true,
    isRecent: true,
  };
  switch (action.type) {
    case UPDATE_ACHIEVEMENT_IS_RECENT: {
      const { achievementKey } = action.payload;
      const achievement = {
        achieved: true,
        isRecent: false,
      };
      return {
        ...state,
        [achievementKey]: achievement,
      };
    }
    case UPDATE_ACHIEVEMENT_GOALS: {
      const { goal, weeklyPlayedTime } = action.payload;
      if (goal === 10 && goal === weeklyPlayedTime && ![ACHIEVEMENT_01_KEY].achieved) {
        return {
          ...state,
          [ACHIEVEMENT_01_KEY]: achievementRecent,
        };
      }
      if (goal === 15 && goal === weeklyPlayedTime && ![ACHIEVEMENT_02_KEY].achieved) {
        return {
          ...state,
          [ACHIEVEMENT_02_KEY]: achievementRecent,
        };
      }
      if (goal === 20 && goal === weeklyPlayedTime && ![ACHIEVEMENT_03_KEY].achieved) {
        return {
          ...state,
          [ACHIEVEMENT_03_KEY]: achievementRecent,
        };
      }
      if (goal === 25 && goal === weeklyPlayedTime && ![ACHIEVEMENT_04_KEY].achieved) {
        return {
          ...state,
          [ACHIEVEMENT_04_KEY]: achievementRecent,
        };
      }
      if (goal === 30 && goal === weeklyPlayedTime && ![ACHIEVEMENT_05_KEY].achieved) {
        return {
          ...state,
          [ACHIEVEMENT_05_KEY]: achievementRecent,
        };
      }
      return {
        ...state,
      };
    }
    case UPDATE_ACHIEVEMENT_SCHOLAR: {
      return {
        ...state,
        [ACHIEVEMENT_18_KEY]: achievementRecent,
      };
    }
    case UPDATE_ACHIEVEMENT_PLAYED_TIME: {
      const { regions } = action.payload;
      let allRegionsTime = 0;
      _.mapValues(regions, (item) => {
        if (item.totalTime) {
          allRegionsTime += item.totalTime;
        }
      });
      if (allRegionsTime === 50) {
        analytics.eventLog('Achievement: Student (Play PZ for 50 min)');
        return {
          ...state,
          [ACHIEVEMENT_11_KEY]: achievementRecent,
        };
      }
      if (allRegionsTime === 100) {
        analytics.eventLog('Achievement: Teacher (Play PZ for 100 min)');
        return {
          ...state,
          [ACHIEVEMENT_12_KEY]: achievementRecent,
        };
      }
      if (allRegionsTime === 200) {
        analytics.eventLog('Achievement: Professor (Play PZ for 200 min)');
        return {
          ...state,
          [ACHIEVEMENT_13_KEY]: achievementRecent,
        };
      }
      return {
        ...state,
      };
    }
    case UPDATE_ACHIEVEMENT_REGIONS: {
      const {
        regionKey,
        unlockTime,
        totalTime,
      } = action.payload;
      if (unlockTime === totalTime) {
        if (regionKey === REGION_05_KEY) {
          return {
            ...state,
            [ACHIEVEMENT_20_KEY]: achievementRecent,
            [REGION_ACHIEVEMENT_KEY[regionKey]]: achievementRecent,
          };
        }
        return {
          ...state,
          [REGION_ACHIEVEMENT_KEY[regionKey]]: achievementRecent,
        };
      }
      return {
        ...state,
      };
    }
    case UPDATE_ACHIEVEMENT_STRESSLEVEL: {
      analytics.eventLog('Feeling Good (Lower your stress level)');
      return {
        ...state,
        [ACHIEVEMENT_19_KEY]: achievementRecent,
      };
    }
    case UPDATE_ACHIEVEMENT_DAYS: {
      const { today } = action.payload;
      const reset = {
        initialDate: today,
        lastDate: today,
      };
      const updateLastDay = {
        initialDate: state.days.initialDate,
        lastDate: today,
      };
      const playedTime = moment(today).diff(state.days.initialDate, 'days');
      if (!state.days.initialDate || moment(today).diff(state.days.lastDate, 'days') > 2) {
        return {
          ...state,
          days: reset,
        };
      }
      if (playedTime >= 7 && !state[ACHIEVEMENT_14_KEY].achieved) {
        analytics.eventLog('Achievement: On a Roll (Play PZ every other day for a week)');
        return {
          ...state,
          days: updateLastDay,
          [ACHIEVEMENT_14_KEY]: achievementRecent,
        };
      }
      if (playedTime >= 14 && !state[ACHIEVEMENT_15_KEY].achieved) {
        analytics.eventLog('Achievement: Making a Habit (Play PZ every other day for 2 weeks)');
        return {
          ...state,
          days: updateLastDay,
          [ACHIEVEMENT_15_KEY]: achievementRecent,
        };
      }
      if (playedTime >= 30 && !state[ACHIEVEMENT_16_KEY].achieved) {
        analytics.eventLog('Achievement: Made a Habit (Play PZ every other day for a month)');
        return {
          ...state,
          days: updateLastDay,
          [ACHIEVEMENT_16_KEY]: achievementRecent,
        };
      }
      if (playedTime >= 60 && !state[ACHIEVEMENT_17_KEY].achieved) {
        analytics.eventLog('Achievement: Made a Habit (Play PZ every other day for 2 months)');
        return {
          ...state,
          days: updateLastDay,
          [ACHIEVEMENT_17_KEY]: achievementRecent,
        };
      }
      return {
        ...state,
        days: updateLastDay,
      };
    }
    default:
      return state;
  }
};

