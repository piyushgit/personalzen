import _ from 'lodash';
import moment from 'moment';

import {
  ACHIEVEMENT_01_KEY,
  ACHIEVEMENT_02_KEY,
  ACHIEVEMENT_03_KEY,
  ACHIEVEMENT_04_KEY,
  ACHIEVEMENT_05_KEY,
  ACHIEVEMENT_11_KEY,
  ACHIEVEMENT_12_KEY,
  ACHIEVEMENT_13_KEY,
  ACHIEVEMENT_14_KEY,
  ACHIEVEMENT_16_KEY,
  ACHIEVEMENT_15_KEY,
  ACHIEVEMENT_17_KEY,
  ACHIEVEMENT_18_KEY,
  ACHIEVEMENT_20_KEY,
} from '../screens/collection/config';

import achievements, { initialState } from './achievements';
import { initialState as initialStateRegions } from './regions';
import {
  REGION_01_KEY,
  REGION_02_KEY,
  REGION_03_KEY,
  REGION_04_KEY,
  REGION_05_KEY,
  REGION_ACHIEVEMENT_KEY,
  REGION_UNLOCK_TIME,
} from '../constants/regions';

test('should sets initial state', () => {
  const newState = achievements(undefined, {});
  expect(newState).toHaveProperty(ACHIEVEMENT_01_KEY);
});

function testAchievementGoalComplete(achievementKey, goal, weeklyPlayedTime) {
  const action = { type: 'UPDATE_ACHIEVEMENT_GOALS', payload: { goal, weeklyPlayedTime } };
  const state = _.cloneDeep(initialState);
  const newState = achievements(state, action);
  expect(newState[achievementKey]).toHaveProperty('achieved');
  expect(newState[achievementKey].achieved).toEqual(true);
  expect(newState[achievementKey]).toHaveProperty('isRecent');
  expect(newState[achievementKey].isRecent).toEqual(true);
}

test('should unlock goals complete achievement', () => {
  testAchievementGoalComplete(ACHIEVEMENT_01_KEY, 10, 10);
  testAchievementGoalComplete(ACHIEVEMENT_02_KEY, 15, 15);
  testAchievementGoalComplete(ACHIEVEMENT_03_KEY, 20, 20);
  testAchievementGoalComplete(ACHIEVEMENT_04_KEY, 25, 25);
  testAchievementGoalComplete(ACHIEVEMENT_05_KEY, 30, 30);
});

function testAchievementTotalPlayedTime(time, achievementKey) {
  const regionsState = _.cloneDeep(initialStateRegions);
  _.mapValues(regionsState, (item) => {
    if (item.totalTime || item.totalTime === 0) {
      item.totalTime = time;
    }
  });
  const action = { type: 'UPDATE_ACHIEVEMENT_PLAYED_TIME', payload: { regions: regionsState } };
  const state = _.cloneDeep(initialState);
  const newState = achievements(state, action);
  expect(newState[achievementKey]).toHaveProperty('achieved');
  expect(newState[achievementKey].achieved).toEqual(true);
  expect(newState[achievementKey]).toHaveProperty('isRecent');
  expect(newState[achievementKey].isRecent).toEqual(true);
}

test('should unlock total played time achievement', () => {
  testAchievementTotalPlayedTime(10, ACHIEVEMENT_11_KEY);
  testAchievementTotalPlayedTime(20, ACHIEVEMENT_12_KEY);
  testAchievementTotalPlayedTime(40, ACHIEVEMENT_13_KEY);
});

function testAchievementRegionComplete(regionKey, totalTime) {
  const unlockTime = REGION_UNLOCK_TIME[regionKey];
  const action = { type: 'UPDATE_ACHIEVEMENT_REGIONS', payload: { regionKey, unlockTime, totalTime } };
  const state = _.cloneDeep(initialState);
  const newState = achievements(state, action);
  expect(newState[REGION_ACHIEVEMENT_KEY[regionKey]]).toHaveProperty('achieved');
  expect(newState[REGION_ACHIEVEMENT_KEY[regionKey]].achieved).toEqual(true);
  expect(newState[REGION_ACHIEVEMENT_KEY[regionKey]]).toHaveProperty('isRecent');
  expect(newState[REGION_ACHIEVEMENT_KEY[regionKey]].isRecent).toEqual(true);
  if (regionKey === REGION_05_KEY) {
    expect(newState[ACHIEVEMENT_20_KEY]).toHaveProperty('achieved');
    expect(newState[ACHIEVEMENT_20_KEY].achieved).toEqual(true);
    expect(newState[ACHIEVEMENT_20_KEY]).toHaveProperty('isRecent');
    expect(newState[ACHIEVEMENT_20_KEY].isRecent).toEqual(true);
  }
}

test('should unlock region complete achievement', () => {
  testAchievementRegionComplete(REGION_01_KEY, 10);
  testAchievementRegionComplete(REGION_02_KEY, 15);
  testAchievementRegionComplete(REGION_03_KEY, 20);
  testAchievementRegionComplete(REGION_04_KEY, 25);
  testAchievementRegionComplete(REGION_05_KEY, 30);
});

test('should unlock scholar achievement', () => {
  const action = { type: 'UPDATE_ACHIEVEMENT_SCHOLAR' };
  const state = _.cloneDeep(initialState);
  const newState = achievements(state, action);
  expect(newState[ACHIEVEMENT_18_KEY]).toHaveProperty('achieved');
  expect(newState[ACHIEVEMENT_18_KEY].achieved).toEqual(true);
  expect(newState[ACHIEVEMENT_18_KEY]).toHaveProperty('isRecent');
  expect(newState[ACHIEVEMENT_18_KEY].isRecent).toEqual(true);
});

function testUpdateAchievementsDays(days, achievementKey) {
  const today = moment().format();
  const action = { type: 'UPDATE_ACHIEVEMENT_DAYS', payload: { today } };
  const state = _.cloneDeep(initialState);
  if (achievementKey === ACHIEVEMENT_15_KEY) {
    state[ACHIEVEMENT_14_KEY].achieved = true;
  }
  if (achievementKey === ACHIEVEMENT_16_KEY) {
    state[ACHIEVEMENT_14_KEY].achieved = true;
    state[ACHIEVEMENT_15_KEY].achieved = true;
  }
  if (achievementKey === ACHIEVEMENT_17_KEY) {
    state[ACHIEVEMENT_14_KEY].achieved = true;
    state[ACHIEVEMENT_15_KEY].achieved = true;
    state[ACHIEVEMENT_16_KEY].achieved = true;
  }
  const day = state.days;
  day.initialDate = moment(today).subtract(days, 'd').format();
  const newState = achievements(state, action);
  expect(newState[achievementKey]).toHaveProperty('achieved');
  expect(newState[achievementKey].achieved).toEqual(true);
  expect(newState[achievementKey]).toHaveProperty('isRecent');
  expect(newState[achievementKey].isRecent).toEqual(true);
}

function testAddDays(days) {
  const dayOne = moment().format();
  const actionOne = { type: 'UPDATE_ACHIEVEMENT_DAYS', payload: { today: dayOne } };
  const state = _.cloneDeep(initialState);
  const newState = achievements(state, actionOne);
  expect(newState.days).toHaveProperty('initialDate');
  expect(newState.days.initialDate).toEqual(dayOne);
  expect(newState.days).toHaveProperty('lastDate');
  expect(newState.days.lastDate).toEqual(dayOne);

  const dayTwo = moment(dayOne).add(days, 'd').format();
  const actionTwo = { type: 'UPDATE_ACHIEVEMENT_DAYS', payload: { today: dayTwo } };
  const secondState = achievements(newState, actionTwo);
  expect(secondState.days).toHaveProperty('initialDate');
  expect(secondState.days.initialDate).toEqual(dayOne);
  expect(secondState.days).toHaveProperty('lastDate');
  expect(secondState.days.lastDate).toEqual(dayTwo);
}

test('should update Play at least every other day achievement', () => {
  testUpdateAchievementsDays(7, ACHIEVEMENT_14_KEY);
  testUpdateAchievementsDays(14, ACHIEVEMENT_15_KEY);
  testUpdateAchievementsDays(30, ACHIEVEMENT_16_KEY);
  testUpdateAchievementsDays(60, ACHIEVEMENT_17_KEY);
  testAddDays(1);
  testAddDays(2);
});
