import {
  AUTHENTICATE_COMPLETED,
  AUTHENTICATE_FAILURE,
} from '../actions/index';

export const initialState = {
  token: null,
  isSuper: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE_COMPLETED: {
      return {
        ...state,
        token: action.payload.token,
        isSuper: action.payload.isSuper,
      };
    }
    case AUTHENTICATE_FAILURE: {
      return {
        ...state,
        token: null,
        isSuper: false,
        error: action.error,
      };
    }
    default:
      return state;
  }
};
