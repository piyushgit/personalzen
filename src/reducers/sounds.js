import {
	PLAY_SOUND,
	STOP_SOUND,
	RESET_SOUNDS,
	SOUND_EFFECTS_ON,
	SOUND_EFFECTS_OFF,
	GAME_MUSIC_ON,
	GAME_MUSIC_OFF
} from '../actions/index.names';

export const GAMEPLAYMUSIC = 'GAMEPLAYMUSIC';
export const INTHENOW = 'INTHENOW';

export const initialState = {
	[GAMEPLAYMUSIC]: {
		playing: false
	},
	[INTHENOW]: {
		playing: false
	},
	gameMusic: true,
	soundEffects: true
};

export default (state = initialState, action) => {
	switch (action.type) {
		case PLAY_SOUND: {
			const { sound } = action.payload;
			const playing = { playing: true };
			return {
				...state,
				[sound]: playing
			};
		}
		case STOP_SOUND: {
			const { sound } = action.payload;
			const playing = { playing: false };
			return {
				...state,
				[sound]: playing
			};
		}
		case RESET_SOUNDS: {
			return {
				...initialState
			};
		}
		case SOUND_EFFECTS_ON: {
			return {
				...state,
				soundEffects: true
			};
		}
		case SOUND_EFFECTS_OFF: {
			return {
				...state,
				soundEffects: false
			};
		}
		case GAME_MUSIC_ON: {
			const playing = { playing: true };
			return {
				...state,
				gameMusic: true,
				[GAMEPLAYMUSIC]: playing
			};
		}
		case GAME_MUSIC_OFF: {
			const playing = { playing: false };
			return {
				...state,
				gameMusic: false,
				[GAMEPLAYMUSIC]: playing
			};
		}
		default:
			return state;
	}
};
