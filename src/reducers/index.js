import * as Routes from '../routes';
import regions from './regions';
import settings from './settings';
import weeklyGoal from './weekly-goal';
import user from './user';
import auth from './auth';
import achievements from './achievements';
import sounds from './sounds';

const rehydrated = (state = false, action) => {
  switch (action.type) {
    case 'persist/REHYDRATE':
      return true;
    default:
      return state;
  }
};

export default ({
  rehydrated,
  nav: Routes.reducer,
  regions,
  user,
  settings,
  weeklyGoal,
  auth,
  achievements,
  sounds,
});

