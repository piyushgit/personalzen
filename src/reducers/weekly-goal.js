import moment from 'moment';
import {
  UPDATE_WEEKLY_PLAYED_TIME,
  UPDATE_GOAL,
  RESET_WEEKLY_START,
} from '../actions/index.names';

const initialState = {
  weeklyPlayedTime: 0,
  nextWeek: null,
  goal: 20,
};

const weekReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_WEEKLY_PLAYED_TIME: {
      let { nextWeek, weeklyPlayedTime } = state;
      const today = moment().startOf('day');
      if (!nextWeek) {
        nextWeek = moment().startOf('day').add(7, 'days').format();
      }
      if (today.isSameOrAfter(nextWeek)) {
        weeklyPlayedTime = 0;
        nextWeek = today.add(7, 'days').format();
      }
      weeklyPlayedTime += 1;

      return {
        ...state,
        weeklyPlayedTime,
        nextWeek,
      };
    }

    case UPDATE_GOAL: {
      const { minutes } = action.payload;
      return {
        ...state,
        goal: minutes,
      };
    }

    // Resets the weekly start and the plyed time
    // once the user has reached the goal.
    case RESET_WEEKLY_START: {
      const { goal } = state;
      let { nextWeek, weeklyPlayedTime } = state;

      if (weeklyPlayedTime >= goal) {
        nextWeek = moment().startOf('day').add(7, 'days').format();
        weeklyPlayedTime = 0;
      }

      return {
        ...state,
        weeklyPlayedTime,
        nextWeek,
      };
    }

    default:
      return state;
  }
};

export default weekReducer;
