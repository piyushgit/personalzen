import _ from 'lodash';
import moment from 'moment';

import {
  REGION_01_KEY,
  REGION_02_KEY,
  REGION_03_KEY,
  REGION_04_KEY,
  REGION_05_KEY,
  REGION_UNLOCK_TIME,
  REGION_LINKS,
} from '../constants/regions';
import regions, { initialState, updateSaturation } from './regions';

test('should sets initial state', () => {
  const newState = regions(undefined, {});
  expect(newState).toHaveProperty(REGION_01_KEY);
});

function testUnlockRegion(regionKey, playedTime) {
  const action = { type: 'UPDATE_PLAYED_TIME', payload: { regionKey, playedTime } };
  const state = _.cloneDeep(initialState);
  const newState = regions(state, action);
  expect(newState[REGION_LINKS[regionKey]]).toHaveProperty('state');
  expect(newState[REGION_LINKS[regionKey]].state).toEqual('unlocked');
}

test('should unlock region', () => {
  testUnlockRegion(REGION_01_KEY, 10);
  testUnlockRegion(REGION_02_KEY, 15);
  testUnlockRegion(REGION_03_KEY, 20);
  testUnlockRegion(REGION_04_KEY, 25);
});

function testUpdatePlayedTime(key) {
  const action = { type: 'UPDATE_PLAYED_TIME', payload: { regionKey: key, playedTime: 1 } };
  const state = _.cloneDeep(initialState);
  const region = state[key];
  region.playedTime = 0;
  region.saturation = 0;
  let i;
  for (i = 0; i <= REGION_UNLOCK_TIME[key]; i += 1) {
    const newState = regions(state, action);
    expect(newState).toHaveProperty(key); // 1
    expect(newState[key]).toHaveProperty('playedTime');
    expect(newState[key].playedTime).toEqual(i + 1);
  }
}

test('should update playedtime', () => {
  testUpdatePlayedTime(REGION_01_KEY);
  testUpdatePlayedTime(REGION_02_KEY);
  testUpdatePlayedTime(REGION_03_KEY);
  testUpdatePlayedTime(REGION_04_KEY);
  testUpdatePlayedTime(REGION_05_KEY);
});

function testRegionsSaturation(regionKey, time) {
  const state = _.cloneDeep(initialState);
  const action = { type: 'UPDATE_SATURATION', payload: { regions: state } };
  const region = state[regionKey];
  region.playedTime = time;
  region.lastDayPlayed = moment().format();
  let i;
  for (i = 0; i <= 10; i += 1) {
    const newState = regions(state, action);
    expect(newState).toHaveProperty(regionKey); // 1
    expect(newState[regionKey]).toHaveProperty('saturation');
    expect(newState[regionKey].saturation).toEqual(10 - i);
    region.lastDayPlayed = moment(state[regionKey].lastDayPlayed, moment.ISO_8601).subtract(21, 'days').format();
    region.playedTime = time;
    region.saturationChange = null;
  }
}
test('should update saturation based on played time', () => {
  testRegionsSaturation(REGION_01_KEY, 10);
  testRegionsSaturation(REGION_02_KEY, 15);
  testRegionsSaturation(REGION_03_KEY, 20);
  testRegionsSaturation(REGION_04_KEY, 25);
  testRegionsSaturation(REGION_05_KEY, 30);
});


function testRegionsSaturationChange(regionKey) {
  const state = _.cloneDeep(initialState);
  const region = state[regionKey];
  const today = moment();
  region.playedTime = REGION_UNLOCK_TIME[regionKey];
  state[regionKey].playedTime = REGION_UNLOCK_TIME[regionKey];
  const lastDayPlayed = moment(today).subtract(210, 'days').format();
  region.lastDayPlayed = lastDayPlayed;
  let i;
  for (i = 0; i < 10; i += 1) {
    const newState = updateSaturation(region, regionKey, moment(lastDayPlayed).add(21 * i, 'days').format());
    expect(newState).toHaveProperty('saturation');
    expect(newState.saturation).toEqual(10 - i);
  }
}

test('should update saturation based on "change saturation"', () => {
  testRegionsSaturationChange(REGION_01_KEY);
  testRegionsSaturationChange(REGION_02_KEY);
  testRegionsSaturationChange(REGION_03_KEY);
  testRegionsSaturationChange(REGION_04_KEY);
  testRegionsSaturationChange(REGION_05_KEY);
});
