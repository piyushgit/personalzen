import moment from 'moment';

import { UPDATE_STRESS_LEVEL, UPDATE_LAST_ACCESS } from '../actions/index.names';

export const initialState = {
  stressLevel: 3,
  lastAccess: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_STRESS_LEVEL: {
      const { stressLevel } = action.payload;
      return {
        ...state,
        stressLevel,
      };
    }
    case UPDATE_LAST_ACCESS: {
      const today = moment();
      return {
        ...state,
        lastAccess: moment(today, moment.ISO_8601).startOf('day').format(),
      };
    }
    default:
      return state;
  }
};

