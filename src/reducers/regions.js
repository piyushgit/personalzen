import _ from 'lodash';
import moment from 'moment';
import { Dimensions } from 'react-native';

import {
  RegionStates,
  REGION_01_KEY,
  REGION_02_KEY,
  REGION_03_KEY,
  REGION_04_KEY,
  REGION_05_KEY,
  REGION_UNLOCK_TIME,
  REGION_LINKS,
} from '../constants/regions';
import { UPDATE_PLAYED_TIME, UPDATE_SCROLL_POSITION, UPDATE_SATURATION } from '../actions/index.names';

const ORIGINAL_WIDTH = 749;
const ORIGINAL_HEIGHT = 4693;

const SATURATION_STEPS = 10;
const INACTIVITY_DAYS_INTERVAL = 21;

const window = Dimensions.get('window');

export const initialState = {
  [REGION_01_KEY]: {
    state: RegionStates.unlocked,
    playedTime: 0, // minutes
    saturation: 0, // 0-9
    lastDayPlayed: null, // last day played
    saturationChange: null,
    totalTime: 0,
  },
  [REGION_02_KEY]: {
    state: RegionStates.locked,
    playedTime: 0,
    saturation: 0,
    lastDayPlayed: null,
    saturationChange: null,
    totalTime: 0,
  },
  [REGION_03_KEY]: {
    state: RegionStates.locked,
    playedTime: 0,
    saturation: 0,
    lastDayPlayed: null,
    saturationChange: null,
    totalTime: 0,
  },
  [REGION_04_KEY]: {
    state: RegionStates.locked,
    playedTime: 0,
    saturation: 0,
    lastDayPlayed: null,
    saturationChange: null,
    totalTime: 0,
  },
  [REGION_05_KEY]: {
    state: RegionStates.locked,
    playedTime: 0,
    saturation: 0,
    lastDayPlayed: null,
    saturationChange: null,
    totalTime: 0,
  },
  scrollPosition: ((window.width * ORIGINAL_HEIGHT) / ORIGINAL_WIDTH) - window.height,
};

/**
 * updateSaturation, this method recalculate the saturation based on last day that user played
 * the region and current day.
 *
 * @export
 * @param {*} region
 * @param {*} key
 * @param {*} today
 * @returns
 */
export function updateSaturation(region, key, today) {
  if (!region.lastDayPlayed) {
    return region;
  }
  today = moment(today);
  let lastDayPlayed = moment(region.lastDayPlayed, moment.ISO_8601).startOf('day');
  let saturationChange;
  let { playedTime } = region;
  const staturationStepDuration = (REGION_UNLOCK_TIME[key] / SATURATION_STEPS);

  // Update staturation change with last played date in case it is null
  if (region.lastDayPlayed != null && region.saturationChange == null) {
    saturationChange = moment(lastDayPlayed);
  } else {
    saturationChange = moment(region.saturationChange, moment.ISO_8601).startOf('day');
  }

  // Compute new saturation based on player's inativity time
  const inactivityPeriod = Math.floor(today.startOf('day').diff(saturationChange, 'days') / INACTIVITY_DAYS_INTERVAL);
  playedTime -= inactivityPeriod * staturationStepDuration;
  const saturation = Math.floor(playedTime / staturationStepDuration);

  // Update saturationChange with date that
  // the last saturation change occured due to inactivity
  const auxDate = moment(saturationChange);
  while (auxDate.add(21, 'days').isSameOrBefore(today)) {
    saturationChange.add(21, 'days');
  }

  lastDayPlayed = lastDayPlayed.format();
  saturationChange = saturationChange.format();

  return Object.assign({}, region, {
    saturation,
    lastDayPlayed,
    saturationChange,
    playedTime,
  });
}


/**
 * regions reducer, recalculates and updates regions saturation, last day played
 * and total played time.
 *
 * @param {*} [state=initialState]
 * @param {*} action
 * @returns
 */
const regions = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PLAYED_TIME: {
      const today = moment();
      const lastDayPlayed = moment(today, moment.ISO_8601).startOf('day').format();
      const { regionKey, playedTime } = action.payload;
      const region = state[regionKey];

      region.lastDayPlayed = lastDayPlayed;
      region.saturationChange = null;
      region.playedTime += playedTime;
      region.totalTime += playedTime;

      const nextRegionKey = REGION_LINKS[regionKey];
      const nextRegion = state[nextRegionKey];
      const unlockTime = REGION_UNLOCK_TIME[regionKey];

      // Checks if next region can be unlocked
      if (region.totalTime >= unlockTime && REGION_LINKS[regionKey]) {
        nextRegion.state = 'unlocked';
      }

      const newRegionState = updateSaturation(region, regionKey, today);

      return {
        ...state,
        [regionKey]: newRegionState,
      };
    }
    case UPDATE_SCROLL_POSITION: {
      return {
        ...state,
        scrollPosition: action.payload.scrollPosition,
      };
    }
    case UPDATE_SATURATION: {
      return _.mapValues(state, (region, key) => {
        const today = moment();
        return updateSaturation(region, key, today);
      });
    }
    default:
      return state;
  }
};

export default regions;
