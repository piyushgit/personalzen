package com.personalzen;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.dooboolab.RNIap.RNIapPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import ca.jaysoo.extradimensions.ExtraDimensionsPackage;
import com.sudoplz.reactnativeamplitudeanalytics.RNAmplitudeSDKPackage;
import io.sentry.RNSentryPackage;
import com.jadsonlourenco.RNShakeEvent.RNShakeEventPackage;
import com.sensors.RNSensorsPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.oblador.keychain.KeychainPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.zmxv.RNSound.RNSoundPackage;
import com.rnfs.RNFSPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.horcrux.svg.SvgPackage;
import com.BV.LinearGradient.LinearGradientPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
        new MainReactPackage(),
            new RNIapPackage(),
            new VectorIconsPackage(),
            new ExtraDimensionsPackage(),
            new RNAmplitudeSDKPackage(MainApplication.this),
            new RNSentryPackage(),
            new RNShakeEventPackage(),
        new RNSensorsPackage(),
        new SplashScreenReactPackage(),
        new KeychainPackage(),
        new RNSoundPackage(),
        new RNFSPackage(),
        new SvgPackage(),
        new LinearGradientPackage(),
        new RNDeviceInfo()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
